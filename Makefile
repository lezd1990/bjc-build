VENV = .venv
LANGUAGE = fr

SRC_DIR = source/${LANGUAGE}
ANNEX_DIR = annexes/${LANGUAGE}/annexes
SUPPORT_DIR = annexes/${LANGUAGE}/aides
SRC_FILES = $(shell find ${SRC_DIR} -type f -name '*.md' ! -name 'README*')
ANNEX_FILES = $(shell find ${ANNEX_DIR} -type f -name '*.md' ! -name 'README*')
SUPPORT_FILES = $(shell find ${SUPPORT_DIR} -type f -name '*.md' ! -name 'README*')

###############
# Build files #
###############

build: build-tex build-tex-annex build-tex-support build-tex-concordance build-osis build-epub build-txt

############################
# Build source LaTeX files #
############################

TEX_DIR = output/${LANGUAGE}/tex
TEX_FILES = $(shell echo ${SRC_FILES} | \
			  sed -e 's/source\/${LANGUAGE}/output\/${LANGUAGE}\/tex\/src/g' | \
			  sed -e 's/\.md/\.tex/g')

build-tex: ${TEX_DIR}/src ${TEX_FILES}

${TEX_DIR}/src/%.tex: ${SRC_DIR}/%.md
	@bin/md2tex.py $^ > $@

${TEX_DIR}/src:
	@mkdir -p $@

############################
# Build annex LaTeX files #
############################

TEX_ANNEX_DIR = output/${LANGUAGE}/tex
TEX_ANNEX_FILES = $(shell echo ${ANNEX_FILES} | \
			  sed -e 's/annexes\/${LANGUAGE}\/annexes/output\/${LANGUAGE}\/tex\/src\/annexes/g' | \
			  sed -e 's/\.md/\.tex/g')

build-tex-annex: ${TEX_ANNEX_DIR}/src/annexes
	@bin/md2tex_annexe.py -l ${LANGUAGE}

${TEX_ANNEX_DIR}/src/annexes:
	@mkdir -p $@

############################
# Build support LaTeX files #
############################

TEX_SUPPORT_DIR = output/${LANGUAGE}/tex
TEX_SUPPORT_FILES = $(shell echo ${SUPPORT_FILES} | \
			  sed -e 's/annexes\/${LANGUAGE}\/aides/output\/${LANGUAGE}\/tex\/src\/aides/g' | \
			  sed -e 's/\.md/\.tex/g') \
                    'output/${LANGUAGE}/tex/src/aides/concordance.tex' \
                    'output/${LANGUAGE}/tex/src/aides/glossaire.tex'

# build-tex-support: ${TEX_SUPPORT_DIR}/src/aides ${TEX_SUPPORT_FILES}

# ${TEX_SUPPORT_DIR}/src/aides/%.tex: ${SUPPORT_DIR}/%.md

build-tex-support: ${TEX_SUPPORT_DIR}/src/aides
	@bin/md2tex_dictionnaire.py -l ${LANGUAGE}
	@bin/md2tex_harmonie_evangile.py -l ${LANGUAGE}
	@bin/txt2tex_glossary.py -l ${LANGUAGE}


${TEX_SUPPORT_DIR}/src/aides:
	@mkdir -p $@

#########################
# Build Dictionary json #
#########################

TXT_DIR = output/${LANGUAGE}/txt
TXT_FILE = ${TXT_DIR}/dictionnaire.json

build-txt: ${TXT_DIR} ${TXT_FILE}

${TXT_FILE}: ${TEX_SUPPORT_DIR}/src/aides
	@bin/tex2json_dictionnaire.py -l ${LANGUAGE}

${TXT_DIR}:
	@mkdir -p $@

###########################################
# Build concordance (support) LaTeX files #
###########################################

LOG_DIR = output/${LANGUAGE}/log
LOG_FILE = ${LOG_DIR}/*log

build-tex-concordance: ${LOG_DIR}
	@bin/txt2tex_concordance.py -l ${LANGUAGE}

${LOG_DIR}:
	@mkdir -p $@

#####################
# Build PDF files   #
#####################

PDF_DIR = output/${LANGUAGE}/pdf
PDF_TEX = bjc_fr_main_165x235.tex
PDF_NAME = bym.pdf

build-pdf: ${PDF_DIR} ${PDF_DIR}/${PDF_NAME}

${PDF_DIR}/${PDF_NAME}: ${TEX_DIR}/${PDF_TEX} ${TEX_FILES}
	@bin/tex2pdf.sh $< $@

${PDF_DIR}:
	@mkdir -p $@

#####################
# Build ePub files  #
#####################

EPUB_DIR = output/${LANGUAGE}/epub
EPUB_FILES = $(shell echo ${SRC_FILES} | \
			  sed -e 's/source\/${LANGUAGE}/output\/${LANGUAGE}\/epub/g' | \
			  sed -e 's/\.md/\.epub/g')

build-epub: ${EPUB_DIR} ${EPUB_FILES}

${EPUB_DIR}/%.epub: ${SRC_DIR}/%.md
	@. ${VENV}/bin/activate; bin/md2epub.py $^ $@ -l ${LANGUAGE}

${EPUB_DIR}:
	@mkdir -p $@

####################
# Build OSIS files #
####################

OSIS_DIR = output/${LANGUAGE}/osis
OSIS_FILE = ${OSIS_DIR}/bible.xml

build-osis: ${OSIS_DIR} ${OSIS_FILE}

${OSIS_FILE}: ${TEX_DIR}/src
	@bin/tex2osis.py $^ $@ -l ${LANGUAGE}

${OSIS_DIR}:
	@mkdir -p $@

###############
# Clean files #
###############

clean:
	# remove files silently
	@echo $(shell rm ${TEX_FILES} ${TEX_ANNEX_FILES} ${TEX_SUPPORT_FILES} ${OSIS_FILE} ${EPUB_FILES} ${LOG_FILE} ${TXT_FILE} 2>/dev/null)

########################
# Install dependencies #
########################

install: install-tex-dependencies install-epub-dependencies install-watch-dependencies

install-tex-dependencies:
	@sudo apt install texlive-lang-french texlive-lang-german texlive-latex-extra texlive-xetex texlive-fonts-recommended fonts-lmodern
	@sudo apt install wget pdftk

install-epub-dependencies:
	@sudo apt install python3-pip python3-virtualenv python3-dev libxml2-dev libxslt1-dev zlib1g-dev
	@virtualenv -p python3 ${VENV}
	@. ${VENV}/bin/activate; pip3 install -U -r requirements.txt

install-watch-dependencies:
	@sudo apt install nodejs
	@npm install -g onchange

install-windows-dependencies:
	@. ${VENV}/bin/activate; pip3 install -U -r windows.txt

########
# Test #
########

test:
	@virtualenv -p python3 ${VENV}
	@. ${VENV}/bin/activate;
	pip3 install freezegun formencode pytest pytest-cov;
	pytest -v --cov=bin --cov=scripts --doctest-modules --ignore="bak"

###############
# Watch files #
###############

watch:
	onchange ${SRC_FILES} -- make build

#########
# Phony #
#########

.PHONY: build build-tex build-tex-annex build-tex-support build-tex-concordance build-osis build-epub build-txt clean install-tex-dependencies install-epub-dependencies install-watch-dependencies watch install-windows-dependencies
