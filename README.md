[![pipeline status](https://gitlab.com/anjc/bjc-build/badges/master/pipeline.svg)](https://gitlab.com/anjc/bjc-build/-/commits/master)
[![coverage report](https://gitlab.com/anjc/bjc-build/badges/master/coverage.svg)](https://gitlab.com/anjc/bjc-build/-/commits/master)

# Clonage du projet

```
git clone git@gitlab.com:anjc/bjc-build.git
```
or
```
git clone https://gitlab.com/anjc/bjc-build.git
```
```
git submodule init
git submodule update
```

# Installation des dépendances

```
apt install -y make
make install
```

then, for a windows platform

```
make install-windows-dependencies
```

# Génération des fichiers

```
make build
```
ou
```
make build LANGUAGE=fr
```

# Génération des pdf

1. Choix du modèles (templates)
2. Choix du langage
3. Récupération de la dernière version des dépôts source et annexes selon le choix du langage
4. Nettoyage des répertoires de travail
5. Génération des fichiers LaTeX (.tex) à partir des sources.
6. Génération du fichier PDF à partir des fichiers .tex
7. Vérification manuelle.

## Les modèles

### Format des modèles

`bjc_{pdf_lang}_{title}_{format}.tex`

* _pdf_lang_: Langue de la Bible à générer en PDF. Actuellement disponible, le _fr_ (français) et le _de_ (germany/allemand)...
* _title_: Titre court/désignation du template. Doit commencer par "test" si il s'agît d'un template de test.
* _format_: Le format final du format papier. Ex: 127x202, 165x235.

### Modèles disponibles

__Available templates__ | __Language__ | __Description__ | __Test__
------------ | ------------- | ------------- | -------------
bjc_fr_eco_127x202.tex  | fr | BYM economic and small format for french - Français : output/fr/tex/_bjc_fr_eco_127x202.tex_ |
bjc_fr_main_165x235.tex | fr | BYM main and larger format for french - Français : output/fr/tex/_bjc_fr_main_165x235.tex_ |
bjc_fr_main_arch_165x235.tex | fr | BYM main with biblical archeology and larger format for french - Français : output/fr/tex/_bjc_fr_main_arch_165x235.tex_ |
bjc_fr_test_arch_165x235.tex | fr | BYM test biblical archeology and larger format for french - Français : output/fr/tex/_bjc_fr_test_arch_165x235.tex_ | x
bjc_fr_main_arch_165x235.tex | fr | BYM main with biblical archeology, larger format and thumb tabs (onglets) for french - Français : output/fr/tex/_bjc_fr_main_full_165x235.tex_ |
bjc_de_main_127x202.tex | de | BYM for German - Deutsche : output/de/tex/_bjc_de_main_127x202.tex_ |
bjc_de_evangelien_127x202.tex | de | BYM (Gospel) for German - Deutsche : output/de/tex/_bjc_de_evangelien_127x202.tex_ |

## Exécution

Le fichier de sortie se trouve dans _output/{pdf_lang}/pdf/pre_bjc.pdf_

Ex: output/fr/pdf/pre_bjc.pdf

* via script

```
./bin/pdf.sh bjc_fr_main_165x235.tex
```

* en ligne de commande

```
TEX='bjc_fr_main_165x235.tex'
PDF_LANG=$(echo $TEX| cut -d'_' -f 2)
cd source/$PDF_LANG/ ; git pull
cd ../../annexes/$PDF_LANG/ ; git pull
cd ../..
make clean LANGUAGE=$PDF_LANG ; make LANGUAGE=$PDF_LANG
make build-pdf LANGUAGE=$PDF_LANG PDF_TEX=$TEX
```

## Dimensions des pdf

```
Grand format : 165 x 235 mm | 6.49606 x 9.25197 in
format eco : 127 x 202 mm | 5 x 7.95276 in
```

# Génération des fichiers LaTeX

```
make build-tex
```
ou
```
make build-tex LANGUAGE=fr
```

# Génération des fichiers LaTeX des annexes

```
make build-tex-annex
```
ou
```
make build-tex-annex LANGUAGE=fr
```

# Génération des fichiers LaTeX de l'aide

```
make build-tex-support
```
ou
```
make build-tex-support LANGUAGE=fr
```

# Génération des fichiers LaTeX des concordances

```
make build-tex-concordance
```
ou
```
make build-tex-concordance LANGUAGE=fr
```

# Génération des fichiers ePub

```
make build-epub
```
ou
```
make build-epub LANGUAGE=fr
```

# Génération du fichier OSIS

```
make build-osis
```
ou
```
make build-osis LANGUAGE=fr
```

# Nettoyage des répértoires des fichiers générés

```
make clean
```
ou
```
make clean LANGUAGE=fr
```

# Les scripts disponibles

__Name__ | __Description__
------------ | -------------
scripts/bym-audit-verses.sh | Permet de faire un audit du nombre de verset et de chapitre attendu. Le résultat de l'exécution doit être redirigé dans _scripts/result\_check.txt_
scripts/bym-check.sh | Permet de vérifier tous les fichiers .md. A lancer dans _bjc_source_
scripts/bym_concordance.py | Permet de générer le fichier de concordance à partir du répertoire _source/<lang>_
scripts/bym_conjugate_verb.py |
scripts/bym_dispatch.py |
scripts/bym_get_infinitive.py |
scripts/bym_get_verbs.py |
scripts/bym_verify_added_word.py |
scripts/bym_verify_dispatch.py |
bin/md2epub.py | Generate ePub files from Markdown files
bin/md2tex_annexe.py | Generate LaTex files from Markdown formatted for annexes
bin/md2tex_dictionnaire.py | Generate LaTeX files from Markdown formatted files for dictionaries
bin/md2tex_harmonie_evangile.py | Generate LaTex files from Markdown formatted for harmony of gospel (harmonie des évangiles)
bin/md2tex.py | Generate LaTeX formatted files from Markdown files
bin/pdf.sh | Generate pdf according to template name.
bin/tex2json_dictionnaire.py | Generate Json dictionnaire from LaTeX files
bin/tex2md_annexe.py | Generate Markdown formatted files from LaTeX files for annexes
bin/tex2md_dictionnaire.py | Generate Markdown formatted dictionnaries from LaTeX files
bin/tex2md.py | Generate Markdown formatted files from LaTeX files
bin/tex2osis.py | Generate an OSIS Bible formatted file from LaTeX files
bin/tex2pdf.sh | Generate PDF file from LaTeX files
bin/txt2tex_concordance.py | Generate a concordance from md files, and base on a list of words (csv/txt)
bin/txt2tex_glossary.py | Generate a glossary tex from txt files (csv, ...).
