#!/usr/bin/env python3

"""TXT2TEX: CONCORDANCE

Generate a concordance from md files, and base on a list of words (csv/txt).
"""

import os
import re

dirname = os.path.dirname(__file__)
md_dir = dirname + '/../source/'
concordance_file = dirname + '/../output/tex/src/aides/concordance.tex'
words_file = dirname + '/../annexes/aides/concordance.txt'
notfound_file = dirname + '/../output/log/concordance.log'
md_files = os.listdir(md_dir)
md_files = [md_file for md_file in md_files
            if (md_file.endswith('.md') and
                md_file[0].isdigit())]  # Ex: 01-Genese.md
md_files.sort()
print(md_files)
print(concordance_file)

# regex verbes "er"
regex_vb_er = '(?:'
regex_vb_er += '[est]|es|ai[est]?|[i]?ons|[i]?ez|(?:ai)?ent'
regex_vb_er += '|é[e]?[s]?|ant[s]?'
regex_vb_er += '|er(?:a(?:i|s)?[st]?|[i]?on[st]|[i]?ez|aient)?'
regex_vb_er += '|(?:a(?:i|s)?|â(m|t)(?:es)?|èrent)'
regex_vb_er += ')'

# regex verbes "i"
regex_vb_ir = '(?:'
regex_vb_ir += 'i[st]|isse[s]?|issai[est]?|iss[i]?ons|iss[i]?ez|iss(?:ai)?ent'
regex_vb_ir += '|i[e]?[s]?|(?:iss)?ant[s]?|ue?s?'
regex_vb_ir += '|ir(?:a(?:i|s)?[st]?|[i]?on[st]|[i]?ez|aient)?|î(m|t)es|irent'
regex_vb_ir += ')'

# regex verbes "re"
regex_vb_re = '(?:'
regex_vb_re += '[st]?|ai[est]?|[i]?ons|[i]?ez|(?:ai)?ent|e[s]?'
regex_vb_re += '|u[e]?[s]?|i[et]?[s]?|ant[s]?'
regex_vb_re += '|r[e]?(?:a(?:i|s)?[st]?|[i]?on[st]|[i]?ez|aient)?'
regex_vb_re += '|î(m|t)es|irent'
regex_vb_re += ')'

# abbreviations livres
books = (
    'Ge', 'Ex', 'Lé', 'No', 'De', 'Jos', 'Jg', '1 S', '2 S', '1 R', '2 R',
    'Es', 'Jé', 'Ez', 'Os', 'Joë', 'Am', 'Ab', 'Jon', 'Mi', 'Na', 'Ha', 'So',
    'Ag', 'Za', 'Mal', 'Ps', 'Pr', 'Job', 'Ca', 'Ru', 'La', 'Ec', 'Est', 'Da',
    'Esd', 'Né', '1 Ch', '2 Ch', 'Mt', 'Mc', 'Lu', 'Jn', 'Ac', 'Ja', 'Ga',
    '1 Th', '2 Th', '1 Co', '2 Co', 'Ro', 'Ep', 'Ph', 'Col', 'Phm', '1 Ti',
    'Tit', '1 Pi', '2 Pi', '2 Ti', 'Jud', 'Hé', '1 Jn', '2 Jn', '3 Jn', 'Ap'
)


def cleanup(text):
    """Remove french quote and comments."""
    text = re.sub(r'<[^<]+>', '', text)  # remove comments
    text = re.sub(r'« *', '', text)
    text = re.sub(r' *»', '', text)
    text = re.sub(r'“ *', '', text)
    text = re.sub(r' *”', '', text)
    text = re.sub(r' +', ' ', text)
    text = text.strip()
    return text


def replace_sc(text):
    """Replace specials characters

    Quotes are replaced by 888.
    Hypens are replaced by 999.

    Need a string argument.
    Return a string.
    """

    text = re.sub(r"'", r'888', text)
    text = re.sub(r'-', r'999', text)

    return text


def restore_sc(text):
    """Restore specials characters

    Restore 888 as quotes.
    Restore 999 as hypens.

    Need a string argument.
    Return a string.
    """

    text = re.sub(r'888', r"'", text)
    text = re.sub(r'999', r'-', text)

    return text


def shorten(text):
    """Shorten some words

    Need a string argument.
    Return a string.
    """

    text = re.sub(r'\bcomme\b', r'com.', text)
    text = re.sub(r'\bdonc\b', r'dc', text)
    text = re.sub(r'\bdans\b', r'ds', text)
    text = re.sub(r'\bdevant\b', r'dvt', text)
    text = re.sub(r'\bfemmes\b', r'fem.', text)
    text = re.sub(r'\bfemme\b', r'fem.', text)
    text = re.sub(r'\bholocauste\b', r'holoc.', text)
    text = re.sub(r'\bhommes\b', r'hom.', text)
    text = re.sub(r'\bhomme\b', r'hom.', text)
    text = re.sub(r'\bhumains\b', r'hum.', text)
    text = re.sub(r'\bJérusalem\b', r'Jérus.', text)
    text = re.sub(r'\bYeroushalaim\b', r'Yerous.', text)
    text = re.sub(r'\bleur\b', r'lr.', text)
    text = re.sub(r'\bjusque\b', r'jsq.', text)
    text = re.sub(r'\bmême\b', r'mm', text)
    text = re.sub(r'\bmaintenant\b', r'mntnt', text)
    text = re.sub(r'\bnous\b', r'ns', text)
    text = re.sub(r'\bordonnance\b', r'ordon.', text)
    text = re.sub(r'\bquand\b', r'qd', text)
    text = re.sub(r'\bquelque\b', r'qq.', text)
    text = re.sub(r'\bSeigneur\b', r'Seign.', text)
    text = re.sub(r'\bsouverain\b', r'souv.', text)
    text = re.sub(r'\btous\b', r'ts', text)
    text = re.sub(r'\btout\b', r'tt', text)
    text = re.sub(r'\btoute\b', r'tte', text)
    text = re.sub(r'\btoutes\b', r'ttes', text)
    text = re.sub(r'\bvêtements\b', r'vêt.', text)
    text = re.sub(r'\bvêtement\b', r'vêt.', text)
    text = re.sub(r'\bvous\b', r'vs', text)
    text = re.sub(r'^\s+', r'', text)
    text = re.sub(r'\s+$', r'', text)
    text = re.sub(r'\.\.$', r'.', text)

    return text


def get_regex(category, search):
    """
    Retourne la regex adaptee au type de mot cherche
    """
    # preserve la compatibilite avec les regex Perl du fichier de mots
    search = re.sub(r'\\K', '', search)

    # application d'une regex en fonction du type de mot
    if category == 'v':
        split = re.split(r'(er|ir|re)$', search)
        if split[1] == 'er':
            regex_vb = regex_vb_er
        elif split[1] == 'ir':
            regex_vb = regex_vb_ir
        elif split[1] == 're':
            regex_vb = regex_vb_re
        search = split[0] + regex_vb
    elif category == 'n' or category == 'adj':
        if search.endswith('eux'):
            search = search[:-1] + '(?:x|se[s]?)'
        elif not search.endswith('?'):
            search = search + '[e]?[sx]?'

    # compilation de la regex
    regex = re.compile(r'\b%s\b' % search, re.IGNORECASE)

    return regex, search


def get_context(match, text):
    # recupere la position du mot dans le verset
    word = match.group(0)
    start = match.span()[0]
    end = match.span()[1]

    # calcul le nombre de mots necessaires au contexte
    if start < 10:
        b = '3'
        a = '6'
    elif end > len(text)-10:
        b = '6'
        a = '3'
    else:
        b = '3'
        a = '3'

    # recupere les elements du contexte qui precedent le mot
    before = text[:start]
    before = replace_sc(before)
    before = before[::-1]
    before = re.sub(r'((?:(?:\W+)(?:\w+)?){' + b + '}).*', r'\1', before)
    before = before[::-1]
    before = restore_sc(before)
    # recupere les elements du contexte qui suivent le mot
    after = text[end:]
    after = replace_sc(after)
    after = re.sub(r'((?:(?:\W+)?(?:\w+)){' + a + '}).*', r'\1', after)
    after = restore_sc(after)

    # reconstruit le mot et son contexte
    context = before + word[:1] + '.' + after
    context = shorten(context)

    return context


def browse_bible(term, search, refs, regex, concordance, notfound):
    # intialisation
    occurrences = 0
    errors = {}
    pattern = r'^([0-9]+):([0-9]+)\t(.+)'

    # parcours des fichiers md
    for md_file in md_files:
        md = open(md_dir + md_file, 'r')
        for line in md:
            line = line.strip()
            if not re.match(pattern, line):
                continue
            # recupere les elements de la liste
            book = int(md_file[:2])  # numero du livre
            chapter, verse, text = (re.match(pattern, line).groups())
            text = cleanup(text)

            # recupere le nom du livre et construit la reference complete
            name = books[book - 1]
            ref = name + ' ' + chapter + ':' + verse

            # si le verset en cours n'est pas dans la liste des versets
            # on passe à l'tieration suivante
            if ref not in refs:
                continue

            # test la regex sur le texte du verset en cours
            match = regex.search(text)

            # si le motif de la regex n'a pas ete trouve dans le verset
            # on log et on passe à li'teration suivante
            if not match:
                errors[ref] = text
                continue

            # incrmentation du comtpeur d'occurrences trouves
            occurrences += 1

            # contexte
            context = get_context(match, text)

            # ecriture de la reference et le contexte
            concordance.write('\\item[\\vref{' + ref + '}] ' + context + '\n')
        md.close()

    # si les compteur different, log anomalie
    log_notfound(notfound, occurrences, term, refs, search, errors)


def write_legend(concordance, line):
    # recuperation et ecriture de la legende du mot, si existante
    if len(line) > 4:
        legend = line[4]
        legend = re.sub(r'\\\\', r'\n\\item ', legend)
        concordance.write('\\begin{legend}\n')
        concordance.write('\\NoAutoSpaceBeforeFDP{\n')
        concordance.write('\\item ')
        concordance.write(legend + '\n')
        concordance.write('}\n\\end{legend}\n')


def log_notfound(notfound, occurrences, term, refs, search, errors):
    count = len(refs)  # compteur du nb de references

    # si les compteur different, log anomalie
    if occurrences != count:
        missing = count - occurrences
        notfound.write('\n' + term + ':' +
                       '\n\t- regex: \\b' + search + '\\b' +
                       '\n\t- nb_ref: ' + str(count) +
                       '\n\t- found: ' + str(occurrences) +
                       '\n\t- missing: ' + str(missing) +
                       '\n\t- refs:\n')
        for ref in errors:
            notfound.write('\t\t> ' + ref + ' : ' + errors[ref] + '\n')


def main():
    # recupere tous les mots dans une liste
    words = open(words_file, 'r')

    # fichier de log
    notfound = open(notfound_file, 'w')
    notfound.write('### Journal des erreurs ###\n')

    # ouverture du fichier de sortie
    concordance = open(concordance_file, 'w')
    # ecriture en-tetes du fichier LaTeX
    concordance.write('\\begin{multicols}{3}\n')
    concordance.write('{\\fontsize{8pt}{0.7em}\\selectfont\n')

    # parcours les mots de la liste
    for line in words:
        line = line.strip()
        if not line:
            continue
        line = line.split('\t')

        # recupere les elements du mot
        term = line[0]  # terme
        search = line[1]  # motif de recherche
        category = line[2]  # type de mot
        refs = line[3].split(';')  # liste de versets

        # application d'une regex en fonction du type de mot
        regex, search = get_regex(category, search)

        print(term)

        # ecriture en-tetes du mot
        concordance.write('\n\\ConcordanceEntry{' + term + '}\n')
        concordance.write('\\vspace{-2mm}\n')
        concordance.write('\\begin{listverse}\n')

        # parcours fichier et ecriture des references
        browse_bible(term, search, refs, regex, concordance, notfound)

        # ecriture en-tetes de fermeture
        concordance.write('\\end{listverse}\n')

        # recuperation et ecriture de la legende du mot, si existante
        write_legend(concordance, line)

    # ecriture en-tetes
    concordance.write('}\n')
    concordance.write('\\end{multicols}\n')

    words.close()
    notfound.close()
    concordance.close()

if __name__ == '__main__':
    main()
