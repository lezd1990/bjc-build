#!/usr/bin/env python3

"""MD2PUB

Generate ePub files from Markdown files
"""

import argparse
import re
import time

from ebooklib import epub
from ebooklib.plugins import standard


AUTHOR = 'ANJC Productions'
CREATOR = AUTHOR
PUBLISHER = AUTHOR
RIGHTS = 'CC-BY-NC-SA 4.0'
LANG_ATTR = {'fr': {'ref': 'BYM',
                    'title': 'Bible de Yéhoshoua ha Mashiah',
                    'description': ('Bible de Yéhoshoua ha Mashiah, édition 2020. '
                                    'Révision de la Bible à partir du texte '
                                    'de la Bible Martin 1744.')},
             'de': {'ref': 'BYM-DE',
                    'title': 'Bibel von Yehoshua ha Mashiah',
                    'description': 'Bibel von Yehoshua ha Mashiah'},
}
LANG_MAPPINGS = {'fr': {'chap': 'Chapitre',
                        'intro': 'Introduction'},
                 'de': {'chap': 'Kapitel',
                        'intro': 'Einführung'},
}


def main(source, destination, lang='fr'):
    """Process."""
    if lang not in LANG_ATTR:
        raise KeyError('Language "{}" is not implemented'.format(lang))
    if destination == '':
        raise FileNotFoundError('empty destination file')
    elif destination is None:
        raise TypeError('destination file is None')
    md_book = open(source)
    epub_book = epub.EpubBook()

    # define style for content
    style = '''body {}
h1 {
    text-align: center;
    font-size: 110%;
}
h2.title {
    text-align: center;
    font-size: 80%;
    font-style: italic;
}
p {
    text-align: justify;
    font-size: 100%;
}
i.verse {
    font-style: normal;
    font-size: 50%;
    vertical-align: super;
}
'''
    default_css = epub.EpubItem(
        uid="style_default",
        file_name="style/default.css",
        media_type="text/css",
        content=style
    )
    # add metadata
    metadata = LANG_ATTR[lang]
    epub_id = re.search(r'(\d+-\w+)\.md$', source).group(1)
    identifier = '{}-{}'.format(metadata['ref'], epub_id)
    epub_book.set_identifier(identifier)
    epub_book.set_language(lang)
    epub_book.add_author(AUTHOR)
    epub_book.add_metadata('DC', 'title', metadata['title'])
    # epub_book.add_metadata('DC', 'identifier', identifier)
    # epub_book.add_metadata('DC', 'language', lang)
    # epub_book.add_metadata('DC', 'author', AUTHOR)
    epub_book.add_metadata('DC', 'creator', CREATOR)
    epub_book.add_metadata('DC', 'publisher', PUBLISHER)
    # epub_book.add_metadata('DC', 'description', metadata['description'])
    epub_book.add_metadata('DC', 'rights', RIGHTS)
    epub_book.add_metadata('DC', 'date', time.strftime('%Y-%m-%d'))
    # init list
    c = {}
    mapping = LANG_MAPPINGS[lang]
    for line in md_book:
        line = line.rstrip()
        if line == '':
            continue
        # get book
        if line.startswith('# '):
            book = re.sub(
                #  r'^# ((?:\d\s)?(?:\w+(\s\w+)*)) \((?:\d\s)?\w+\.?\)$',
                r'^# ([^(]+).+',
                r'\1',
                line
            )
            #import pdb; pdb.set_trace()
            book = book.strip()
            print(book)
            epub_book.set_title(book + ' (BYM)')
            continue
        # get book infos & intro
        if re.search('^[^\d#]+', line):
            if 0 not in c:
                c[0] = epub.EpubHtml(title=mapping['intro'], file_name='intro.xhtml')
                c[0].add_item(default_css)
                epub_book.add_item(c[0])
                c[0].content = '<h1>{}</h1>'.format(mapping['chap'])
            c[0].content += '<p>' + line + '</p>'
            continue
        # get chapter
        if line.startswith('## '):
            chap = re.sub(r'[^\d]*(\d+)', r'\1', line)
            chap = int(chap)
            # close last paragraph of previous chapter
            if chap > 1:
                c[chap - 1].content += '</p>'
            c[chap] = epub.EpubHtml(
                title=mapping['chap'] + ' ' + str(chap),
                file_name=str(chap) + '.xhtml'
            )
            c[chap].add_item(default_css)
            epub_book.add_item(c[chap])
            c[chap].content = '<h1>' + mapping['chap'] + ' ' + str(chap) + '</h1><p>'
            continue
        # get title
        if line.startswith('### '):
            # line = re.sub(r'<!--([^>]*)-->', r'\\TitleNote{\1}', line)
            title = re.sub(r'### (.*)', r'\1', line)
            c[chap].content += '</p><h2 class="title">' + title + '</h2><p>'
            continue
        # get verse
        if re.search('^\d+:\d+\t', line):
            # line = re.sub(r'<!--([^>]*)-->', r'\\Note{\1}', line)
            verse = re.sub(r'^\d+:(\d+)\t.*', r'\1', line)
            text = re.sub(r'^\d+:\d+\t(.*)', r'\1', line)
            c[chap].content += '<i class="verse">' + verse + '</i> ' + text + ' '
            continue
    c[chap].content += '</p>'
    # create table of contents
    chapters = list(c.values())
    epub_book.toc = chapters
    # style for navigation file
    style = 'body { color: black; }'
    nav_css = epub.EpubItem(
        uid="style_nav",
        file_name="style/nav.css",
        media_type="text/css",
        content=style
    )
    # add navigation files
    epub_book.add_item(epub.EpubNcx())
    nav = epub.EpubNav()
    nav.add_item(nav_css)
    epub_book.add_item(nav)
    # add css files
    epub_book.add_item(default_css)
    epub_book.add_item(nav_css)
    # create spine
    epub_book.spine = ['nav', ] + chapters
    # create epub file
    epub.write_epub(destination, epub_book, {'plugins': [standard.SyntaxPlugin()]})
    md_book.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert Markdown file to ePub')
    parser.add_argument('input', help='Markdown file to convert to ePub')
    parser.add_argument('output', help='ePub destination file')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANG_ATTR.keys())
    args = parser.parse_args()
    lang = args.lang
    main(args.input, args.output, lang=lang)
