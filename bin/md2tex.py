#!/usr/bin/env python3

"""MD2TEX

Generate LaTeX formatted files from Markdown files
"""

import argparse
import re


LINE_LENGTH = 116  # length of line into pdf with 9pt
MAX_LENGTH_NOTE = LINE_LENGTH * 2
RE_TITLE = re.compile(r'^# (\d{0,1}[\. ]?.+)\((\d{0,1}[\. ]?.+)\)$')


def repl_note(m):
    """Replacing note according to is length."""
    group = m.groups()[1]
    if len(group) > MAX_LENGTH_NOTE:
        # Ex: {\interfootnotelinepenalty=100 \Note{My very long text.}}
        return '{\\interfootnotelinepenalty=100 \\Note{%s}}' % group
    return '\\Note{%s}' % group


def get_titles(line):
    """Return title and short title of a book from a line.

    >>> get_titles('foobar')
    (None, None)
    >>> get_titles('# foo')
    (None, None)
    >>> get_titles('# My Book')
    (None, 'None')
    >>> get_titles('# Actes (Ac.)')
    ('Actes', 'Ac.')
    >>> get_titles('# Bereshit (Genèse) (Ge.)')
    ('Bereshit (Genèse)', 'Ge.')
    >>> get_titles('# 1 Thessaloniciens (1 Th.)')
    ('1 Thessaloniciens', '1 Th.')
    >>> get_titles('# 1 Thessalonicher (1.Thes)')
    ('1 Thessalonicher', '1.Thes')
    >>> get_titles('# 1 Thessalonicher (1. Thes)')
    ('1 Thessalonicher', '1. Thes')
    >>> get_titles('# 1 Yohanan (1 Jean) (1 Jn.)')
    ('1 Yohanan (1 Jean)', '1 Jn.')
    >>> get_titles('# 1 Yohanan (1 Johannes) (1 Joh)')
    ('1 Yohanan (1 Johannes)', '1 Joh')
    >>> get_titles('# Epheser (Eph)')
    ('Epheser', 'Eph')
    >>> get_titles('')
    (None, None)
    >>> get_titles(None)
    (None, None)
    >>> get_titles(0)
    Traceback (most recent call last):
        ...
    AttributeError: 'int' object has no attribute 'strip'
    >>> get_titles(1)
    Traceback (most recent call last):
        ...
    AttributeError: 'int' object has no attribute 'strip'
    >>> get_titles(False)
    Traceback (most recent call last):
        ...
    AttributeError: 'bool' object has no attribute 'strip'
    >>> get_titles(True)
    Traceback (most recent call last):
        ...
    AttributeError: 'bool' object has no attribute 'strip'
    """
    if line in (None, ''):
        return (None, None)
    line = line.strip()
    result = RE_TITLE.match(line)
    title = short_title = None
    if result:
        title, short_title = result.groups()
        title = title.strip()
        short_title = short_title.strip()
    return title, short_title


def main(filename):
    """Process."""
    fh = open(filename, 'r')
    info = False
    for line in fh:
        line = line.rstrip()
        #line = re.sub(r'(\d+)(ème|er)', r'\1\\up{\2}', line)
        line = re.sub(r' ([!?:;»])', r'~\1', line)
        line = re.sub(r'(«) ', r'\1~', line)
        line = re.sub(r'([0-9]) ([0-9])', r'\1~\2', line)
        line = re.sub(r'%', '\%', line)
        # quote
        line = re.sub(r'`', r'\\textasciigrave{}', line)
        # get book
        if line.startswith('# '):
            book, book_short = get_titles(line)
            print('\ShortTitle{' + book_short + '}\BookTitle{' + book +
                  '}\BFont')
            continue
        # get book infos & intro
        if re.search('^[^\d#]+', line):
            if not info:
                info = True
                print(r'\vspace{-\baselineskip}\noindent\hrulefill\vspace{-\baselineskip}')
                print(r'{\footnotesize\textit{\bigskip{\centering{}')
            if line.startswith('Date de') or \
               line.startswith('Zeit der') or \
               line.startswith('Datum der'):  # germany
                print(r'\\' + line + r'\\}}')
                print(r'\textit{')
            else:
                print(r'\\' + line)
            continue
        # get chapter
        if line.startswith('## '):
            chap = re.sub(r'[^\d]*(\d+)', r'\1', line)
            if chap == '1':
                print(r'\\\bigskip')  # end of introduction
                print(r'}')
                print(r'}')
                print(r'\vspace{-\baselineskip}\par\nobreak\noindent\hrulefill\vspace{-\baselineskip}')
                print(r'\begin{multicols}{2}')
            print('\Chap{' + chap + '}')
            continue
        # get title
        if line.startswith('### '):
            line = re.sub(r'<!--([^>]*)-->', r'\\TitleNote{\1}', line)
            title = re.sub(r'### (.*)', r'\\TextTitle{\1}', line)
            print(title)
            continue
        # get verse
        if re.search('^\d+:\d+\t', line):
            line = re.sub(r'(<w(.(morph|lemma)+[^>]*)>)(.[^<]*)(<\/w>)',
                          r'\\bymStrong{\1}{\4}{\5}', line)
            line = re.sub(r'(<!--)([^>]*)(-->)', repl_note, line)
            line = re.sub(r'\[([^…\]]*)\]', r'\\TextDial{\1}', line)
            verse = re.sub(r'^\d+:(\d+)\t.*', r'\1', line)
            text = re.sub(r'^\d+:\d+\t(.*)', r'\1', line)
            if verse == '1':
                print('\VerseOne{}' + text)
            else:
                print('\VS{' + verse + '}' + text)
            continue
    print(r'\PPE{}')
    print(r'\end{multicols}')
    fh.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert Markdown file to LaTeX')
    parser.add_argument('filename',
                        help='Markdown file to convert to LaTeX')
    args = parser.parse_args()
    main(args.filename)
