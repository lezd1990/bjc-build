#!/usr/bin/env python3

"""MD2TEX: annexes

Generate LaTeX files from Markdown formatted for annexes.
"""

import os
import re
import sys
import argparse


LANGUAGES = ['fr', 'de']


def main(filename):

    re_begin = re.compile(r'^\\(begin|sub|section|clearpage)')

    md = open(filename, 'r')
    # pre-format line
    prev = ''
    newline = False
    for index, line in enumerate(md):
        line = line.strip()
        line = line or ''
        line = re.sub(r'« ', r'«~', line)
        line = re.sub(r' ([!?:;»])', r'~\1', line)
        line = re.sub(r'([0-9]) ([0-9])', r'\1~\2', line)
        # allow to remove space before colon ':'
        line = re.sub(r'(([0-1]{1} )?[A-Z][a-zé]{1,2}[.] [0-9]+:[0-9]+(-[0-9]+)?)', r'\\vref{\1}', line)
        line = re.sub(r'(\d+)(ème|er|ère)', r'\1\\up{\2}', line)
        line = re.sub(r'%', '\%', line)
        # quote
        line = re.sub(r'`', r'\\textasciigrave{}', line)

        if line.startswith('---') or line.startswith('***') or \
           line.startswith('___'):
            print('\\clearpage')
            prev = '\\clearpage'
            continue
        elif line.startswith('# '):
            line = re.sub(r'# (.+)',
                          r'\\begin{center}{\\LARGE \1}\\end{center}',
                          line)
            if not index == 0:
                print('\\end{small}')
            print(line)
            print('\\begin{small}')
            prev = line or ''
            continue
        elif index == 0:  # first loop
            print('\\begin{small}')
            prev = line or ''
            continue
        # exposant
        line = re.sub(r'<sup>([^<]+)</sup>', r'\\up{\1}', line)
        line = re.sub(r'__([^_]+)__', r'\\textbf{\1}', line)  # gras
        line = re.sub(r'\*\*([^*]+)\*\*', r'\\textbf{\1}', line)  # gras
        line = re.sub(r'^(«~)\*([^*]+)\*', r'\1\\emph{\2}', line)  # emphase
        line = re.sub(r'\*([^*]+)\*', r'\\textit{\1}', line)  # italic
        line = re.sub(r'#### (.+)', r'\\subsubsection*{\1}', line)
        line = re.sub(r'### (.+)', r'\\subsection*{\1}', line)
        line = re.sub(r'## (.+)', r'\\section*{\1}', line)

        # note de bas de page
        line = re.sub(r'<!--([^>]*)-->', r'\\footnote{\1}', line)
        # image
        # for example with mutlicolumn:
        # https://tex.stackexchange.com/questions/12262/multicol-and-figures
        #
        # \documentclass[a5paper]{article}
        # \usepackage{multicol,caption}
        # \usepackage[demo]{graphicx}
        # \usepackage{lipsum}
        # \newenvironment{Figure}
        #   {\par\medskip\noindent\minipage{\linewidth}}
        #   {\endminipage\par\medskip}
        # \begin{document}
        # 
        # \begin{multicols}{2}
        # \lipsum[1]
        # \begin{Figure}
        #  \centering
        #  \includegraphics[width=\linewidth]{foo}
        #  \captionof{figure}{my caption of the figure}
        # \end{Figure}
        # 
        # \lipsum[1]
        # \end{multicols}
        # 
        # \end{document}
        line = re.sub(r'!\[(.+)\]\(([^\/]+\.(jpg|png|jpeg|jfif))\){ *(.+) *}',  # noqa
                      r'\\begin{Figure}'
                      r'\n  \\centering'
                      r'\n  \\includegraphics[\4]{\2}'
                      r'\n  \\caption[\1]{\1}'
                      r'\n\\end{Figure}', line)
        line = re.sub(r'!\[(.+)\]\((https?\:\/\/.*\/)([^\/]+\.(jpg|png|jpeg|jfif))\){ *(.+) *}',  # noqa
                      r'\\write18{wget \2\3}'
                      r'\n\\begin{Figure}'
                      r'\n  \\centering'
                      r'\n  \\includegraphics[\5]{\3}'
                      r'\n  \\caption[\1]{\1}'
                      r'\n\\end{Figure}', line)
        line = re.sub(r'!\[(.+)\]\(([^\/]+\.(jpg|png|jpeg|jfif)) +"(.+)"\){ *(.+) *}',  # noqa
                      r'\\begin{Figure}'
                      r'\n  \\centering'
                      r'\n  \\includegraphics[\5]{\2}'
                      r'\n  \\caption[\4]{\1}'
                      r'\n\\end{Figure}', line)
        line = re.sub(r'!\[(.+)\]\((https?\:\/\/.*\/)([^\/]+\.(jpg|png|jpeg|jfif)) +"(.+)"\){ *(.+) *}',  # noqa
                      r'\\write18{wget \2\3}'
                      r'\n\\begin{Figure}'
                      r'\n  \\centering'
                      r'\n  \\includegraphics[\6]{\3}'
                      r'\n  \\caption[\5]{\1}'
                      r'\n\\end{Figure}', line)
        line = re.sub(r'!\[(.+)\]\(([^\/]+\.(jpg|png|jpeg|jfif))\)',
                      r'\\begin{Figure}'
                      r'\n  \\centering'
                      r'\n  \\includegraphics[width=\\linewidth]{\2}'
                      r'\n  \\caption[\1]{\1}'
                      r'\n\\end{Figure}', line)
        line = re.sub(r'!\[(.+)\]\((https?\:\/\/.*\/)([^\/]+\.(jpg|png|jpeg|jfif))\)',
                      r'\\write18{wget \2\3}'
                      r'\n\\begin{Figure}'
                      r'\n  \\centering'
                      r'\n  \\includegraphics[width=\\linewidth]{\3}'
                      r'\n  \\caption[\1]{\1}'
                      r'\n\\end{Figure}', line)
        line = re.sub(r'!\[(.+)\]\(([^\/]+\.(jpg|png|jpeg|jfif)) +"(.+)"\)',
                      r'\\begin{Figure}'
                      r'\n  \\centering'
                      r'\n  \\includegraphics[width=\\linewidth]{\2}'
                      r'\n  \\caption[\4]{\1}'
                      r'\n\\end{Figure}', line)
        line = re.sub(r'!\[(.+)\]\((https?\:\/\/.*\/)([^\/]+\.(jpg|png|jpeg|jfif)) +"(.+)"\)',
                      r'\\write18{wget \2\3}'
                      r'\n\\begin{Figure}'
                      r'\n  \\centering'
                      r'\n  \\includegraphics[width=\\linewidth]{\3}'
                      r'\n  \\caption[\5]{\1}'
                      r'\n\\end{Figure}', line)
        # url
        line = re.sub(r'!\[(.+)\]\((https?\:\/\/.+)\)',
                      r'\\href{\2}{\1}',
                      line)
        line = re.sub(r'!\[\]\((https?\:\/\/.+)\)',
                      r'\\url{\1}',
                      line)
        # blockquote
        line = re.sub(r'^«(.+)',
                      r'\\begin{quote}\n«\1\\end{quote}',
                      line)
        line = re.sub(r'(\d+)(ème|er|ère)', r'\1\\up{\2}', line)
        line = re.sub(r'<div style="text-align: right">(.+)</div>',
                      r'\\begin{flushright}\1\\end{flushright}', line)
        if newline and line and not re_begin.match(line) and \
           not re_begin.match(prev):
            if line.startswith('-'):
                if not prev.startswith('-'):
                    print('\\begin{itemize}[label=\\textbullet]')
            else:
                if prev:
                    print('\\newline')
                else:
                    print('\\bigskip')
        newline = False
        if not re_begin.match(line) and \
           not re_begin.match(prev):
            newline = True
        if prev.startswith('-'):
            if not line.startswith('-'):
                print('\\end{itemize}')
        if line.startswith('-'):
            print('  \\item ' + line[1:])
        else:
            print(line)
        if not (prev == '\\clearpage' and not line):
            prev = line
    print('\\end{small}')

    md.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert LaTeX file to Mardown for annexes')
    # parser.add_argument('filename',
    #                     help='LaTeX file to convert to Markdown')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANGUAGES)
    options = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/annexes/'
    md_dir = dirname + '/../annexes/' + options.lang + '/annexes/'
    print(md_dir)
    print(tex_dir)
    # get a list of all LaTeX books files
    md_files = os.listdir(md_dir)
    md_files = [md_file for md_file in md_files
                if (md_file.endswith('.md') and
                    not md_file.startswith('dictionnaire') and
                    not md_file[0].isdigit())]  # Ex: 01-Genese.md
    md_files.sort()
    print(md_files)

    orig_stdout = sys.stdout
    for md_file in md_files:
        with open(tex_dir + md_file[:-3] + '.tex', 'w') as tex:
            sys.stdout = tex
            main(md_dir + md_file)
    sys.stdout = orig_stdout
