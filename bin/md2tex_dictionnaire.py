#!/usr/bin/env python3

"""MD2TEX for dictionaries

Generate LaTeX files from Markdown formatted files
"""

import os
import re
import sys
import argparse


LANGUAGES = ['fr', 'de']


def main(filename):
    md = open(filename, 'r')
    # pre-format line
    print("\\begin{multicols}{2}")

    newline = False
    for line in md:
        line = re.sub(r'« ', r'«~', line)
        line = re.sub(r' ([!?:;»])', r'~\1', line)
        line = re.sub(r'([0-9]) ([0-9])', r'\1~\2', line)
        line = re.sub(r'(\d+)(ème|er|ère)', r'\1\\up{\2}', line)
        line = re.sub(r'%', '\%', line)
        # quote
        line = re.sub(r'`', r'\\textasciigrave{}', line)

        # get book infos \\textit{([^}]*)}.*
        if (line.startswith('\#\#.*\n')):
            dicoentry = re.sub(r'', r'', line)
            newline = False
        else:
            dicotext = re.sub(r'<!--(([^>]+))-->', r'\\vref{\2}', line)
            dicotext = re.sub(r'^\+', r'\\\\-', dicotext)
            dicotext = re.sub(r'^   \+ ', r'\\\\', dicotext)
            dicotext = re.sub(r'^([2-9]|[1-9][0-9]+)\.', r'\\\\\1.', dicotext)
            if not newline:
                dicotext = re.sub(r'^1\.', r'\\\\1.', dicotext)
                if not re.match(r'^(#|\\|\n)', dicotext):
                    dicotext = re.sub(r'(.+)', r'\\\\\1', dicotext)
            newline = False
            if re.match(r'##(.*)\n', dicotext):
                dicotext = re.sub(r'##(.*)\n', r'\\textit{\1}\\newline\n',
                                  dicotext)
                newline = True
            dicotext = re.sub(r'#(.*)\n', r'\\DicoEntry{\1}', dicotext)

            print(dicotext, end='')
        continue
    print("\\end{multicols}")

    md.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert LaTeX file to Mardown for dictionaries')
    # parser.add_argument('filename',
    #                     help='LaTeX file to convert to Markdown')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANGUAGES)
    options = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/aides/'
    md_dir = dirname + '/../annexes/' + options.lang + '/aides/'
    print(tex_dir)
    print(md_dir)
    # get a list of all Markdown books files
    md_files = os.listdir(md_dir)
    # md_files = [md_file for md_file in md_files if md_file.endswith('dictionnaire.md')]
    md_files = [md_file for md_file in md_files if
                (md_file.startswith('dictionnaire') and
                 md_file.endswith('.md'))]
    md_files.sort()
    print(md_files)
    
    orig_stdout = sys.stdout
    for md_file in md_files:
        with open(tex_dir + md_file[:-3] + '.tex', 'w') as tex:
            sys.stdout = tex
            main(md_dir + md_file)
    sys.stdout = orig_stdout
