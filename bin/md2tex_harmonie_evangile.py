#!/usr/bin/env python3

"""MD2TEX harmony of gospel

Generate LaTeX files from Markdown formatted
"""

import os
import re
import sys
import argparse


LANGUAGES = ['fr', 'de']


def main(filename='harmonie-evangile.md', lang='fr'):
    if lang not in LANGUAGES:
        raise KeyError('Language "{}" is not implemented'.format(lang))
    md = open(filename, 'r')
    
    print('\\begin{center}')
    print('\\begin{longtable}{ L{0.4\\textwidth} @{} *{4}{C{.1\\textwidth}} @{} }')
    print('\\toprule')
    if lang == 'de':
        print('''Titel & Matthäus (Mt) & Markus (Mk) & Lukas (Lk) & Yohanan (Joh)\\\\ \\hline \\hline''')
    else:
        print('''Titre & Matthaios (Mt.) & Markos (Mc.) & Loukas (Lu.) & Yohanan (Jn.)\\\\ \\hline \\hline''')
    print('\\endfirsthead')
    if lang == 'de':
        print('\\multicolumn{5}{c}{Harmonie der Evangelien}\\\\')
        print('Titel & Mt & Mk & Lk & Joh\\\\ \\hline \\hline\\\\')
    else:
        print('\\multicolumn{5}{c}{Harmonie des évangiles}\\\\')
        print('Titre & Mt. & Mc. & Lu. & Jn.\\\\ \\hline \\hline\\\\')
    print('\\endhead')
    
    count = 1
    for line in md:
        # quote
        line = re.sub(r'`', r'\\textasciigrave{}', line)
        if line.startswith('## '):
            print('\\\\\\multicolumn{5}{p{0.95\\columnwidth}}{\\textbf{' + line[3:].strip() +
                      '}}\\\\\\nopagebreak')
        elif line.startswith('### '):
            columns = line[4:].strip().split('&')
            res = columns[0]
            for column in columns[1:]:
                res += '&'
                if column:
                    res = res + '\\vref{' + column + '}'
            print('{\\tiny' + str(count) + '.}' + res + '\\\\')
            count += 1
    print('\\end{longtable}')
    print('\\end{center}')
    
    md.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert LaTeX file to Mardown for harmony of gospel')
    # parser.add_argument('filename',
    #                     help='LaTeX file to convert to Markdown')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANGUAGES)
    options = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/aides/'
    md_dir = dirname + '/../annexes/' + options.lang + '/aides/'
    filename = 'harmonie-evangile.md'
    md_file = md_dir + filename
    print(md_dir)
    print(tex_dir)
    print(md_file)
    
    orig_stdout = sys.stdout
    with open(tex_dir + filename[:-3] + '.tex', 'w') as tex:
        sys.stdout = tex
        main(md_file, lang=options.lang)
    sys.stdout = orig_stdout
