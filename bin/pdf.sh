#!/bin/bash

# Generate pdf according to template name.
# Usage:
#     ./bin/pdf.sh input.tex
# Ex: 
# ./bin/pdf.sh bjc_fr_eco_127x202.tex

function main() {
    if [[ -z ${1} ]]; then
        echo -e "Usage:\n\n\t${0} input.tex\n"
        exit 1
    fi

    local TEX=${1}  # Name of the template. Ex: 'bjc_fr_eco_127x202.tex'
    local PDF_LANG=$(echo $TEX| cut -d'_' -f 2)
    cd source/$PDF_LANG/ ; git pull
    cd ../../annexes/$PDF_LANG/ ; git pull
    cd ../..
    make clean LANGUAGE=$PDF_LANG  # cleanup
    # generate all files
    make build-tex LANGUAGE=$PDF_LANG
    make build-tex-annex LANGUAGE=$PDF_LANG
    make build-tex-support LANGUAGE=$PDF_LANG
    make build-tex-concordance LANGUAGE=$PDF_LANG
    make build-pdf LANGUAGE=$PDF_LANG PDF_TEX=$TEX  # generate pdf
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "${@}"
fi
