# Apokalupsis (Apocalypse ou révélation) (Ap.)

Signification : Mettre à nu, révélation d'une vérité, action de révéler

Auteur : Yohanan (Jean)

Thème : L'aboutissement de toutes choses

Date de rédaction : Env. 95 ap. J.-C.

Le terme apocalypse, du grec « apokalupsis », évoque « l'action de révéler ce qui était caché ou inconnu ». Ce mot a pour racine « apokalupto » qui signifie aussi « découvrir, dévoiler ce qui est voilé ou recouvert ».

C'est à Patmos, île grecque de la Mer Égée – où il s'exila en raison de la persécution de l'empereur Domitien (51 – 96 ap. J.-C.) – que Yohanan reçut une révélation de Yéhoshoua ha Mashiah (Jésus-Christ) ainsi qu'un message s'adressant aux « sept assemblées » qui constituaient certainement les villes de l'Asie Mineure où se trouvaient les principales concentrations de chrétiens. Si Éphèse figure dans les écrits de la Nouvelle Alliance et que Thyatire et Laodicée y sont brièvement mentionnées, les quatre autres assemblées – qu'on ne retrouve nulle part ailleurs dans les Écritures – étaient sans doute le fruit du travail missionnaire de Paulos (Paul). Les sept lettres s'adressent à l'ange de chacune de ces assemblées locales, autrement dit aux messagers de celles-ci (probablement un ancien ou un responsable).

Ce livre, qui arrive en conclusion des Écritures, annonce les événements qui doivent précéder la fin de l'histoire de l'humanité.

## Chapitre 1

### Introduction

1:1	Révélation<!--« Apokalupsis » en grec. Voir la présentation du livre de l'Apocalypse.--> de Yéhoshoua Mashiah, qu'Elohîm lui-même<!--Voir Ga. 1:1.--> a donnée pour montrer à ses esclaves les choses qui doivent arriver avec promptitude<!--Vient du grec « tachos » qui signifie « vitesse, rapidité, vivacité et promptitude ».--> et il l'a fait connaître par l'envoi de son ange à Yohanan son esclave.
1:2	Lequel a rendu témoignage de la parole d'Elohîm et du témoignage de Yéhoshoua Mashiah, tout ce qu'il a vu.
1:3	Béni est celui qui lit et ceux qui entendent les paroles de cette prophétie, et qui gardent les choses qui y sont écrites ! Car le temps est proche.

### Yéhoshoua ha Mashiah (Jésus-Christ)

1:4	Yohanan aux sept assemblées qui sont en Asie : à vous grâce et paix, de la part de celui QUI EST, et QUI ÉTAIT et QUI VIENT<!--Les prophètes ont prophétisé la venue de YHWH en personne : Es. 35:4, 40:10-11, 60:1-2 ; Za. 14:1-21 ; Jn. 14:1-3. Yéhoshoua Mashiah (Jésus-Christ) est bien YHWH qui vient.-->, et de la part des sept Esprits qui sont devant son trône,
1:5	et de la part de Yéhoshoua Mashiah, le témoin<!--Job 16:19.--> fidèle, le premier-né d'entre les morts<!--Voir commentaire Col. 1:15.--> et le chef<!--Un prince, un chef, un meneur, un magistrat. Yéhoshoua est le chef des chefs. Il est également le chef du chef de ce monde. Voir Jn. 12:31.--> des rois de la Terre. À celui qui nous a aimés et qui nous a lavés de nos péchés dans son sang,
1:6	et qui a fait de nous des rois et des prêtres pour Elohîm, son Père, à lui soient la gloire et la force d'âges en âges. Amen !

## Chapitre 2

### Éphèse : L'assemblée qui a abandonné son premier amour

2:1	Écris à l'ange<!--Ange, du grec « aggelos » : « envoyé, messager, un ange, un messager d'Elohîm ». Ce terme sert à désigner aussi bien les créatures spirituelles que les êtres humains.--> de l'assemblée d'Éphèse : Voici ce que dit celui qui tient les sept étoiles dans sa main droite, et qui marche au milieu des sept chandeliers d'or :
2:2	Je connais tes œuvres, et ton travail, et ta patience, et je sais que tu ne peux pas supporter les méchants, et que tu as éprouvé ceux qui se disent apôtres et qui ne le sont pas, et que tu les as trouvés menteurs ;
