#AARON
##, de l'hébreu « aharown » : « haut placé » ou « éclairé ».
Issu de la tribu de Lévi, frère aîné de Moshè (Moïse) dont il fut le porte-parole. Premier grand-prêtre* en Israël. Voir <!--Ex. 4:14--> ; <!--Ex. 6:16-20--> et <!--Ex. 28-->.

#AMOUR
##
Il existe plusieurs traductions et définitions du mot « amour » en hébreu et en grec, elles varient selon le contexte.
+ Les termes hébreux désignant l'amour :
1. « 'ahab » : « amours »
   + Amours, amis. Voir <!--Os. 8:9--> et <!--Pr. 5:19-->.
2. « 'ahabah » : « amour humain, amour d'Elohîm pour son peuple »
   + Amour, affection, aimer. Voir <!--De. 7:8--> ; <!--1 S. 20:17--> et <!--Pr. 10:12-->.
3. « checed » : « bonté, miséricorde, fidélité »
   + Grâce, miséricorde, compassion, affection. Voir <!--Ge. 40:14--> ; <!--Ex. 34:7--> et <!--No. 14:19-->.
4. « yediyd » : « celui qui est aimé, bien-aimé »
   + Bien-aimé, amour. Voir <!--De. 33:12--> ; <!--Es. 5:1--> ; <!--Jé. 11:15--> et <!--Ps. 45:1-->.
+ Les termes grecs désignant l'amour :
1. « agape » : « amour, charité, affection, bienveillance »
   + Amour d'Elohîm, amour désintéressé que doit manifester l'être humain né d'en haut. Voir <!--Jn. 15:13--> ; <!--Jn. 17:26--> ; <!--1 Co. 8:1--> ; <!--1 Co. 13:3--> ; <!--Ro. 5:5--> et <!--1 Jn. 4:8-->.
2. « eros » : « l'amour qui prend »
   + Amour dans la dimension sexuelle.
3. « phileo » : « aimer, montrer des signes d'amour »
   + Amour filial. Voir <!--Jn. 21:17--> et <!--1 Co. 16:22-->.
4. « philadelphia » : « amour fraternel »
   + Amour des frères et sœurs d'une même famille, amour des chrétiens les uns pour les autres. Voir <!--1 Th. 4:9--> ; <!--Ro. 12:10--> et <!--Hé. 13:1-->.
5. « stergo » : « chérir avec affection ». « Astorgos » : « sans affection naturelle » est un dérivé présumé de « stergo »
   + (Sans) affection naturelle. Voir <!--Ro. 1:31-->.

#EZRA
##, de l'hébreu « `ezra' » : « secours », généralement traduit par « Esdras ».
Fils de Serayah et descendant du grand-prêtre Aaron, Ezra était scribe et prêtre. Il enseigna le peuple d'Elohîm dans la torah de YHWH et mit en place des réformes après la reconstruction du temple. Son histoire se trouve dans le livre éponyme : Esdras.
