& Matthaios (Matthieu) & Markos (Marc) & Loukas (Luc) & Yohanan (Jean)

## Naissance et adolescence de Yéhoshoua (Jésus)

### Genèse de Yéhoshoua&1:1-17&&3:23-38&
### Annonce de la naissance de Yohanan le Baptiste (Jean-Baptiste)&&&1:5-25&
### Annonce de la naissance de Yéhoshoua&&&1:26-38&

## Yéhoshoua ha Mashiah (Jésus-Christ) au début de son service public

### Yohanan le Baptiste (Jean-Baptiste) l'envoyé d'Elohîm&3:1-12&1:1-8&3:1-18&
### Le baptême de Yéhoshoua ha Mashiah&3:13-17&1:9-11&3:21-22&
### La tentation de Yéhoshoua au désert&4:1-11&1:12-13&4:1-13&
### Témoignage de Yohanan le Baptiste&&&&1:15-34
### Les premiers disciples de Yéhoshoua ha Mashiah&&&&1:35-42
### Les disciples, Philippos et Netanél&&&&1:43-51
### Miracle de Cana&&&&2:1-12
