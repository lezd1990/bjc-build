# Les temps bibliques (jour, mois, année)

Voici les mois de l'année biblique, avec leur durée et leur correspondance dans notre calendrier grégorien. À noter qu'à l'origine ils étaient désignés par un numéro (1er mois, 2ème mois, etc.) et qu'ils n'ont été nommés qu'à partir de l'exil babylonien.

1. Nisan (ou Abib), 30 jours : Mars-Avril.
2. Iyyar (ou Ziv), 29 jours : Avril-Mai.
3. Sivan, 30 jours : Mai-Juin.
4. Tammouz, 29 jours : Juin-Juillet.
5. Ab, 30 jours : Juillet-Août.
6. Éloul, 29 jours : Août-Septembre.
7. Tishri (ou Éthanim), 30 jours : Septembre-Octobre.
8. Marchesvan (ou Boul), 29 jours : Octobre-Novembre.
9. Chislev (ou Kisleu), 30 jours : Novembre-Décembre.
10. Tébeth, 29 jours : Décembre-Janvier.
11. Shebat, 30 jours : Janvier-Février.
12. Adar, 29 jours : Février-Mars.
13. Ve-adar ou deuxième Adar : tous les 3 ans.

## Le jour

__Le jour__ débute avec le coucher du soleil (donc à la tombée de la nuit) et se termine au coucher du soleil suivant (voir Ge. 1:8 : « *Il y eut un soir et il y eut un matin : second jour* »).

## La nuit ou nuitée

__La nuit__ est divisée en veilles.
Dans les textes de l'Ancienne Alliance, la nuit est divisée en 3 veilles. Les indications horaires sont approximatives :
- 1ère veille : de 18h à 22h.
- 2ème veille ou veille du milieu : de 22h à 2h du matin.
- 3ème veille : de 2h à 6h.

Dans les Évangiles et le Testament de Yéhoshoua (Jésus), la nuit est divisée en 4 veilles, selon l'habitude romaine :
- 1ère veille ou « soir » : de 18h à 21h.
- 2ème veille : de 21h à minuit.
- 3ème veille ou « chant du coq » : de minuit à 3h du matin.
- 4ème veille : de 3h à 6h.

