#!/usr/bin/env python3
"""
Testing bin files.
"""

import os.path
import sys
import unittest
from unittest import mock
from io import StringIO
import xml.etree.ElementTree as ET

try:
    import ebooklib
except ImportError:
    ebooklib = None
try:
    import formencode
except ImportError:
    formencode = None

from freezegun import freeze_time

import bin as func


FIXTURES = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                         'fixtures'))


class MockZipFile:
    """Fake ZipFile."""

    files = None
    contents = None

    def __init__(self, *args, **kwargs):
        self.files = []
        self.contents = {}

    def __call__(self, *args, **kwargs):
        return self

    def __iter__(self):
        return iter(self.files)

    def write(self, fname):
        """Fake write."""
        self.files.append(fname)

    def writestr(self, fname, content, *args, **kwargs):
        """Fake writestr."""
        self.contents[fname] = content

    def close(self):
        """Fake close."""
        pass


class CommonTest(unittest.TestCase):
    """Common tests."""

    @staticmethod
    def _run(function, source, *args, **kwargs):
        """Run a function according to source file/dir and returns stdout."""
        # Store the reference, in case you want to show things again in
        # standard output
        old_stdout = sys.stdout
        # This variable will store everything that is sent to the standard
        # output
        result = StringIO()
        sys.stdout = result
        # Here we can call anything we like, like external modules, and
        # everything that they will send to standard output will be stored on
        # "result"
        try:
            function.main(source, *args, **kwargs)
        finally:
            # Redirect again the std output to screen
            sys.stdout = old_stdout
        # Then, get the stdout like a string and process it!
        return result.getvalue()


class TestBin(CommonTest):
    """Testing bin."""

    def assertEqualXML(self, want, got):
        """Comparing XML."""
        from formencode.doctest_xml_compare import xml_compare
        dump = lambda x: ET.fromstring(x.strip())
        reporter = lambda x: sys.stdout.write(x + "\n")
        self.assertTrue(xml_compare(dump(want), dump(got), reporter))

    @unittest.skipIf(ebooklib is None, 'EbookLib is not installed')
    @unittest.skipIf(formencode is None, 'formencode is not installed')
    @freeze_time('2020-05-24 12:34:56')
    def test_md2epub(self):
        filename = '01-Genese'
        function = func.md2epub
        src = 'md'
        dst = 'epub'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        with mock.patch('ebooklib.epub.zipfile.ZipFile', MockZipFile()) as mck:
            function.main('{}.{}'.format(path, src),
                          '{}.{}'.format(path, dst))
        # check content
        self.assertIn('mimetype', mck.contents)
        self.assertIn('META-INF/container.xml', mck.contents)
        self.assertIn('EPUB/content.opf', mck.contents)
        self.assertIn('EPUB/intro.xhtml', mck.contents)
        self.assertIn('EPUB/1.xhtml', mck.contents)
        self.assertIn('EPUB/2.xhtml', mck.contents)
        self.assertIn('EPUB/toc.ncx', mck.contents)
        self.assertIn('EPUB/nav.xhtml', mck.contents)
        self.assertIn('EPUB/style/default.css', mck.contents)
        self.assertIn('EPUB/style/nav.css', mck.contents)
        folder = '{}.{}'.format(path, dst)
        def read(fname, mode='rb'):
            """Read files."""
            with open(os.path.normpath(os.path.join(folder, fname)), mode) as \
                  fout:
                ret = fout.read()
            return ret

        self.assertEqual(mck.contents['mimetype'], 'application/epub+zip')
        expected = read('META-INF/container.xml', 'r')
        self.assertEqual(expected, mck.contents['META-INF/container.xml'])
        # expected = read('EPUB/content.opf')
        # FIXME self.assertEqualXML(expected, mck.contents['EPUB/content.opf'])
        cont = mck.contents['EPUB/content.opf']
        self.assertIn(b'<dc:publisher>ANJC Productions</dc:publisher>',
                      cont)
        self.assertIn(b'<dc:date>2020-05-24</dc:date>', cont)
        self.assertIn(b'<dc:language>fr</dc:language>', cont)
        self.assertIn(b'<dc:title>Bible de Y', cont)
        self.assertIn(b'<dc:title>Bereshit (BYM)</dc:title>', cont)
        self.assertIn(b'<dc:creator id="creator">ANJC Productions</dc:creator>',
                      cont)
        self.assertIn(b'<dc:creator>ANJC Productions</dc:creator>', cont)
        self.assertIn(b'<dc:identifier id="id">BYM-01-Genese</dc:identifier>',
                      cont)
        self.assertIn(b'<dc:rights>CC-BY-NC-SA 4.0</dc:rights>',
                      cont)
        expected = read('EPUB/intro.xhtml')
        self.assertEqualXML(expected, mck.contents['EPUB/intro.xhtml'])
        expected = read('EPUB/1.xhtml')
        self.assertEqualXML(expected, mck.contents['EPUB/1.xhtml'])
        expected = read('EPUB/2.xhtml')
        self.assertEqualXML(expected, mck.contents['EPUB/2.xhtml'])
        expected = read('EPUB/toc.ncx')
        self.assertEqualXML(expected, mck.contents['EPUB/toc.ncx'])
        expected = read('EPUB/nav.xhtml')
        self.assertEqualXML(expected, mck.contents['EPUB/nav.xhtml'])
        expected = read('EPUB/style/default.css', 'r')
        self.assertEqual(expected.strip(),
                         mck.contents['EPUB/style/default.css'].strip())
        expected = read('EPUB/style/nav.css', 'r')
        self.assertEqual(expected.strip(),
                         mck.contents['EPUB/style/nav.css'].strip())

    def test_md2tex_annexe(self):
        function = func.md2tex_annexe
        filename = 'les-temps-bibliques'
        src = 'md'
        dst = 'tex'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, '{}.{}'.format(path, src))
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open('{}.{}'.format(path, dst), 'r') as expected:
            self.assertEqual(expected.read(), res)

    def test_md2tex_dictionnaire(self):
        filename = 'dictionnaire'
        function = func.md2tex_dictionnaire
        src = 'md'
        dst = 'tex'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, '{}.{}'.format(path, src))
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open('{}.{}'.format(path, dst), 'r') as expected:
            self.assertEqual(expected.read(), res)

    def test_md2tex_harmonie_evangile(self):
        filename = 'harmonie-evangile'
        function = func.md2tex_harmonie_evangile
        src = 'md'
        dst = 'tex'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, '{}.{}'.format(path, src))
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open('{}.{}'.format(path, dst), 'r') as expected:
            self.assertEqual(expected.read(), res)

    def test_md2tex(self):
        filename = '01-Genese'
        function = func.md2tex
        src = 'md'
        dst = 'tex'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, '{}.{}'.format(path, src))
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open('{}.{}'.format(path, dst), 'r') as expected:
            self.assertEqual(expected.read(), res)

    def test_md2tex_2(self):
        filename = '66-Apocalypse'
        function = func.md2tex
        src = 'md'
        dst = 'tex'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, '{}.{}'.format(path, src))
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open('{}.{}'.format(path, dst), 'r') as expected:
            self.assertEqual(expected.read(), res)

    def test_tex2json_dictionnaire(self):
        filename = 'dictionnaire'
        function = func.tex2json_dictionnaire
        src = 'tex'
        dst = 'json'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, '{}.{}'.format(path, src))
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open('{}.{}'.format(path, dst), 'r') as expected:
            self.assertEqual(expected.read(), res)

    # FIXME def test_tex2md_annexe(self):
    #     filename = 'les-temps-bibliques'
    #     function = func.tex2md_annexe
    #     src = 'tex'
    #     dst = 'md'
    #     path = os.path.normpath(os.path.join(FIXTURES, filename))
    #     res = self._run(function, '{}.{}'.format(path, src))
    #     self.assertTrue(res)
    #     self.assertTrue(isinstance(res, str))
    #     with open('{}.{}'.format(path, dst), 'r') as expected:
    #         self.assertEqual(expected.read(), res)

    def test_tex2md_dictionnaire(self):
        filename = 'dictionnaire'
        function = func.tex2md_dictionnaire
        src = 'tex'
        dst = 'md'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, '{}.{}'.format(path, src))
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open('{}.{}'.format(path, dst), 'r') as expected:
            self.assertEqual(expected.read(), res)

    # FIXME def test_tex2md(self):
    #     filename = '01-Genese'
    #     function = func.tex2md
    #     src = 'tex'
    #     dst = 'md'
    #     path = os.path.normpath(os.path.join(FIXTURES, filename))
    #     res = self._run(function, '{}.{}'.format(path, src))
    #     self.assertTrue(res)
    #     self.assertTrue(isinstance(res, str))
    #     with open('{}.{}'.format(path, dst), 'r') as expected:
    #         self.assertEqual(expected.read(), res)

    # FIXME def test_tex2md_2(self):
    #     filename = '66-Apocalypse'
    #     function = func.tex2md
    #     src = 'tex'
    #     dst = 'md'
    #     path = os.path.normpath(os.path.join(FIXTURES, filename))
    #     res = self._run(function, '{}.{}'.format(path, src))
    #     self.assertTrue(res)
    #     self.assertTrue(isinstance(res, str))
    #     with open('{}.{}'.format(path, dst), 'r') as expected:
    #         self.assertEqual(expected.read(), res)

    @freeze_time('2020-05-24 12:34:56')
    def test_tex2osis(self):
        filename = 'osis.xml'
        function = func.tex2osis
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, FIXTURES)
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open(path, 'r') as expected:
            self.assertEqual((expected.read()).strip(), res)

    @freeze_time('2020-05-24 12:34:56')
    def test_tex2osis_none_dir(self):
        filename = 'osis.xml'
        function = func.tex2osis
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, FIXTURES)
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open(path, 'r') as expected:
            self.assertEqual((expected.read()).strip(), res)

    # def test_tex2txt_concordance(self):
    #     pass

    # def test_txt2tex_concordance(self):
    #     pass

    def test_txt2tex_glossary(self):
        filename = 'glossaire'
        function = func.txt2tex_glossary
        src = 'txt'
        dst = 'tex'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, '{}.{}'.format(path, src))
        self.assertTrue(res)
        self.assertTrue(isinstance(res, str))
        with open('{}.{}'.format(path, dst), 'r') as expected:
            self.assertEqual((expected.read().strip()), res)


class TestBinFailed(CommonTest):
    """Testing bin failed."""

    def tearDown(self):
        # these directories should not be created
        for fake in ['fake.epub', 'fake.md', 'fake.tex',
                     'fake.txt', 'fake.json']:
            try:
                os.remove(os.path.join(FIXTURES, fake))
            except FileNotFoundError:
                pass

    @unittest.skipIf(ebooklib is None, 'EbookLib is not installed')
    def test_md2epub_src_filenotfound(self):
        function = func.md2epub
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.epub'))
        with self.assertRaises(FileNotFoundError):
            function.main(path, path)

    @unittest.skipIf(ebooklib is None, 'EbookLib is not installed')
    def test_md2epub_src_empty_file(self):
        function = func.md2epub
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.epub'))
        with self.assertRaises(FileNotFoundError):
            function.main('', path)

    @unittest.skipIf(ebooklib is None, 'EbookLib is not installed')
    def test_md2epub_src_none_file(self):
        function = func.md2epub
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.epub'))
        with self.assertRaises(TypeError):
            function.main(None, path)

    @unittest.skipIf(ebooklib is None, 'EbookLib is not installed')
    def test_md2epub_dst_empty_file(self):
        function = func.md2epub
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.md'))
        with self.assertRaises(FileNotFoundError):
            function.main(path, '')

    @unittest.skipIf(ebooklib is None, 'EbookLib is not installed')
    def test_md2epub_dst_none_file(self):
        function = func.md2epub
        path = os.path.normpath(os.path.join(FIXTURES, '01-Genese.md'))
        with self.assertRaises(TypeError):
            function.main(path, None)

    def test_md2tex_annexe_filenotfound(self):
        function = func.md2tex_annexe
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.md'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_md2tex_annexe_empty_file(self):
        function = func.md2tex_annexe
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_md2tex_annexe_none_file(self):
        function = func.md2tex_annexe
        with self.assertRaises(TypeError):
            self._run(function, None)

    def test_md2tex_dictionnaire_filenotfound(self):
        function = func.md2tex_dictionnaire
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.md'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_md2tex_dictionnaire_empty_file(self):
        function = func.md2tex_dictionnaire
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_md2tex_dictionnaire_none_file(self):
        function = func.md2tex_dictionnaire
        with self.assertRaises(TypeError):
            self._run(function, None)

    def test_md2tex_harmonie_evangile_filenotfound(self):
        function = func.md2tex_harmonie_evangile
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.md'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_md2tex_harmonie_evangile_empty_file(self):
        function = func.md2tex_harmonie_evangile
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_md2tex_harmonie_evangile_none_file(self):
        function = func.md2tex_harmonie_evangile
        with self.assertRaises(TypeError):
            self._run(function, None)

    def test_md2tex_filenotfound(self):
        function = func.md2tex
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.md'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_md2tex_empty_file(self):
        function = func.md2tex
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_md2tex_none_file(self):
        function = func.md2tex
        with self.assertRaises(TypeError):
            self._run(function, None)

    def test_tex2json_dictionnaire_filenotfound(self):
        function = func.tex2json_dictionnaire
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.tex'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_tex2json_dictionnaire_empty_file(self):
        function = func.tex2json_dictionnaire
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_tex2json_dictionnaire_none_file(self):
        function = func.tex2json_dictionnaire
        with self.assertRaises(TypeError):
            self._run(function, None)

    def test_tex2md_annexe_filenotfound(self):
        function = func.tex2md_annexe
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.tex'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_tex2md_annexe_empty_file(self):
        function = func.tex2md_annexe
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_tex2md_annexe_none_file(self):
        function = func.tex2md_annexe
        with self.assertRaises(TypeError):
            self._run(function, None)

    def test_tex2md_dictionnaire_filenotfound(self):
        function = func.tex2md_dictionnaire
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.tex'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_tex2md_dictionnaire_empty_file(self):
        function = func.tex2md_dictionnaire
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_tex2md_dictionnaire_none_file(self):
        function = func.tex2md_dictionnaire
        with self.assertRaises(TypeError):
            self._run(function, None)

    def test_tex2md_filenotfound(self):
        function = func.tex2md
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.tex'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_tex2md_empty_file(self):
        function = func.tex2md
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_tex2md_none_file(self):
        function = func.tex2md
        with self.assertRaises(TypeError):
            self._run(function, None)

    def test_tex2osis_empty_dir(self):
        function = func.tex2osis
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_txt2tex_glossary_filenotfound(self):
        function = func.txt2tex_glossary
        path = os.path.normpath(os.path.join(FIXTURES, 'fake.txt'))
        with self.assertRaises(FileNotFoundError):
            self._run(function, path)

    def test_txt2tex_glossary_empty_file(self):
        function = func.txt2tex_glossary
        with self.assertRaises(FileNotFoundError):
            self._run(function, '')

    def test_txt2tex_glossary_none_file(self):
        function = func.txt2tex_glossary
        with self.assertRaises(TypeError):
            self._run(function, None)


class TestBinI18n(CommonTest):
    """Testing bin internationalization."""

    @unittest.skipIf(ebooklib is None, 'EbookLib is not installed')
    def test_md2epub_i18n(self):
        lang = 'de'
        filename = '01-Genese'
        function = func.md2epub
        src = 'md'
        dst = 'epub'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        with mock.patch('ebooklib.epub.zipfile.ZipFile', MockZipFile()) as mck:
            function.main('{}.{}'.format(path, src),
                          '{}.{}'.format(path, dst), lang=lang)
        # check content
        self.assertIn('mimetype', mck.contents)
        self.assertIn('META-INF/container.xml', mck.contents)
        self.assertIn('EPUB/content.opf', mck.contents)
        self.assertIn('EPUB/intro.xhtml', mck.contents)
        self.assertIn('EPUB/1.xhtml', mck.contents)
        self.assertIn('EPUB/2.xhtml', mck.contents)
        self.assertIn('EPUB/toc.ncx', mck.contents)
        self.assertIn('EPUB/nav.xhtml', mck.contents)
        self.assertIn('EPUB/style/default.css', mck.contents)
        self.assertIn('EPUB/style/nav.css', mck.contents)
        self.assertEqual(mck.contents['mimetype'], 'application/epub+zip')
        content = mck.contents['EPUB/content.opf']
        self.assertIn(b'<dc:title>Bibel von Yehoshua ha Mashiah</dc:title>',
                      content)
        self.assertNotIn(b'<dc:title>Bible de Y', content)
        self.assertIn(b'<dc:language>de</dc:language>', content)
        self.assertNotIn(b'<dc:language>fr</dc:language>', content)
        self.assertIn(b'<dc:identifier id="id">'
                      b'BYM-DE-01-Genese</dc:identifier>', content)
        self.assertNotIn(b'BYM-01-Genese', content)

    @unittest.skipIf(ebooklib is None, 'EbookLib is not installed')
    def test_md2epub_i18n_unknown(self):
        lang = 'unknown'
        filename = '01-Genese'
        function = func.md2epub
        src = 'md'
        dst = 'epub'
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        with self.assertRaises(KeyError):
            with mock.patch('ebooklib.epub.zipfile.ZipFile', MockZipFile()) as \
                    mck:
                function.main('{}.{}'.format(path, src),
                              '{}.{}'.format(path, dst), lang=lang)
        self.assertEqual(mck.files, [])
        self.assertEqual(mck.contents, {})

    def test_md2tex_harmonie_evangile_i18n(self):
        lang = 'de'
        filename = 'harmonie-evangile.md'
        function = func.md2tex_harmonie_evangile
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, path, lang=lang)
        self.assertIn('Titel & Matthäus (Mt) & Markus (Mk) & Lukas (Lk) '
                      '& Yohanan (Joh)', res)
        self.assertNotIn('Titre & Matthaios', res)
        self.assertNotIn('Titre & Matthieu', res)

    def test_md2tex_harmonie_evangile_i18n_unknown(self):
        lang = 'unknown'
        filename = 'harmonie-evangile.md'
        function = func.md2tex_harmonie_evangile
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        with self.assertRaises(KeyError):
            self._run(function, path, lang=lang)

    def test_tex2md_i18n(self):
        lang = 'de'
        filename = '01-Genese.tex'
        function = func.tex2md
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, path, lang=lang)
        self.assertIn('Kapitel', res)
        self.assertNotIn('Chapitre', res)

    def test_tex2md_i18n_unknown(self):
        lang = 'unknown'
        filename = '01-Genese.tex'
        function = func.tex2md
        path = os.path.normpath(os.path.join(FIXTURES, filename))
        res = self._run(function, path, lang=lang)
        self.assertNotIn('Kapitel', res)
        self.assertIn('Chapitre', res)

    def test_tex2osis_i18n(self):
        lang = 'de'
        function = func.tex2osis
        res = self._run(function, FIXTURES, lang=lang)
        self.assertIn('<title>Bibel von Yehoshua ha Mashiah</title>', res)
        self.assertNotIn('Bible Yéhoshua', res)
        self.assertIn('<identifier type="OSIS">Bible.BYM-DE</identifier>', res)
        self.assertNotIn('Bible.BYM</identifier>', res)

    def test_tex2osis_i18n_unknown(self):
        lang = 'unknown'
        function = func.tex2osis
        with self.assertRaises(KeyError):
            self._run(function, FIXTURES, lang=lang)

    # def test_txt2tex_glossary_i18n(self):
    #     lang = 'de'
    #     filename = 'glossaire.txt'
    #     function = func.txt2tex_glossary
    #     path = os.path.normpath(os.path.join(FIXTURES, filename))
    #     res = self._run(function, path, lang=lang)
    #     self.assertIn('text', res)

    # def test_txt2tex_glossary_i18n_unknown(self):
    #     lang = 'unknown'
    #     filename = 'glossaire.txt'
    #     function = func.txt2tex_glossary
    #     path = os.path.normpath(os.path.join(FIXTURES, filename))
    #     with self.assertRaises(KeyError):
    #         res = self._run(function, path, lang=lang)


if __name__ == '__main__':
    unittest.main()
