#!/usr/bin/env python3

"""TEX2JSON for dictionaries.

Generate Json dictionnaire from LaTeX files
"""

import os
import re
import sys
import argparse


LANGUAGES = ['fr', 'de']


EMPTY_VALUES = (None, '')
# TODO implement german books
BOOKS = {'Ge': 'GN', 'Ex': 'EX', 'Lé': 'LV', 'No': 'NU', 'De': 'DT',
         'Jos': 'JS', 'Jg': 'JG', 'Ru': 'RT', '1 S': 'S1', '2 S': 'S2',
         '1 R': 'K1', '2 R': 'K2', '1 Ch': 'R1', '2 Ch': 'R2', 'Esd': 'ER',
         'Né': 'NH', 'Est': 'ET', 'Job': 'JB', 'Ps': 'PS', 'Pr': 'PR',
         'Ec': 'EC', 'Ca': 'SS', 'Es': 'IS', 'Jé': 'JR', 'La': 'LM',
         'Ez': 'EK', 'Da': 'DN', 'Os': 'HS', 'Joë': 'JL', 'Am': 'AM',
         'Ab': 'OB', 'Jon': 'JH', 'Mi': 'MC', 'Na': 'NM', 'Ha': 'HK',
         'So': 'ZP', 'Ag': 'HG', 'Za': 'ZC', 'Mal': 'ML', 'Mt': 'MT',
         'Mc': 'MK', 'Lu': 'LK', 'Jn': 'JN', 'Ac': 'AC', 'Ro': 'RM',
         '1 Co': 'C1', '2 Co': 'C2', 'Ga': 'GL', 'Ep': 'EP', 'Ph': 'PP',
         'Col': 'CL', '1 Th': 'H1', '2 Th': 'H2', '1 Ti': 'T1', '2 Ti': 'T2',
         'Tit': 'TT', 'Phm': 'PM', 'Hé': 'HB', 'Ja': 'JM','1 Pi': 'P1',
         '2 Pi': 'P2' , '1 Jn': 'J1', '2 Jn': 'J2', '3 Jn': 'J3', 'Jud': 'JD',
         'Ap': 'RV'}


def parseref(bk, chap, vs, multivs):
    ref2 = ''
    ref = '<span class=\\"bibleref\\" data-id=\\"' + BOOKS[bk] + chap
    if vs:
        ref += '_' + vs
    ref += '\\">' + bk + '. ' + chap
    if vs:
        ref += ':' + vs
    ref += '</span>'
    
    if multivs:
        multivs = multivs.split(',')
        
        for val in multivs:
            if val:
                val = re.sub(r'^\s+', '', val)
                if ref2:
                    ref2 += ','
                ref2 += '<span class=\\"bibleref\\" data-id=\\"' + \
                        BOOKS[bk] + chap + '_' + val + '\\">' + val + '</span>'

    if ref2:
        return '%s,%s' % (ref, ref2)
    else:
        return ref


def repl(m):
    """Replace match verses by references."""
    m1, m2, m3, m4 = m.groups()
    return parseref(m1, m2, m3 or '', m4 or '')


def main(filename):
    tex = open(filename, 'r')
    print('{')
    print('\t"data": [')
    # pre-format line
    # parcours du livre
    for line in tex:
        # pre-formatage ligne
        line = line.strip()
        line = re.sub(r'\\up{(ème|er|ère)}', r'<sup>\1</sup>', line)
        line = re.sub(r'~', r' ', line)
        line = re.sub(r'\\%', '%', line)
        line = re.sub(r'"', r'\"', line)
        # quote
        line = re.sub(r'\\textasciigrave{}', '`', line)
        line = re.sub(r'\\textquotesingle{}', "'", line)
        # footnote
        line = re.sub(r'{\\interfootnotelinepenalty=100 +\\Note{([^}]+)}}',
                      r'\\Note{\1}', line)
        line = re.sub(r'\\vref\{\b([1|2|3]?\s?[a-zA-Zéë]+)\.(?:\s+(\d+))?'
                      '(?::(\d+(?:[-–]\d+)?)((?:,\s*\d+(?:[-–]\d+)?)*))?\}',
                      repl, line)
        line = re.sub(r'\\vref{(.*?)}', r'\1', line)

        if re.match(r'^\\DicoEntry\{(.*?)\}\\textit\{(.*?)\}', line):
            word, etymology = re.match(
                r'^\\DicoEntry\{(.*?)\}\\textit\{(.*?)\}', line).groups()
            etymology = re.sub(r'^,\s+', '', etymology)
            print('\t{')
            print('\t\t"word": "%s",' % word)
            print('\t\t"etymology": "%s",' % etymology)
        if re.match(r'^(?!(\\DicoEntry|\\begin|\\end))', line) and \
           not re.match(r'^\s*$', line):
            definition = line
            if definition.startswith('\\\\') or \
               definition.endswith('\\newline'):
                definition = re.sub(r'^\\\\', '<br/>', definition)
                definition = re.sub(r'\\newline', '<br/>', definition)
                print(definition, end='')
            else:
                print('\t\t"definition": "%s' % definition, end='')

        if not line:
            print('"\n\t},')
    tex.close()
    print('"\n\t}')
    print('\t]')
    print('}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert LaTeX file to Json for dictionaries')
    # parser.add_argument('filename',
    #                     help='LaTeX file Json')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANGUAGES)
    options = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/aides/'
    txt_dir = dirname + '/../output/' + options.lang + '/txt/'
    print(tex_dir)
    print(txt_dir)
    # get a list of all LaTeX books files
    tex_files = os.listdir(tex_dir)
    tex_files = [tex_file for tex_file in tex_files if
                 tex_file.endswith('dictionnaire.tex')]
    tex_files.sort()
    print(tex_files)

    orig_stdout = sys.stdout
    for tex_file in tex_files:
        with open(txt_dir + tex_file[:-4] + '.json', 'w') as txt:
            sys.stdout = txt
            main(tex_dir + tex_file)
    sys.stdout = orig_stdout
