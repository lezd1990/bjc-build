#!/usr/bin/env python3

"""TEX2MD

Generate Markdown formatted files from LaTeX files
"""

import os
import re
import sys
import argparse


LANGUAGES = ['fr', 'de']


def main(filename, lang='fr'):
    tex = open(filename, 'r')
    
    for line in tex:
        # pre-format line
        line = line.rstrip()
        line = re.sub(r'\\(?:Title)?Note{([^}]*)}', r'<!--\1-->', line)
        line = re.sub(r'\\TextDial{([^}]*)}', r'[\1]', line)
        line = re.sub(r'\\up{(ème|er)}', r'\1', line)
        line = re.sub(r'~', r' ', line)
        line = re.sub(r'\\%', '%', line)
        # quote
        line = re.sub(r'\\textasciigrave{}', '`', line)
        # footnote
        line = re.sub(r'{\\interfootnotelinepenalty=100 +\\Note{([^}]+)}}',
                      r'\\Note{\1}', line)

        # get book
        if line.startswith('\\ShortTitle'):
            book = re.sub(r'^\\ShortTitle{([^}]*)}\\BookTitle{([^}]*)}.*',
                          r'\2 (\1)', line)
            print('\n# ' + book)
            continue

        # get book infos
        if (line.startswith('\\\\Auteur') or
                line.startswith('\\\\(Heb') or
                line.startswith('\\\\(Gr') or
                line.startswith('\\\\Signification') or
                line.startswith('\\\\Thème') or
                line.startswith('\\\\Date de') or
                # german:
                line.startswith('\\\\Autor') or
                line.startswith('\\\\Bedeutung') or
                line.startswith('\\\\Thema') or
                line.startswith('\\\\Zeit der')):
            infos = re.sub(r'^\\\\([^\\]*)(?:\\\\}}?)?', r'\1', line)
            infos = re.sub(r'\\bigskip', '', infos)
            print('\n' + infos)
            continue

        line = re.sub(r'\\bigskip', '\n\n', line)

        # get book intro
        if line.startswith('\\\\'):
            intro = re.sub(r'^\\\\([^\\]*).*', r'\1', line)
            print('\n' + intro)
            continue

        # get title
        if line.startswith('\\TextTitle'):
            title = re.sub(r'^\\TextTitle{([^}]*)}', r'\1', line)
            print('\n### ' + title, end='\n\n')
            flag = False
            continue

        # get chapter num
        if line.startswith('\\Chap'):
            chap = re.sub(r'^\\Chap{(\d{1,3})}', r'\1', line)
            if lang == 'de':  # deutsh
                print('\n## Kapitel ' + chap)
            else:
                print('\n## Chapitre ' + chap)
            flag = True
            continue

        # get verse num and text
        if line.startswith('\\VerseOne') or line.startswith('\\VS'):
            if line.startswith('\\VerseOne'):
                verse = '1'
                text = re.sub(r'^\\VerseOne{}(.*)', r'\1', line)
                # if there is no title before the first verse
                if flag:
                    print()
                    flag = False
            if line.startswith('\\VS'):
                verse = re.sub(r'^\\VS{(\d{1,3})}.*', r'\1', line)
                text = re.sub(r'^\\VS{\d{1,3}}(.*)', r'\1', line)

            # write output
            print(chap + ':' + verse + '\t' + text)
            continue

    tex.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert Markdown file to LaTeX')
    # parser.add_argument('filename',
    #                     help='Markdown file to convert to LaTeX')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANGUAGES)
    options = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/'
    md_dir = dirname + '/../output/' + options.lang + '/md/src/'
    # get a list of all LaTeX books files
    tex_files = os.listdir(tex_dir)
    tex_files = [tex_file for tex_file in tex_files if tex_file.endswith('.tex')]
    tex_files.sort()

    orig_stdout = sys.stdout
    for tex_file in tex_files:
        with open(md_dir + tex_file[:-4] + '.md', 'w') as md:
            sys.stdout = md
            main(tex_dir + tex_file, lang=options.lang)
    sys.stdout = orig_stdout
