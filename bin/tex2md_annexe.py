#!/usr/bin/env python3

"""TEX2MD for annexes

Generate Markdown formatted files from LaTeX files
"""

import os
import re
import sys
import argparse


LANGUAGES = ['fr', 'de']


def main(filename):

    tex = open(filename, 'r')
    for line in tex:
        # pre-format line
        line = line.rstrip()
        line = re.sub(r'\\clearpage', '---\n', line)
        line = re.sub(r'\\clearpage', '***\n', line)
        line = re.sub(r'\\clearpage', '___\n', line)
        line = re.sub(r'\\up{(ème|er|ère)}', r'\1', line)
        line = re.sub(r'([^\\])~', r'\1 ', line)
        line = re.sub(r'\\+~', '~', line)
        line = re.sub(r'\\%', '%', line)
        # quote
        line = re.sub(r'\\textasciigrave{}', '`', line)
        # footnote
        line = re.sub(r'{\\interfootnotelinepenalty=100 +\\Note{([^}]+)}}',
                      r'\\Note{\1}', line)
        # exposant
        line = re.sub(r'\\up{([^}]+)}', r'<sup>\1</sup>', line)
        # TODO \item
        line = re.sub(r'\\begin{quote}', '\n\n', line)
        line = re.sub(r'\\end{quote}', '\n', line)
        line = re.sub(r'\\begin{small}', '', line)
        line = re.sub(r'\\end{small}', '', line)
        line = re.sub(r'\\begin{flushright}',
                      '\n<div style="text-align: right">', line)
        line = re.sub(r'\\end{flushright}', '</div>', line)
        line = re.sub(r'\\begin{(.+)}{(.+)}\\end{\1}', r'\2', line)
        line = re.sub(r'\\textbf{([^}]+)}', r'__\1__', line)  # gras
        line = re.sub(r'\\textit{([^}]+)}', r'*\1*', line)  # italique
        line = re.sub(r'\\emph{([^}]+)}', r'*\1*', line)  # emphase
        line = re.sub(r'\\bigskip', '\n\n', line)  # linebreak
        line = re.sub(r'\\newline', '\n', line)  # carriage return

        if not line:
            continue

        # list
        if re.match(r'^[0-9]+-.*', line):
            if not line.endswith('\n'):
                print(line)
                continue

        # title
        if line.startswith('\LARGE'):
            line = re.sub(r'\\LARGE (.+)', r'# \1', line)
            print(line, end='\n\n')
            continue

        # subtitle
        if line.startswith('\section'):
            line = re.sub(r'\\section\*{(.+)}', r'## \1', line)
            print('\n\n' + line, end='\n\n')
            continue

        # subsection
        if line.startswith('\subsection'):
            line = re.sub(r'\\subsection\*{(.+)}', r'### \1', line)
            print('\n\n' + line, end='\n\n')
            continue

        # sub subsection
        if line.startswith('\subsubsection'):
            line = re.sub(r'\\subsubsection\*{(.+)}', r'#### \1', line)
            print('\n\n' + line, end='\n\n')
            continue

        print(line, end='')

    tex.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert Markdown file to LaTeX for annexes')
    # parser.add_argument('filename',
    #                     help='Markdown file to convert to LaTeX')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANGUAGES)
    options = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/annexes/'
    md_dir = dirname + '/../output/' + options.lang + '/md/src/annexes/'
    # get a list of all LaTeX books files
    tex_files = os.listdir(tex_dir)
    tex_files = [tex_file for tex_file in tex_files
                 if (tex_file.endswith('.tex') and
                     not tex_file.startswith('dictionnaire') and
                     not tex_file[0].isdigit())]  # Ex: 01-Genese.tex
    print(tex_files)
    tex_files.sort()

    orig_stdout = sys.stdout
    for tex_file in tex_files:
        with open(md_dir + tex_file[:-4] + '.md', 'w') as md:
            sys.stdout = md
            main(tex_dir + tex_file)
    sys.stdout = orig_stdout
