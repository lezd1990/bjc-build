#!/usr/bin/env python3

"""TEX2MD for dictionaries

Generate Markdown formatted dictionnaries from LaTeX files
"""

import os
import re
import sys
import argparse


LANGUAGES = ['fr', 'de']


def main(filename):
    tex = open(filename, 'r')
    # pre-format line
    for line in tex:
        line = re.sub(r'\\begin.*\n', r'', line)
        line = re.sub(r'\\end{mult.*\n', r'', line)
        line = re.sub(r'~', r' ', line)
        line = re.sub(r'\\up{(ème|er|ère)}', r'\1', line)
        line = re.sub(r'\\%', '%', line)
        # quote
        line = re.sub(r'\\textasciigrave{}', '`', line)
        # footnote
        line = re.sub(r'{\\interfootnotelinepenalty=100 +\\Note{([^}]+)}}',
                      r'\\Note{\1}', line)
        # get book infos \\textit{([^}]*)}.*
        if (line.startswith('\\DicoEntry')):
            dicoentry = re.sub(r'^\\DicoEntry{([^}]*)}\\textit{([^}]*)}.*', r'#\1\n##\2', line)
            print(dicoentry, end='')
        else :
            dicotext = re.sub(r'(\\vref{([^}]+)})', r'<!--\2-->', line)
            dicotext = re.sub(r'\\\\-', r'+', dicotext)
            dicotext = re.sub(r'\\\\([^\d])', r'   + \1', dicotext)
            dicotext = re.sub(r'^\\\\(\d\.)', r'\1', dicotext)
            print(dicotext, end='')
        continue
    tex.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert Markdown file to LaTeX for dictionaries')
    # parser.add_argument('filename',
    #                     help='Markdown file to convert to LaTeX')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANGUAGES)
    options = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/aides/'
    md_dir = dirname + '/../output/' + options.lang + '/md/src/aides/'

    print(tex_dir)
    # get a list of all LaTeX books files
    tex_files = os.listdir(tex_dir)
    # tex_files = [tex_file for tex_file in tex_files if tex_file.endswith('dictionnaire.tex')]
    tex_files = [tex_file for tex_file in tex_files if
                 (tex_file.startswith('dictionnaire') and
                  tex_file.endswith('.tex'))]
    tex_files.sort()

    orig_stdout = sys.stdout
    for tex_file in tex_files:
        with open(md_dir + tex_file[:-4] + '.md', 'w') as md:
            sys.stdout = md
            main(tex_dir + tex_file)
    sys.stdout = orig_stdout
