#!/usr/bin/env python3

"""TEX2OSIS

Generate an OSIS Bible formatted file from LaTeX files
"""

import argparse
import os
import sys
import re
import time


# OSIS books names
BOOKS = (
    # Torah
    'Gen', 'Exod', 'Lev', 'Num', 'Deut',
    # Neviim
    'Josh', 'Judg', '1Sam', '2Sam', '1Kgs', '2Kgs', 'Isa', 'Jer', 'Ezek',
    'Hos', 'Joel', 'Amos', 'Obad', 'Jonah', 'Mic', 'Nah', 'Hab', 'Zeph',
    'Hag', 'Zech', 'Mal',
    # Ketouvim
    'Ps', 'Prov', 'Job', 'Song', 'Ruth', 'Lam', 'Eccl', 'Esth', 'Dan', 'Ezra',
    'Neh', '1Chr', '2Chr',
    # Gospels
    'Matt', 'Mark', 'Luke', 'John',
    # Jesus Testament
    'Acts', 'Jas', 'Gal', '1Thess', '2Thess', '1Cor', '2Cor', 'Rom', 'Eph',
    'Phil', 'Col', 'Phlm', '1Tim', 'Titus', '1Pet', '2Pet', '2Tim', 'Jude',
    'Heb', '1John', '2John', '3John', 'Rev',
)
RIGHTS = 'CC-BY-NC-SA 4.0'
LANG_ATTR = {'fr': {'ref': 'BYM',
                    'title': 'Bible de Yéhoshoua ha Mashiah',
                    'description': ('Bible de Yéhoshoua ha Mashiah, édition 2020. '
                                    'Révision de la Bible à partir du texte '
                                    'de la Bible Martin 1744.')},
             'de': {'ref': 'BYM-DE',
                    'title': 'Bibel von Yehoshua ha Mashiah',
                    'description': 'Bibel von Yehoshua ha Mashiah'},
}



def main(tex_dir, lang='fr'):
    if lang not in LANG_ATTR:
        raise KeyError('Language "{}" is not implemented'.format(lang))
    ref = LANG_ATTR[lang]['ref']
    title = LANG_ATTR[lang]['title']
    description = LANG_ATTR[lang]['description']
    # get a list of all LaTeX books files
    tex_files = os.listdir(tex_dir)
    tex_files = [tex_file for tex_file in tex_files if re.match('[0-9]{2}-.+\.tex',
                                                                tex_file)]
    tex_files.sort()

    # OSIS output
    # print header
    print('<?xml version="1.0" encoding="UTF-8"?>\n<osis ' +
          'xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace"' +
          ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
          'xsi:schemaLocation="http://www.bibletechnologies.net/2003/' +
          'OSIS/namespace http://www.bibletechnologies.net/' +
          'osisCore.2.1.1.xsd">')
    print('<osisText osisIDWork="{}" osisRefWork="Bible" '.format(ref) +
          'xml:lang="{}">'.format(lang))
    print('<header>')
    print('\t<work osisWork="{}">'.format(ref))
    print('\t\t<title>{}</title>'.format(title))
    print('\t\t<date>' + time.strftime('%Y-%m-%d') + '</date>')
    print('\t\t<description>{}</description>'.format(description))
    print('\t\t<type type="OSIS">Bible</type>')
    print('\t\t<identifier type="OSIS">Bible.{}</identifier>'.format(ref))
    print('\t\t<source>http://www.bibledejesuschrist.org/</source>')
    print('\t\t<rights>{}</rights>'.format(RIGHTS))
    print('\t</work>')
    print('</header>')

    for tex_file in tex_files:
        intro = False
        chap_num = False

        # get book id
        book_id = tex_file[:2]
        book_abbr = BOOKS[int(book_id) - 1]

        # write
        print('<div type="book" osisID="' + book_abbr + '">')
    
        with open(tex_dir + '/' + tex_file, 'r') as tex:
            for tex_line in tex:
                # pre-format line
                tex_line = tex_line.rstrip()
                tex_line = re.sub(r'\\TextDial{(.[^}]*)}', r'[\1]', tex_line)
                # quote
                tex_line = re.sub(r'\\textasciigrave{}', '`', tex_line)
                tex_line = re.sub(r'\\textquotesingle{}', "'", tex_line)
                # footnote
                tex_line = re.sub(r'{\\interfootnotelinepenalty=100 +\\Note{([^}]+)}}',
                                  r'\\Note{\1}', tex_line)
                tex_line = re.sub(r'~', r' ', tex_line)
                tex_line = re.sub(r'\\%', r'%', tex_line)
    
                # get book name
                if tex_line.startswith('\\ShortTitle'):
                    book_name = re.sub(r'\\ShortTitle{.*?}\\BookTitle' +
                                       r'{(.[^}]*)}\\.*$', r'\1', tex_line)
                    print('<title type="main">' + book_name + '</title>')
    
                # get book introduction
                if tex_line.startswith('\\textit{'):
                    intro = True
                    print('<div type="introduction">', end='')
                if intro and tex_line.startswith('\\\\'):
                    tex_line = re.sub(r'^\\\\', r'', tex_line)
                    tex_line = re.sub(r'\\up{(ème|er|ère)}',
                                      r'<hi type="super">\1</hi>',
                                      tex_line)
                    if tex_line.endswith('\\\\}') or \
                            tex_line.endswith('\\bigskip'):
                        intro = False
                        tex_line = re.sub(r'(\\\\}|\\bigskip)$',
                                          r'</div>',
                                          tex_line)
                    print(tex_line)

                # get chapter num
                if tex_line.startswith('\\Chap'):
                    if chap_num:
                        print('\t</chapter>')  # end previous chapter
                    chap_num = re.sub(r'^\\Chap{(\d{1,3})}', r'\1', tex_line)
                    print('\t<chapter osisID="' + book_abbr +
                          '.' + chap_num + '">')

                if tex_line.startswith('\\TextTitle'):
                    tex_line = re.sub(r'\\TextTitle{(.[^}]*)}', r'\1',
                                      tex_line)
                    tex_line = re.sub(r'\\(?:Title)?Note{(?:.*)?}', r'',
                                      tex_line)
                    print('\t\t<title>' + tex_line + '</title>')

                # get verse num and text
                if tex_line.startswith('\\VerseOne') or \
                        tex_line.startswith('\\VS'):
                    if tex_line.startswith('\\VerseOne'):
                        vers_num = '1'
                        vers_txt = re.sub(r'^\\VerseOne{}(.*)', r'\1',
                                          tex_line)
                    if tex_line.startswith('\\VS'):
                        vers_num = re.sub(r'^\\VS{(\d{1,3})}.*', r'\1',
                                          tex_line)
                        vers_txt = re.sub(r'^\\VS{\d{1,3}}(.*)', r'\1',
                                          tex_line)
                    # get strings
                    match_strongs_ml = re.search(r'\\bymStrong{(.[^}]*)}{(.[^}]*)}{(.[^}]*)}',
                                              vers_txt)
                    if match_strongs_ml:
                        vers_txt = re.sub(r'\\bymStrong{(.[^}]*)}{(.[^}]*)}{(.[^}]*)}',
                                          r'\1\2\3',
                                          vers_txt)
                    # get notes
                    match_note_ml = re.search(r'\\FTNT{.[^}]*\\\\[\-1-9]',
                                              vers_txt)
                    if match_note_ml:
                        vers_txt = re.sub(r'\\\\', r'\n', vers_txt)
                        vers_txt = re.sub(r'^([\-1-9])(\s?.*)$',
                                          r'<item><label>\1</label>\2</item>',
                                          vers_txt,
                                          flags=re.M)
                        vers_txt = re.sub(r'\\Note{(.[^}]*)}',
                                          r'<note type="study">\1</note>',
                                          vers_txt)
                        vers_txt = re.sub(r'(</note>)(.*)(</item>)',
                                          r'\3\1\2',
                                          vers_txt)
                        vers_txt = re.sub(r'<item>(.*)',
                                          r'<list>\n<item>\1',
                                          vers_txt,
                                          flags=re.S)
                        vers_txt = re.sub(r'(.*)</item>',
                                          r'\1</item>\n</list>',
                                          vers_txt,
                                          flags=re.S)
                    vers_txt = re.sub(r'\\Note{(.[^}]*)}',
                                      r'<note type="study">\1</note>',
                                      vers_txt)

                    print('\t\t<verse osisID=\"' + book_abbr + '.' +
                          chap_num + '.' + vers_num + '" sID="' +
                          book_abbr + '.' + chap_num + '.' +
                          vers_num + '"/>' + vers_txt + '<verse eID="' +
                          book_abbr + '.' + chap_num + '.' +
                          vers_num + '"/>')

            print('\t</chapter>')
            print('</div>')
    print('</osisText>')
    print('</osis>', end='')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert LaTex file to OSIS')
    parser.add_argument('source', help='LaTex files directory')
    parser.add_argument('output', help='OSIS file')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANG_ATTR.keys())
    args = parser.parse_args()

    tex_dir = args.source
    osis_file = args.output

    orig_stdout = sys.stdout
    with open(osis_file, 'w') as fout:
        sys.stdout = fout
        main(tex_dir, lang=args.lang)
    sys.stdout = orig_stdout
