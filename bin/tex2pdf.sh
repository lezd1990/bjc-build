#!/bin/bash
# Generate PDF file from LaTeX files

function main() {
    if [[ -z ${1} ]] || [[ -z ${2} ]]; then
        echo -e "Usage:\n\n\t${0} input.tex ouput.pdf\n"
        exit 1
    fi

    local JOBNAME="pre_bjc"
    local TEX=${1}
    local PDF=${2}
    local PDF_DIR=$(dirname ${PDF})
    local XELATEX="xelatex --output-directory=${PDF_DIR}"
    local MAKEINDEX="makeindex ${PDF_DIR}/${JOBNAME}.nlo -s nomencl.ist -o ${PDF_DIR}/${JOBNAME}.nls"
    cd ${PDF_DIR};
    rm -f ${JOBNAME}.ilg ${JOBNAME}.log ${JOBNAME}.nlo ${JOBNAME}.toc ${JOBNAME}.out ${JOBNAME}.aux ${JOBNAME}.nls ${JOBNAME}.pdf;
    rm -f ${JOBNAME}.lof ${JOBNAME}.lot
    cd -
    ${XELATEX} -jobname=${JOBNAME} ${TEX}
    ${MAKEINDEX}
    ${XELATEX} -jobname=${JOBNAME} ${TEX}
    ${XELATEX} -jobname=${JOBNAME} ${TEX}
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "${@}"
fi
