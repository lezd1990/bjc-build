#!/usr/bin/env python3

"""TEX2TEXT concordance

Generate text files (txt/csv) from LaTeX files
"""

import os
import re
import collections
from optparse import OptionParser


def sort(outputs):
    """Sort outputs."""
    import locale
    # useful to sort accented characters
    locale.setlocale(locale.LC_ALL, 'fr_FR.UTF8')
    ret = collections.OrderedDict([])
    keys = sorted(outputs.keys(), key=locale.strxfrm)
    for key in keys:
        ret.update({key: outputs[key]})
    return ret


def write(output):
    """Write output."""
    txt.write(output['word'] +  # term
              '\t' +
              # search pattern
              output['word'] +
              '\t' +
              # category
              '\t' +
              # list of verses
              ';'.join(output['verses']) +
              '\t' +
              # legend
              '\\\\'.join(output['items']) +
              '\n')


def main(tex_dir, txt_dir, tex_file='concordance.tex'):
    print(txt_dir)
    print(tex_dir)
    
    tex = open(tex_dir + tex_file, 'r')
    txt = open(txt_dir + tex_file[:-4] + '.txt', 'w')
    
    outputs = {}
    output = {}
    
    for line in tex:
        line = line.strip()
        # quote
        line = re.sub(r'\\textasciigrave{}', '`', line)
        line = re.sub(r'\\textquotesingle{}', "'", line)
        # footnote
        line = re.sub(r'{\\interfootnotelinepenalty=100 +\\Note{([^}]+)}}',
                      r'\\Note{\1}', line)
        if line.startswith('\ConcordanceEntry'):
            if output:
                outputs[output['word']] = output.copy()
                output = {}
            line = re.sub(r'\\ConcordanceEntry{', '', line)
            line = re.sub('}', '', line)
            word = line
            output = {'word': line, 'verses': [], 'items': []}
        elif line.startswith('\item['):
            # Ex: \item[\vref{Ex 7:7}] qui lui enfanta A. et Moïse ; les
            line = re.sub(r'\\item\[\\vref{', '', line)
            line = line.split('}')[0]
            output['verses'].append(line)
        elif line.startswith('\item '):
            line = re.sub(r'\\item +', '', line)
            output['items'].append(line)
    
    if output:
        outputs[output['word']] = output.copy()
        output = {}
    outputs = sort(outputs)
    for out in outputs.values():
        write(out)
    
    tex.close()
    txt.close()


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-l", "--lang", dest="lang",
                      help="language. Default: 'fr' (french)", default="fr",
                      choices=['fr', 'de'])
    (options, args) = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/aides/'
    txt_dir = dirname + '/../output/' + options.lang + '/txt/src/aides/'
    main(tex_dir, txt_dir)
