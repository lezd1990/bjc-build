#!/usr/bin/env python3

"""TXT2TEX: CONCORDANCE

Generate a concordance from md files, and base on a list of words (csv/txt).
"""

import os
import re
from optparse import OptionParser


# regex verbes "er"
regex_vb_er = '(?:'
regex_vb_er += '[est]|es|ai[est]?|[i]?ons|[i]?ez|(?:ai)?ent'
regex_vb_er += '|é[e]?[s]?|ant[s]?'
regex_vb_er += '|er(?:a(?:i|s)?[st]?|[i]?on[st]|[i]?ez|aient)?'
regex_vb_er += '|(?:a(?:i|s)?|â(m|t)(?:es)?|èrent)'
regex_vb_er += ')'

# regex verbes "i"
regex_vb_ir = '(?:'
regex_vb_ir += 'i[st]|isse[s]?|issai[est]?|iss[i]?ons|iss[i]?ez|iss(?:ai)?ent'
regex_vb_ir += '|i[e]?[s]?|(?:iss)?ant[s]?|ue?s?'
regex_vb_ir += '|ir(?:a(?:i|s)?[st]?|[i]?on[st]|[i]?ez|aient)?|î(m|t)es|irent'
regex_vb_ir += ')'

# regex verbes "re"
regex_vb_re = '(?:'
regex_vb_re += '[st]?|ai[est]?|[i]?ons|[i]?ez|(?:ai)?ent|e[s]?'
regex_vb_re += '|u[e]?[s]?|i[et]?[s]?|ant[s]?'
regex_vb_re += '|r[e]?(?:a(?:i|s)?[st]?|[i]?on[st]|[i]?ez|aient)?'
regex_vb_re += '|î(m|t)es|irent'
regex_vb_re += ')'

# abbreviations livres
BOOKS = (
    'Ge', 'Ex', 'Lé', 'No', 'De', 'Jos', 'Jg', '1 S', '2 S', '1 R', '2 R',
    'Es', 'Jé', 'Ez', 'Os', 'Joë', 'Am', 'Ab', 'Jon', 'Mi', 'Na', 'Ha', 'So',
    'Ag', 'Za', 'Mal', 'Ps', 'Pr', 'Job', 'Ca', 'Ru', 'La', 'Ec', 'Est', 'Da',
    'Esd', 'Né', '1 Ch', '2 Ch', 'Mt', 'Mc', 'Lu', 'Jn', 'Ac', 'Ja', 'Ga',
    '1 Th', '2 Th', '1 Co', '2 Co', 'Ro', 'Ep', 'Ph', 'Col', 'Phm', '1 Ti',
    'Tit', '1 Pi', '2 Pi', '2 Ti', 'Jud', 'Hé', '1 Jn', '2 Jn', '3 Jn', 'Ap'
)

# regex pattern to match verses
RE_VERSE = re.compile(r'^([0-9]+):([0-9]+)\t(.+)')

RE_COMMENTS = re.compile(r'<[^<]+>')  # remove comments
RE_PONCTUATIONS = re.compile(
    (r'('
     '« *|'  # remove first level left double quotes
     ' *»|'  # remove first level right double quotes
     '“ *|'  # remove second level left double quotes
     ' *”'  # remove second level right double quotes
')'))
RE_SPACES = re.compile(r' +')
RE_QUOTE = re.compile(r"'")
RE_HYPHEN = re.compile(r'-')
RE_QUOTE_INVERSE = re.compile(r'888')
RE_HYPHEN_INVERSE = re.compile(r'999')

RE_ANTISLASHES = re.compile(r'\\\\')

RE_INFINITIVE = re.compile(r'(er|ir|re)$')

RE_SHORTEN_COMME = re.compile(r'\bcomme\b')
RE_SHORTEN_DONC = re.compile(r'\bdonc\b')
RE_SHORTEN_DANS = re.compile(r'\bdans\b')
RE_SHORTEN_DEVANT = re.compile(r'\bdevant\b')
RE_SHORTEN_FEMME = re.compile(r'\bfemmes?\b')
RE_SHORTEN_HOLOCAUSTE = re.compile(r'\bholocauste\b')
RE_SHORTEN_HOMME = re.compile(r'\bhommes?\b')
RE_SHORTEN_HUMAIN = re.compile(r'\bhumains?\b')
RE_SHORTEN_JERUSALEM = re.compile(r'\bJérusalem\b')
RE_SHORTEN_YEROUSHALAIM = re.compile(r'\bYeroushalaim\b')
RE_SHORTEN_LEUR = re.compile(r'\bleur\b')
RE_SHORTEN_JUSQUE = re.compile(r'\bjusque\b')
RE_SHORTEN_MEME = re.compile(r'\bmême\b')
RE_SHORTEN_MAINTENANT = re.compile(r'\bmaintenant\b')
RE_SHORTEN_NOUS = re.compile(r'\bnous\b')
RE_SHORTEN_ORDONNANCE = re.compile(r'\bordonnance\b')
RE_SHORTEN_QUAND = re.compile(r'\bquand\b')
RE_SHORTEN_QUELQUE = re.compile(r'\bquelques?\b')
RE_SHORTEN_SEIGNEUR = re.compile(r'\bSeigneur\b')
RE_SHORTEN_SOUVERAIN = re.compile(r'\bsouverain\b')
RE_SHORTEN_TOUS = re.compile(r'\btous\b')
RE_SHORTEN_TOUT = re.compile(r'\btout\b')
RE_SHORTEN_TOUTE = re.compile(r'\btoute\b')
RE_SHORTEN_TOUTES = re.compile(r'\btoutes\b')
RE_SHORTEN_VETEMENT = re.compile(r'\bvêtements?\b')
RE_SHORTEN_VOUS = re.compile(r'\bvous\b')
RE_SHORTEN_END_DOUBLE_DOT = re.compile(r'\.\.$')

# perl compatibility
RE_LOOKBEHIND = re.compile(r'\\K')


def sort(refs):
    """Sort list reference of verses according to bible orders."""
    def sort_by(content):
        content = content.rsplit(' ', 1)
        book = content[0]
        chap, num = map(int, content[-1].split(':'))
        return (BOOKS.index(book), chap, num)

    ret = sorted(
        refs,
        key=sort_by
    )
    return ret


def cleanup(text):
    """Remove french quote and comments."""
    text = RE_COMMENTS.sub('', text)
    text = RE_PONCTUATIONS.sub('', text)
    text = RE_SPACES.sub(' ', text)
    text = text.strip()
    return text


def replace_sc(text):
    """Replace specials characters

    Quotes are replaced by 888.
    Hypens are replaced by 999.

    Need a string argument.
    Return a string.
    """

    text = RE_QUOTE.sub('888', text)
    text = RE_HYPHEN.sub('999', text)

    return text


def restore_sc(text):
    """Restore specials characters

    Restore 888 as quotes.
    Restore 999 as hypens.

    Need a string argument.
    Return a string.
    """

    text = RE_QUOTE_INVERSE.sub("'", text)
    text = RE_HYPHEN_INVERSE.sub('-', text)

    return text


def shorten(text):
    """Shorten some words

    Need a string argument.
    Return a string.
    """
    text = RE_SHORTEN_COMME.sub(r'com.', text)
    text = RE_SHORTEN_DONC.sub(r'dc', text)
    text = RE_SHORTEN_DANS.sub(r'ds', text)
    text = RE_SHORTEN_DEVANT.sub(r'dvt', text)
    text = RE_SHORTEN_FEMME.sub(r'fem.', text)
    text = RE_SHORTEN_HOLOCAUSTE.sub(r'holoc.', text)
    text = RE_SHORTEN_HOMME.sub(r'hom.', text)
    text = RE_SHORTEN_HUMAIN.sub(r'hum.', text)
    text = RE_SHORTEN_JERUSALEM.sub(r'Jérus.', text)
    text = RE_SHORTEN_YEROUSHALAIM.sub(r'Yerous.', text)
    text = RE_SHORTEN_LEUR.sub(r'lr.', text)
    text = RE_SHORTEN_JUSQUE.sub(r'jsq.', text)
    text = RE_SHORTEN_MEME.sub(r'mm', text)
    text = RE_SHORTEN_MAINTENANT.sub(r'mntnt', text)
    text = RE_SHORTEN_NOUS.sub(r'ns', text)
    text = RE_SHORTEN_ORDONNANCE.sub(r'ordon.', text)
    text = RE_SHORTEN_QUAND.sub(r'qd', text)
    text = RE_SHORTEN_QUELQUE.sub(r'qq.', text)
    text = RE_SHORTEN_SEIGNEUR.sub(r'Seign.', text)
    text = RE_SHORTEN_SOUVERAIN.sub(r'souv.', text)
    text = RE_SHORTEN_TOUS.sub(r'ts', text)
    text = RE_SHORTEN_TOUT.sub(r'tt', text)
    text = RE_SHORTEN_TOUTE.sub(r'tte', text)
    text = RE_SHORTEN_TOUTES.sub(r'ttes', text)
    text = RE_SHORTEN_VETEMENT.sub(r'vêt.', text)
    text = RE_SHORTEN_VOUS.sub(r'vs', text)
    text = text.strip()
    text = RE_SHORTEN_END_DOUBLE_DOT.sub(r'.', text)
    return text


def get_regex(category, search):
    """
    Retourne la regex adaptee au type de mot cherche
    """
    # preserve la compatibilite avec les regex Perl du fichier de mots
    search = RE_LOOKBEHIND.sub('', search)

    # application d'une regex en fonction du type de mot
    if category == 'v':
        search, infinitive, dummy = RE_INFINITIVE.split(search)
        if infinitive == 'er':
            regex_vb = regex_vb_er
        elif infinitive == 'ir':
            regex_vb = regex_vb_ir
        elif infinitive == 're':
            regex_vb = regex_vb_re
        search += regex_vb
    elif category in ('n', 'adj'):
        if search.endswith('eux'):
            search = search[:-1] + '(?:x|se[s]?)'
        elif not search.endswith('?'):
            search = search + '[e]?[sx]?'

    # compilation de la regex
    regex = re.compile(r'\b%s\b' % search, re.IGNORECASE)

    return regex, search


def get_context(match, text):
    # recupere la position du mot dans le verset
    word = match.group(0)
    start, end = match.span()

    # calcul le nombre de mots necessaires au contexte
    if start < 10:
        b = '3'
        a = '6'
    elif end > len(text)-10:
        b = '6'
        a = '3'
    else:
        b = '3'
        a = '3'

    # recupere les elements du contexte qui precedent le mot
    before = text[:start]
    before = replace_sc(before)
    before = before[::-1]
    before = re.sub(r'((?:(?:\W+)(?:\w+)?){' + b + '}).*', r'\1', before)
    before = before[::-1]
    before = restore_sc(before)
    # recupere les elements du contexte qui suivent le mot
    after = text[end:]
    after = replace_sc(after)
    after = re.sub(r'((?:(?:\W+)?(?:\w+)){' + a + '}).*', r'\1', after)
    after = restore_sc(after)

    # reconstruit le mot et son contexte
    context = before + word[:1] + '.' + after
    context = shorten(context)

    return context


class Bible(object):
    """Manage bible."""

    notfound = None
    bible = None

    def open(self, md_dir, md_files, notfound_file):
        # Write bible verses on a single file."""
        bible = {}
        # parcours des fichiers md
        for md_file in md_files:
            md = open(md_dir + md_file, 'r')
            for line in md:
                line = line.strip()
                content = RE_VERSE.match(line)
                if not content:
                    continue
                # recupere les elements de la liste
                book = int(md_file[:2])  # numero du livre
                chapter, verse, text = content.groups()

                # recupere le nom du livre et construit la reference complete
                name = BOOKS[book - 1]
                ref = name + ' ' + chapter + ':' + verse
                bible[ref] = cleanup(text)
            md.close()
        self.bible = bible
        self.notfound = open(notfound_file, 'w')
        return True

    def close(self):
        """Close file."""
        self.notfound.close()
        return True

    def browse(self, term, search, refs, regex, concordance):
        """Browse the bible."""
        occurrences = 0
        errors = {}
        result = []
        refs = list(filter(None, refs))
        for ref in sort(refs):
            # test la regex sur le texte du verset en cours
            try:
                text = self.bible[ref]
            except KeyError:
                errors[ref] = ''
                continue

            match = regex.search(text)

            # si le motif de la regex n'a pas ete trouve dans le verset
            # on log et on passe à li'teration suivante
            if not match:
                errors[ref] = text
                continue

            # incrmentation du comtpeur d'occurrences trouves
            occurrences += 1
            # contexte
            context = get_context(match, text)
            result.append((ref, context))

        # si les compteur different, log anomalie
        self.log_notfound(occurrences, term, refs, search, errors)
        return result

    def log(self, msg):
        """Log message."""
        self.notfound.write(msg)

    def log_notfound(self, occurrences, term, refs, search, errors):
        """Log not found refs."""
        count = len(refs)  # compteur du nb de references

        # si les compteur different, log anomalie
        if occurrences != count:
            missing = count - occurrences
            self.log('\n' + term + ':' +
                     '\n\t- regex: \\b' + search + '\\b' +
                     '\n\t- nb_ref: ' + str(count) +
                     '\n\t- found: ' + str(occurrences) +
                     '\n\t- missing: ' + str(missing) +
                     '\n\t- success rate: ' \
                     + str(round((occurrences / count) * 100)) + '%'
                     '\n\t- refs:\n')
            for ref in errors:
                self.log('\t\t> ' + ref + ' : ' + errors[ref] + '\n')


def write_legend(concordance, line):
    # recuperation et ecriture de la legende du mot, si existante
    if len(line) > 4:
        legend = line[4]
        legend = RE_ANTISLASHES.sub(r'\n\\item ', legend)
        concordance.write('\\begin{legend}\n')
        concordance.write('\\NoAutoSpaceBeforeFDP{\n')
        concordance.write('\\item ')
        concordance.write(legend + '\n')
        concordance.write('}\n\\end{legend}\n')


def main(md_dir, concordance_file, words_file, notfound_file):
    md_files = os.listdir(md_dir)
    md_files = [md_file for md_file in md_files
                if (md_file.endswith('.md') and
                    md_file[0].isdigit())]  # Ex: 01-Genese.md
    md_files.sort()
    print(md_files)
    print(concordance_file)
    # recupere tous les mots dans une liste
    words = open(words_file, 'r')
    bible = Bible()
    bible.open(md_dir, md_files, notfound_file)

    # fichier de log
    bible.log('### Journal des erreurs ###\n')

    # ouverture du fichier de sortie
    concordance = open(concordance_file, 'w')
    # ecriture en-tetes du fichier LaTeX
    concordance.write('\\begin{multicols}{3}\n')
    concordance.write('{\\fontsize{8pt}{0.7em}\\selectfont\n')

    # parcours les mots de la liste
    for line in words:
        line = line.strip()
        if not line:
            continue
        line = line.split('\t')

        # recupere les elements du mot
        term = line[0]  # terme
        search = line[1]  # motif de recherche
        category = line[2]  # type de mot
        refs = line[3].split(';')  # liste de versets

        # application d'une regex en fonction du type de mot
        regex, search = get_regex(category, search)

        print(term)

        # parcours fichier et ecriture des references
        result = bible.browse(term, search, refs, regex, concordance)
        if result:
            # ecriture en-tetes du mot
            concordance.write('\n\\ConcordanceEntry{' + term + '}\n')
            concordance.write('\\vspace{-2mm}\n')
            concordance.write('\\begin{listverse}\n')
            for ref, context in result:
                # ecriture de la reference et le contexte
                concordance.write('\\item[\\vref{' + ref + '}] ' + context + '\n')
                # ecriture en-tetes de fermeture
            concordance.write('\\end{listverse}\n')

            # recuperation et ecriture de la legende du mot, si existante
            write_legend(concordance, line)

    # ecriture en-tetes
    concordance.write('}\n')
    concordance.write('\\end{multicols}\n')

    words.close()
    bible.close()
    concordance.close()


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-l", "--lang", dest="lang",
                      help="language. Default: 'fr' (french)", default="fr",
                      choices=['fr', 'de'])
    (options, args) = parser.parse_args()

    dirname = os.path.dirname(__file__)
    md_dir = dirname + '/../source/' + options.lang + '/'
    concordance_file = dirname + '/../output/' + options.lang + \
            '/tex/src/aides/concordance.tex'
    words_file = dirname + '/../annexes/' + options.lang + \
            '/aides/concordance.txt'
    notfound_file = dirname + '/../output/' + options.lang + \
            '/log/concordance.log'

    main(md_dir, concordance_file, words_file, notfound_file)
