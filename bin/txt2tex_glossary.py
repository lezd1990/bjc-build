#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""TXT2TEX: GLOSSARY

Generate a glossary tex from txt files (csv, ...).
"""

import os
import re
import sys
import argparse


LANGUAGES = ['fr', 'de']


def main(filename='glossaire.txt', lang='fr'):
    txt = open(filename, 'r')
    # \\begin{longtable}{ @{} *{4}{C{.20\\textwidth}} @{} }
    # \\begin{longtable}[t]{l>{\\raggedright} @{} *{3}{p{0.2\\linewidth}>{\\raggedright\\arraybackslash}} @{} }
    print('''\\begin{center}
    \\begin{longtable}[t]{l>{\\raggedright\\arraybackslash}p{0.2\\linewidth}>{\\raggedright\\arraybackslash}p{0.2\\linewidth}>{\\raggedright\\arraybackslash}p{0.2\\linewidth}}
    \\toprule
    % \\multicolumn{4}{c}{Glossaire}\\\\
    Bible & Hébreu/\\textit{Araméen} & Grec & Français \\\ \\hline \\hline
    \\endfirsthead
    \\multicolumn{4}{c}{Glossaire}\\\\
    Bible & Hébreu/\\textit{Araméen} & Grec & Français \\\\ \\hline \\hline\\\\
    \\endhead
    ''', end='')
    separator = '&'
    def get_col(content):
        content = content.strip()
        return content

    for line in txt:
        # quote
        line = re.sub(r'`', r'\\textasciigrave{}', line)
        line = re.sub(r"'", r'\\textquotesingle{}', line)
        cols = line.split(';')
        # les mots en arameens sont mis en italique
        value = get_col(cols[1])
        if value:
            value = re.sub(r'\*([^*]+)\*', r'\\textit{\1}', value)  # italic
        print('{}{}{}{}{}{}{}\\\\'.format(
            get_col(cols[0]), separator,
            value, separator,
            get_col(cols[2]), separator,
            get_col(cols[3])))
    print('\end{longtable}')
    print('\end{center}', end='')

    txt.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert text file to LaTeX for glossary')
    # parser.add_argument('filename',
    #                     help='Markdown file to convert to LaTeX')
    parser.add_argument('-l', '--lang',
                        help="language. Default: 'fr' (french)",
                        default='fr', choices=LANGUAGES)
    options = parser.parse_args()

    dirname = os.path.dirname(__file__)
    tex_dir = dirname + '/../output/' + options.lang + '/tex/src/aides/'
    txt_dir = dirname + '/../annexes/' + options.lang + '/aides/'
    filename = 'glossaire.txt'
    print(txt_dir)
    print(tex_dir)
    print(filename)

    orig_stdout = sys.stdout
    with open(tex_dir + filename[:-4] + '.tex', 'w') as tex:
        main(txt_dir + filename, lang=options.lang)
    sys.stdout = orig_stdout
