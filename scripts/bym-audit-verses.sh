#!/bin/bash
files="[0-9][0-9]-*.md"
versePattern="^\d{1,3}:\d{1,3}\t"
echo "Enter bjc directory path from here"

read fileDir
#fileDir=../bjc-source

cd $fileDir
echo "Counting books ..."
countBooks=$(ls $files | wc -l)

echo
echo "**Result of the number of books :"
echo $countBooks

echo
echo "Checking Chapter and Verse numbers ..."
echo "Counting...."
#TRYING WITH A WHILE LOOP
for f in $files; do
    echo "--------------------"
    echo $f
    countChapter=0
    countVerse=0
    count=0
    result=$(grep -Po "$versePattern" $f | cut -d":" -f1 | uniq -c | sed -e 's/^ *//;s/ /,/')
    for res in $result; do
        chapter=$(echo "$res" | cut -d"," -f2)
        count=$(echo "$res" | cut -d"," -f1)
        echo "## Chapitre $chapter"
        echo "Nb Verse $count"
        countVerse=`expr $countVerse + $count`
        countChapter=`expr $countChapter + 1`
    done
    echo "Book : $f"
    echo "Nb Chapitre $countChapter"
    echo "Nb Verse $countVerse"
done
