#!/bin/sh
# Vérifie tous les fichiers .md
#
# bash bym-check.sh
#
# author: Rodrigue (rodnsi)
# created at: 05/10/2018
# last updated: 10/05/2020 10:32:00
#
#
# Ex:
# Dans l'exemple ci-après en lancant le script on se rend compte qu'il y a deux points virgules (;) qui se suivent dans Marc 1:20 et qu'il y a un espace en trop après le petit et l'apostrophe dans 1 chroniques 11:41 :
# ```
# $ sh bjc-check.sh
# 41-Marc.md:1:20	Et immédiatement il les appela ; ; et, laissant leur père Zébédée dans la barque avec les ouvriers, ils le suivirent.
# (...)
# 38-1Chroniques.md:11:41	Urie le Héthien, Zabad fils d' Achlaï,
# (...)
# ```
# Astuce :
# Remplacer les doubles quotes (") par des guillemets (« »)
# sed -i -E "s/\" ?([a-zA-ZÉÊÈéêëèàâäôöòïîìùûüçœ,'’ ]+) ?\"/« \1 »/g" *.md;
# Remplacer les espaces insécables par un vrai espace
# sed -i 's/\xc2\xa0/ /g' *.md
# Remplacer les virgules par des virgules lorsque l'énumeration de versets proviennent d'un même livre.
# Ex: "Ap. 17:16-17 ; 18:8" devient "Ap. 17:16-17, 18:8"
# /!\ Faire une merge request pour checker car ca ne fonctionne pas totalement.
# Ex: "Hé. 5:5-6 ; 6:20 ; 7:1-2" devient "Hé. 5:5-6, 6:20 ; 7:1-2" au lieu de "Hé. 5:5-6, 6:20, 7:1-2"
# sed -i -E "s/([a-z]+\. [0-9]+:[-0-9]+) *(;) *([0-9]+:[-0-9]+)+/\1, \3/g" *md

echo "==== Trouver le mot 'peronne' au lieu de 'personne'"
grep --exclude README.md --color=always -n "peronne" *.md
echo "==== Trouver les titres de chapitre commençant par une minuscule"
grep --exclude README.md --color=always -n "# [a-zêéëèàâäôöïîùûüçœ]" *.md
echo "==== Trouver les espaces inutiles (les doubles espaces/fin de phrase)"
# Please run this command to fix: "sed -i 's/ $//g' *md"
#grep --exclude README.md --color=always ". $" *.md
grep --exclude README.md --color=always -i "?  " *.md
grep --exclude README.md --color=always -i ";  " *.md
grep --exclude README.md --color=always -i "  ;" *.md
grep --exclude README.md --color=always -i "\!  " *.md
grep --exclude README.md --color=always -i "  \!" *.md
grep --exclude README.md --color=always -i ":  " *.md
grep --exclude README.md --color=always -i "  :" *.md
echo "==== Trouver les tabulations en fin de phrase"
grep --exclude README.md --color=always -P "\t$" *.md
echo "==== Trouver les virgules collées avant une lettre"
grep --exclude README.md --color=always -i ",[[:alpha:]]" *.md
echo "==== Trouver les point-virgules collés avant un caractère alphanumérique"
grep --exclude README.md --color=always -i ";[[:alnum:]]" *.md
echo "==== Trouver les point-virgules collés après un caractère alphanumérique"
grep --exclude README.md --color=always -i "[[:alnum:]];" *.md
echo "==== Trouver les points-virgules avant un guillement fermant"
grep --exclude README.md --color=always -i "; *[»”]" *.md
echo "==== Trouver les point d'interrogation collés avant un caractère alphanumérique"
grep --exclude README.md --color=always -i "?[[:alnum:]»]" *.md
echo "==== Trouver les point d'interrogation collés après un caractère alphanumérique"
grep --exclude README.md --color=always -i "[[:alnum:]»]?" *.md
grep --exclude README.md --color=always -i "  ?" *.md
echo "==== Trouver les point d'exclamation collés après un caractère alphanumérique"
grep --exclude README.md --color=always -i "[[:alnum:]»]\!" *.md
echo "==== Trouver les point d'exclamation collés avant un caractère alphanumérique"
grep --exclude README.md --color=always -i "\![[:alnum:]]" *.md
echo "==== Trouver les deux points collés avant une lettre"
grep --exclude README.md --color=always -i ":[[:alpha:]«“]" *.md
echo "==== Trouver les deux points collés après une lettre"
grep --exclude README.md --color=always -i "[[:alpha:]»”]:" *.md
echo "==== Trouver les points collés au milieu de lettres"
grep --exclude README.md --color=always -i "[[:alpha:]][.][[:alpha:]]" *.md
echo "==== Trouver les points avant une lettre minuscule"
grep --exclude README.md "\. [a-zêéëèàâäôöïîùûüçœ]" *.md | grep -v "J\.-C\." | grep -v "Ps\. chapitre" | grep --color=always "\. [a-zêéëèàâäôöïîùûüçœ]"
echo "==== Trouver plusieurs espaces avant/après un point"
grep --exclude README.md --color=always "\.  " *.md
grep --exclude README.md --color=always "  \." *.md
echo "==== Trouver plusieurs espaces avant une paranthèses fermantes"
grep --exclude README.md --color=always -i "[[:alnum:]] )" *.md
grep --exclude README.md --color=always -i "  )" *.md
echo "==== Trouver plusieurs espaces avant une paranthèses ouvrantes"
grep --exclude README.md --color=always -i "( [[:alnum:]]" *.md
grep --exclude README.md --color=always -i "(  " *.md
echo "==== Trouver les espaces manquants avec un guillemet fermant"
grep --exclude README.md --color=always -i  "[[:alnum:]]»" *.md
grep --exclude README.md --color=always "  »" *.md
grep --exclude README.md --color=always "\.»" *.md
grep --exclude README.md --color=always "\!»" *.md
grep --exclude README.md --color=always "\>»" *.md
echo "==== Trouver les espaces manquants avec un guillemet ouvrant"
grep --exclude README.md --color=always -i "«[[:alnum:]]" *.md
grep --exclude README.md --color=always "«  " *.md
echo "==== Trouver plusieurs espaces avant/après une virgule"
grep --exclude README.md --color=always " ," *.md
grep --exclude README.md --color=always "  ," *.md
grep --exclude README.md --color=always ",  " *.md
echo "==== Trouver les ponctuations collées entre elles"
##### grep --exclude README.md --color=always " \." *.md
##### #grep --exclude README.md -P "\. *," *.md | grep -v "J.-C." | grep --color=always -P "\. *,"
##### grep --exclude README.md --color=always -P "[0-9]\. +;" *.md  # entre des énumérations de versets
##### grep --exclude README.md --color=always -P "[\.\?\,\;\!] *[\.\?\,\;\!]" *.md | grep -v "J.-C." | grep --color=always -P "[\.\?\,\;\!] *[\.\?\,\;\!]"
echo "==== etc. ne doit pas finir par trois points etc..."
grep --exclude README.md --color=always -i "etc\.\." *.md
echo "==== Trouver les balises de références mal férmées"
grep --exclude README.md --color=always "<--" *.md
#echo "==== Trouver les 'ou' dans les commentaires sans guillemet."
#grep --exclude README.md --color=always -i '<!--ou [^«]' *md
echo "==== Trouver les guillemets mal férmés"
grep --exclude README.md --color=always -P "«[^»<]+«" *.md
grep --exclude README.md --color=always -Pzo --binary-files=text "«[^»<]+[«#]" *.md
grep --exclude README.md --color=always -P "»[^«<>]+»" *.md
grep --exclude README.md --color=always -Pzo --binary-files=text "»[^«<>]+[»]" *.md
grep --exclude README.md --color=always -P "“[^”<]+[“«»]" *.md
grep --exclude README.md --color=always -Pzo --binary-files=text "“[^”<]+[“«»#]" *.md
grep --exclude README.md --color=always -P "”[^“]+”" *.md
grep --exclude README.md --color=always -Pzo --binary-files=text "”[^“]+”" *.md
grep -P --color=always "«[^»]*[A-Z][a-zé] [0-9]+:[0-9]+[^»]*»" *md
echo "==== Trouver les minuscules suivis d'une majuscule"
grep --exclude README.md --color=always "[a-zéêëèäâàçîïôöûüùò][A-ZÉÊÈËÔÖÎÏÀÄÂÇÛÜÙÒ]" *md
echo "==== Trouver les deux petits points (..) aulieu d'un point (.) au de trois petits points (...)"
grep --exclude README.md --color=always "[^.]\. *\.[^.]" *.md
grep --exclude README.md --color=always "[^.]\. *\.$" *.md
echo "==== Trouver les ponctuations écrits en double"
grep --exclude README.md --color=always -P "\? *\?" *.md
grep --exclude README.md --color=always -P "\! *\!" *.md
grep --exclude README.md --color=always -P ": *:" *.md
grep --exclude README.md --color=always -P ", *," *.md
grep --exclude README.md --color=always -P "\) *\)" *.md
grep --exclude README.md --color=always -P "\( *\(" *.md
grep --exclude README.md --color=always -P "; *;" *.md
grep --exclude README.md --color=always -P "« *«" *.md
grep --exclude README.md --color=always -P "» *»" *.md
grep --exclude README.md --color=always -P "” *”" *.md
grep --exclude README.md --color=always -P "“ *“" *.md
echo "==== Trouver les doubles quotes au lieu de guillemet"
grep --exclude README.md --color=always '"' *.md
echo "==== Trouver les minuscules après un guillemet"
grep --exclude README.md --color=always -P ": [«“‘][a-zêéëèàâäôöïîùûüçœ]" *.md
echo "==== Trouver les guillemets spéciales avec un espace en trop avant/après"
grep --exclude README.md --color=always '“ ' *.md
grep --exclude README.md --color=always ' ”' *.md
echo "==== Trouver les doubles chevrons qui devraient être remplacés par des guillemets dans les fichiers"
grep --exclude README.md --color=always -i "<<" *.md
grep --exclude README.md --color=always -i ">>" *.md
echo "==== Trouver les mauvais e dans l'o (œ). Ex: 'coeur' au lieu de 'cœur'"
grep --exclude README.md --color=always -i 'oeu' *.md
grep --exclude README.md --color=always -i 'oei' *.md
grep --exclude README.md --color=always -i 'oet' *.md
echo "==== Trouver les début de verset avec un espace au lieu d'une tabulation"
grep --exclude README.md --color=always -i -P "^[0-9]+:[0-9]+ " *.md
echo "==== Trouver les tabulation avec un espace en trop"
grep --exclude README.md --color=always -i -P "^[0-9]+:[0-9]+\t " *.md
echo "==== Trouver les mauvaises tabulations"
grep --exclude README.md --color=always -i -P "[\.,; \!\?\:][0-9]+:[0-9]+\t" *.md
echo "==== Trouver les lettres seules au milieu d'espaces"
grep --exclude README.md -i -P "[\t ][b-np-xzéêëèâäoöòïîìùûüçœ] " *.md | grep -v "Agrippa" | grep -v "Salmanasar" | grep --color=always -i -P "[\t ][b-np-xzéêëèâäoöòïîìùûüçœ] "
grep --exclude README.md --color=always -P "[\t ][OA] " *.md
echo "==== Trouver les d simple quote espace (d' )"
grep --exclude README.md --color=always -i -P "[cdjlmnstuq]['’] " *.md
###########echo "==== Trouver les d apostrophe (d’) à remplacer par des simples quotes (d')"
###########echo "==== Please run this command to fix: sed -i ""s/\([CcDdJjLlMmNnSsTtUuQq]\)’\([^,. ]\)/\1'\2/g"" *.md"
########### grep --exclude README.md --color=always -i "[cdjlmnstuq]’[^,. ]" *.md  # decommenter si besoin
echo "==== Trouver les doubles 'lorsque que'"
grep --exclude README.md --color=always -i -P 'lorsque que ' *.md
echo "==== Trouver les doubles 'ii' au lieu de 'il'"
grep --exclude README.md --color=always -i -P '[#\.\?\!:;,<>\)\t] *ii ' *.md
echo "==== Trouver les doubles 'll' au lieu de 'il'"
grep --exclude README.md --color=always -i -P '[#\.\?\!:;,<>\)\t] *ll ' *.md
echo "==== Trouver les doubles 'IL' ou 'ILs' au lieu de 'Il' ou 'Ils'"
grep --exclude README.md --color=always -P '[#\.\?\!:;,<>\)\t] *ILs? ' *.md
echo "==== Trouver les Ils (majuscule) avec un point manquant"
grep --exclude README.md --color=always -P '[^.\!\?\:#\]«–] Ils' *md
grep --exclude README.md --color=always -P '[^.\!\?\:#\]«–] Elle' *md
echo "==== Trouver les doubles 'ells' au lieu de 'elle'"
grep --exclude README.md --color=always -iP ' +ells' *.md
###########echo "==== Trouver les majuscules après une virgule"
###########grep --exclude README.md --color=always -Pzo --binary-files=text ", *[\r\n][0-9]+:[0-9]+\t+[A-ZÊÉËÈÀÂÄÔÖÏÎÙÛÜÇ][a-zêéëèàâäôöïîùûüçœ'’\`\- ]+" *.md
echo "==== Trouver les minuscules après un point (.) ou un point d'interrogation et un nouveau verset"
grep --exclude README.md --color=always -Pzo --binary-files=text '[.\?] *[\r\n][0-9]+:[0-9]+\t+[a-zêéëèàâäôöïîùûüçœ]+' *.md
echo "==== Trouver les versets en double"
grep --exclude README.md --color=always -i -Pzo --binary-files=text '[\r\n]([0-9]+:[0-9]+).+[\r\n]\1\t' *.md
grep --exclude README.md --color=always -i -P '[\r\n]([0-9]+:[0-9]+).+\1\t' *.md
echo "==== Trouver les doublons consécutifs"
grep --exclude README.md --color=always -i -P '[^[:alpha:]êéëèàâäôöïîùûüçœ-]+((?!vous|nous)[[:alpha:]êéëèàâäôöïîùûüçœ-]+)\ +\1[^[:alpha:]êéëèàâäôöïîùûüçœ-]+' *.md
#echo "==== Trouver les espaces insécables docx (non-breaking space)"
#grep --exclude README.md --color=always -P "[\xa0]" *.md
echo "==== Trouver les doubles espaces"
grep --exclude README.md --color=always -P " {2,}." *.md
echo "==== Trouver les caractères spéciaux"
#grep --exclude README.md --color=always -i -P "[^\xa0[:alnum:]êéëèàâäôöïîùûüçœ '\"‘’,;?\!:#\.<>«»“”\`\t\(\)–\-\[\]\\\/]" *.md  # decommenter si besoin
grep --exclude README.md --color=always -i -P "[^\xa0[:alnum:]êéëèàâäôöïîùûüçœ '\"‘’,;?\!:#\.<>«»“”\t\(\)–\-\[\]\\\/\*\+\`\´\=\*]" *.md
echo "==== Trouver les commentaires non fermés"
grep --exclude README.md --color=always -P '<[^\!].+>' *.md
grep --exclude README.md --color=always -P '<![^-].+>' *.md
grep --exclude README.md --color=always -P '<!-[^-].+->' *.md
grep --exclude README.md --color=always -P '<!.+[^-]+->' *.md
grep --exclude README.md --color=always -P '<![^>]+[^-]+>' *.md
grep --exclude README.md --color=always -P '<![^>]+$' *.md
echo "Trouver les espaces en trop avant une virgule"
grep --exclude README.md --color=always ' ,' *.md
echo "==== Trouver les espaces manquants après un commentaire"
grep --exclude README.md --color=always -P '\-\->[^ ,-\.”’\)]' *.md
echo "==== Trouver les espaces en trop avant un commentaire"
grep --exclude README.md --color=always -P ' <\!' *.md
echo "==== Trouver les ponctuations collées entre elles avec un commentaire au milieu"
grep --exclude README.md --color=always -P '[\.\?\;\!-,] *<!--.+--> *[\.\?\;\!-,]' *.md
grep --exclude README.md --color=always -P ': *<!--.+--> *:' *.md
echo "==== Trouver les commentaires ne se fermant pas par un point (.)"
grep --exclude README.md --color=always '[^.!?]-->' *.md
echo "==== Trouver les commentaires commençant par un espace"
grep --exclude README.md --color=always '<!-- ' *.md
echo "==== Trouver les commentaires commençant par un point (.)"
grep --exclude README.md --color=always '<!--\.' *.md
echo "==== Trouver les commentaires commençant par une parenthèse"
grep --exclude README.md --color=always '<!--(' *md
echo "==== Trouver les commentaires commençant par une minuscule"
grep --exclude README.md --color=always '<!--[a-zêéëèàâäôöïîùûüçœ]' *md
grep --exclude README.md --color=always '<!--« [a-zêéëèàâäôöïîùûüçœ]' *md
echo "==== Trouver les parenthèses entourées par une ponctuation"
grep --exclude README.md -P '[.!;,?] *\([^\(\)]+\) *[.!;,?]' *.md | grep -v "J.-C." | grep --color=always -P '[.!;,?] *\([^\(\)]+\) *[.!;,?]'
echo "==== Trouver les versets ne se terminant pas par une ponctuation et enchaînant sur un chapitre suivant"
grep --exclude README.md --color=always -Pzo --binary-files=text '[\r\n]([0-9: ]+\t).+[a-zêéëèàâäôöïîùûüçœ\>] *[\r\n]+#' *md
echo "==== Trouver les énumérations de versets du même livre séparées par des point-virgules au lieu d'une virgule"
grep --exclude README.md --color=always -i -P "[a-z]+\. [0-9]+:[0-9]+(-[0-9]+)?( *; *[0-9]+:[0-9]+(-[0-9]+)?)+" *.md
grep --exclude README.md --color=always -P "; [-0-9:]{2,}" *md
grep --exclude README.md --color=always -i -P "(([0-9] )?[a-zêéëèàâäôöïîùûüçœ]+\.) +[0-9:, -]+; *\1" *.md
echo "==== Trouver les énumérations de versets mal formatées"
# Ex: Es. 49:10. Ap. 7:16 => un point (.) au lieu d'un point-virgule (;)
# Ex: Voir 1 Ti. 1:1 : Tit. 1:3. => deux points de suspension (:) au lieu d'un point-virgule (;)
grep --exclude README.md --color=always -i -P "([0-9] )?[a-zêéëèàâäôöïîùûüçœ]+\. +[0-9:, -]+[^\;] ([0-9] )?[a-zêéëèàâäôöïîùûüçœ]+\. +[0-9:, -]" *.md
echo "==== Trouver les espaces entre les numéros du chapitre et le verset"
grep --exclude README.md --color=always -i -P "[0-9]+( :|: | : )[0-9]+" *.md
echo "==== Trouver le tétragramme mal orthographié"
grep --color=always "YWHW" *.md
echo "==== Trouver les mots 'orient' et 'occident' avec un 'o' minuscule"
#grep --exclude README.md --color=always "orient" *.md
#grep --exclude README.md --color=always "occident" *.md
echo "==== Trouver le mot 'courtine' au lieu de 'rideau'"
grep --exclude README.md --color=always -i "courtine" *.md
echo "==== Trouver le mot 'sabbat' au lieu de 'shabbat'"
grep --exclude README.md --color=always -i "\bsabbat\b" *.md
echo "==== Trouver l'expression 'quelque soit' au lieu de 'quel que soit'"
grep --exclude README.md --color=always -i -P 'quelques? soi' *.md
echo "==== Trouver le mot 'grand prêtre' sans petit tiret (-) au lieu de 'grand-prêtre'"
grep --exclude README.md --color=always -i "grand prêtre" *.md
echo "==== Trouver <c'est à dire> sans tiret (-)."
grep --exclude README.md --color=always -i "est à dire" *.md
echo "==== Trouver les <c.à.d> au lieu de <c'est-à-dire>."
grep --exclude README.md --color=always -i "\.à\.d" *.md
echo "==== Trouver le mot 'elohim', 'élohim', 'elohim', 'élohîm' ou 'elohïm'... au lieu de 'elohîm'"
grep --exclude README.md --color=always -i -P "([eé]lohim|élohîm|[eé]lohïm)" *.md
echo "==== Trouver le mot 'assembée' au lieu de 'assemblée'"
grep --exclude README.md --color=always -i -P "assembée" *.md
# echo "==== Trouver les longs tirets (–) par des petits tirets (-)"
# grep --exclude README.md "–" *.md | grep -v "J.-C." | grep --color=always "–"
echo "==== Trouver le mot J.-C avec un point manquant aprés le C"
grep --exclude README.md --color=always -P 'J.-C[^\.]' *md
echo "==== Trouver les nombres mal formatés"
grep --exclude README.md --color=always '[0-9]000' *md
echo "==== Trouver les doubles lettres en dédbut de mot."
grep --exclude README.md --color=always -P '[\t ]aa' *md
grep --exclude README.md --color=always -P '[\t ]aA' *md
grep --exclude README.md -P '[\t ]Aa' *md | grep -v "Aaron" | grep --color=always -P '[\t ]Aa'
grep --exclude README.md --color=always -iP '[\t ]àà' *md
grep --exclude README.md --color=always -iP '[\t ]ââ' *md
grep --exclude README.md --color=always -iP '[\t ]bb' *md
grep --exclude README.md --color=always -iP '[\t ]cc' *md
grep --exclude README.md --color=always -iP '[\t ]dd' *md
grep --exclude README.md --color=always -iP '[\t ]ee' *md
grep --exclude README.md --color=always -iP '[\t ]éé' *md
grep --exclude README.md --color=always -iP '[\t ]êê' *md
grep --exclude README.md --color=always -iP '[\t ]ff' *md
grep --exclude README.md --color=always -iP '[\t ]gg' *md
grep --exclude README.md --color=always -iP '[\t ]hh' *md
grep --exclude README.md --color=always -iP '\tii' *md
grep --exclude README.md --color=always -P '[ ]ii' *md
grep --exclude README.md --color=always -P '[\t ]iI' *md
grep --exclude README.md --color=always -P '[\t ]Ii' *md
grep --exclude README.md --color=always -iP '[\t ]jj' *md
grep --exclude README.md --color=always -iP '[\t ]kk' *md
grep --exclude README.md --color=always -iP '[\t ]ll' *md
grep --exclude README.md --color=always -iP '[\t ]mm' *md
grep --exclude README.md --color=always -iP '[\t ]nn' *md
grep --exclude README.md --color=always -iP '[\t ]oo' *md
grep --exclude README.md --color=always -iP '[\t ]ôô' *md
grep --exclude README.md --color=always -iP '[\t ]pp' *md
grep --exclude README.md --color=always -iP '[\t ]qq' *md
grep --exclude README.md --color=always -iP '[\t ]rr' *md
grep --exclude README.md --color=always -iP '[\t ]ss' *md
grep --exclude README.md --color=always -iP '[\t ]tt' *md
grep --exclude README.md --color=always -iP '[\t ]uu' *md
grep --exclude README.md --color=always -iP '[\t ]vv' *md
grep --exclude README.md --color=always -iP '[\t ]ww' *md
grep --exclude README.md --color=always -iP '[\t ]xx' *md
grep --exclude README.md --color=always -iP '[\t ]yy' *md
grep --exclude README.md --color=always -iP '[\t ]zz' *md
echo "==== Trouver l'expresion 'Voir ' avant une majuscule et devant deux points (:)"
grep --exclude README.md --color=always ' : Voir ' *md
echo "==== Trouver les 'parce' non suivi de 'que'"
grep --color=always -i 'parce [^q]' *md
echo "==== Trouver les 'rien avoir' au lieu de 'rien à voir'"
grep --color=always -i 'rien avoir' *md
echo "==== Trouver les commentaires après une ponctuation"
grep --exclude README.md --color=always -P '[\.\;\-,] ?<' *md
echo "==== Trouver les mauvaises abréviations de versets Deuteronome (De.), Genèse (Ge.), Exode (Ex.)"
grep --exclude README.md --color=always -P 'Ex ' *md
grep --exclude README.md --color=always -P 'Gn\. ' *md
grep --exclude README.md --color=always -P 'Gen\. ' *md
grep --exclude README.md --color=always -P 'Gen ' *md
grep --exclude README.md --color=always -P 'Dt\. ' *md
grep --exclude README.md --color=always -P 'Le\. [0-9]' *md
grep --exclude README.md --color=always -P 'L[ée]v\. ' *md
grep --exclude README.md --color=always -P 'Nb [0-9]' *md
grep --exclude README.md --color=always -P 'Nb\. ' *md
grep --exclude README.md --color=always -P 'Rm\. ' *md
grep --exclude README.md --color=always -P 'Rom ' *md
grep --exclude README.md --color=always -P 'Apo ' *md
grep --exclude README.md --color=always -P 'Apo\. ' *md
grep --exclude README.md --color=always -P 'Phil ' *md
grep --exclude README.md --color=always -P 'Phi\. ' *md
grep --exclude README.md --color=always -P 'Joë ' *md
grep --exclude README.md --color=always -P 'Tim[.]? ' *md
grep --exclude README.md --color=always -P 'Tm[.]? ' *md
grep --exclude README.md --color=always -P '[1-2] T[.] ' *md
grep --exclude README.md --color=always -P 'Tit ' *md
grep --exclude README.md --color=always -P 'Tite[.]? [0-9]' *md
grep --exclude README.md --color=always -P 'Jos [0-9]' *md
grep --exclude README.md --color=always -P '\bHe. [0-9]' *md
grep --exclude README.md --color=always -P ' Sa\.' *md
grep --exclude README.md --color=always -P '[1-2] Cor\.' *md
grep --exclude README.md --color=always -P 'Je\.' *md
grep --exclude README.md --color=always -P 'Mat[t]?[.]? ' *md
grep --exclude README.md --color=always -P 'Act[.]? ' *md
grep --exclude README.md --color=always -P 'Eph[.]? ' *md
grep --exclude README.md --color=always -P 'Ecc[.]? ' *md
grep --exclude README.md --color=always -P 'Job[.] [0-9]' *md
grep --exclude README.md --color=always -P 'Jo[.]? [0-9]' *md
grep --exclude README.md --color=always -P '[1-2] Rois [0-9]' *md
grep -P --color=always "[A-Z][a-zé] [0-9]+:" *md
grep -P --color=always "[A-Z][A-ZÉ]\. [0-9]+:" *md
echo "==== Trouver les sous-chapitres dans le sous-niveau est supérieur ou égale à 4"
grep --color=always -P "####+" *md
echo "==== Trouver les titres de chapitre mal orthographiés. Ex: '## CHAPITRE 1' au lieu de '## Chapitre 1'"
grep -P '^## ' *md | grep --color=always --invert-match -P 'Chapitre [1-9]([0-9])*'
echo "==== Trouver les sauts de versets"
grep --exclude README.md --color=always -Pzo --binary-files=text "# *Chapitre.+[\r\n]+(#.+[\r\n]+)?[0-9]+:[^1]" *.md;
for i in `seq 0 300`;
do
    y=$(($i+2));
    grep --exclude README.md --color=always -i -Pzo --binary-files=text "[\r\n]([0-9]+):$i\t+.+[\r\n]+(#.+[\r\n]+)?\1:$y\t" *.md;
done


# Trucs et astuces
#echo "==== Trouver les guillemets manquants après deux points (:)"
#grep --exclude README.md --color=always -i -P "[^«“] (répon)?d[îtesanirenz]+(-leurs?|lui)?[ ]+:[ ]+[^«“‘]" *.md
#grep --exclude README.md --color=always -i -Pzo --binary-files=text "[^«“] (répon)?d[îtesanirenz]+(-leurs?|lui)?[ ]+:[\r\n][0-9]+:[0-9]+\t[ ]*[^«“‘,; ]+ " *.md


#"Remplacement des long tirets (–) par des petits tirets (-)."
#git cm "1,2,3 Jean: remplacement des guillemet (’) par des simples quotes (')."


# juif -> Juif, pharaon -> Pharaon
