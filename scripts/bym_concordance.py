"""Script to generate concordances file."""

from os import listdir
import os.path
import re
import sys
from time import time
from collections import defaultdict, OrderedDict

# verify paths
DIRNAME = os.path.dirname(__file__)
INPATH = os.path.normpath(os.path.join(DIRNAME, '../source/fr/'))
OUTPATH = os.path.normpath(os.path.join(DIRNAME, '../output/fr/md/'))

# user information
print("\n\n> Script run : « {} »".format(os.path.basename(sys.argv[0])))
print("> Target path: « {} »".format(INPATH))
print("> Output path: « {} »\n\n".format(OUTPATH))

# constants
EXT = ".md"

# liste
DECISIONAL_FILENAME = 'decision_list.md'   # liste décisionnelle

# output
CLEANUP_FILENAME = 'cleanup.md'
ADDED_FILENAME = 'added_word.md'

# dictionary of files
FILES_DICT = OrderedDict({
    ('adj', 'adjectives.md'),   # adjectifs utilisés
    ('n', 'nouns.md'),          # noms utilisés
    ('np', 'proper_nouns.md'),  # noms propres utilisés
    ('v', 'verbs.md')})         # verbes utilisés

# dictionnaire des abbreviations des livres
DICT_BOOKS = OrderedDict([
    ('01', 'Ge'), ('02', 'Ex'), ('03', 'Lé'), ('04', 'No'), ('05', 'De'),
    ('06', 'Jos'), ('07', 'Jg'), ('08', '1 S'), ('09', '2 S'), ('10', '1 R'),
    ('11', '2 R'), ('12', 'Es'), ('13', 'Jé'), ('14', 'Ez'), ('15', 'Os'),
    ('16', 'Joë'), ('17', 'Am'), ('18', 'Ab'), ('19', 'Jon'), ('20', 'Mi'),
    ('21', 'Na'), ('22', 'Ha'), ('23', 'So'), ('24', 'Ag'), ('25', 'Za'),
    ('26', 'Mal'), ('27', 'Ps'), ('28', 'Pr'), ('29', 'Job'), ('30', 'Ca'),
    ('31', 'Ru'), ('32', 'La'), ('33', 'Ec'), ('34', 'Est'), ('35', 'Da'),
    ('36', 'Esd'), ('37', 'Né'), ('38', '1 Ch'), ('39', '2 Ch'), ('40', 'Mt'),
    ('41', 'Mc'), ('42', 'Lu'), ('43', 'Jn'), ('44', 'Ac'), ('45', 'Ja'),
    ('46', 'Ga'), ('47', '1 Th'), ('48', '2 Th'), ('49', '1 Co'),
    ('50', '2 Co'), ('51', 'Ro'), ('52', 'Ep'), ('53', 'Ph'), ('54', 'Col'),
    ('55', 'Phm'), ('56', '1 Ti'), ('57', 'Tit'), ('58', '1 Pi'),
    ('59', '2 Pi'), ('60', '2 Ti'), ('61', 'Jud'), ('62', 'Hé'),
    ('63', '1 Jn'), ('64', '2 Jn'), ('65', '3 Jn'), ('66', 'Ap')
])

# list of books
BOOKS = [n[1] for n in DICT_BOOKS.items()]

# regex pattern to match verses
RE_VERSE = re.compile(r'^([0-9]+:[0-9]+)\t(.+)')

# all regex
RE_COMMENTS = re.compile(r'<[^<]+>')  # remove comments
RE_PONCTUATIONS = re.compile(
    (r'['
     '«'    # remove first level left double quotes
     '»'    # remove first level right double quotes
     '“'    # remove second level left double quotes
     '”'    # remove second level right double quotes
     ','    # remove commas
     r'\!'  # remove exclamation point
     r'\?'  # remove question mark
     r'\.'  # remove points
     r'\;'  # remove semi-colon
     r'\('  # remove left parenthesis
     r'\)'  # remove right parenthesis
     r'\]'  # remove right parenthesis
     r'\['  # remove left bracket
     r'\:'
     ']'))

RE_HYPHENS_NOUNS = re.compile(
    (r'\b(?<!-)('  # nouns
     'vis-à-vis|'
     "c.est-à-dire|"
     'premiers?-nés?|'
     'petits?-fils?|'
     'petites?-filles?|'
     'avant-postes?|'
     'avant-hier|'
     "aujourd.hui|"
     'aveugle-nés?|'
     'avant-hier|'
     'belles?-filles?|'
     'belles?-sœurs?|'
     'belles?-mères?|'
     'beaux?-frères?|'
     'beaux?-pères?|'
     'pots?-de-vins?|'
     'demi-sicles?|'
     'grands?-prêtres?|'
     'après-demain|'
     'arrière-gardes?|'
     'auto-contrôle|'
     'avant-gardes?|'
     'arc-en-ciel|'
     'demi-hin|'
     'demi-tribus?|'
     'petits?-enfants?|'
     'bien-être|'
     'biens?-aimée?s?|'
     'chauve-souris|'
     'demi-coudées?|'
     'demi-heure|'
     'demi-quartier|'
     'pêle-mêle|'
     'reine-mère|'
     'second-premier|'
     'sur-le-champ|'
     'syro-phénicienn?e?s?|'
     'hors-la-lois?|'
     'ici-bas|'
     'non-négligeable|'
     'nu-pieds?|'
     'par-derrière|'
     'porc-épics?|'
     'porte-bannières?|'
     'très-haut|'
     'très-sainte?s?|'
     'très-puissant|'
     'grands-mères?|'
     'nord-est|'
     'nord-ouest|'
     'sud-est|'
     'sud-ouest|'
     'nouveau-nés?|'
     'mort-nés?|'
     'elles?-mêmes?|'
     'eux-mêmes|'
     'guet-apens|'
     'êtes-vous|'
     'lui-même|'
     'soi-même|'
     'moi-même|'
     'toi-même|'
     'nous-mêmes|'
     'vous-mêmes|'
     'par-dessous|'
     'par-dessus|'
     'par-devant|'
     'quelques-une?s|'
     # adverbs
     'au-devant|'
     'au-dessus|'
     'au-dessous|'
     'au-delà|'
     'au-dehors|'
     'au-dedans|'
     'là-dedans|'
     'là-dessus|'
     'là-bas|'
     'peut-être|'
     'là-haut'
     r')\b(?!-)'), flags=re.IGNORECASE)

# persons
RE_HYPHENS_PERSONS = re.compile(
    (r'\b(?<!-)('
     'Anti-Mashiah|'
     'Yashoubi-Léhem|'
     'Yegar-Sahadouta|'
     'Yiphtah-El|'
     'Yishbo-Benob|'
     'Yonathelem-Rehoqiym|'
     'Yosheb-Basshébeth|'
     'Youshab-Hésed|'
     'Abel-Keramim|'
     'Abel-Maïm|'
     'Abel-Mehola|'
     'Abel-Mitsraïm|'
     'Abel-Sittim|'
     'Abel-(Beth)-[a-zâàéèêëôôîïûü\']+|'
     'Almon-Diblathaïm|'
     r'En-[a-zâàéèêëôôîïûü\-]+|'
     'Etsyôn-Guéber|'
     'Eyn-Tappouah|'
     'Hamon-Gog|'
     'Ébed-Mélec|'
     'Églath-Sheliyshiyah|'
     'Éphès-Dammim|'
     'Ésar-Haddon|'
     'Abed-Négo|'
     'Abi-Albon|'
     'Adami-Nékeb|'
     'Adoni-Bézek|'
     'Adoni-Tsédek|'
     'Al-Thasheth|'
     'Allélou-Yah|'
     'Ar-Moab|'
     'Ashteroth-Karnaïm|'
     'Atharoth-Addar|'
     r'Athroth-[a-zâàéèêëôôîïûü\-]+|'
     'Atta-El-roï|'
     'Ayeleth-Hashachar|'
     'Aznoth-Thabor|'
     r'Bar-[a-zâàéèêëôôîïûü\-]+|'  # Ex: Baal-Zebub
     r'Baal-[a-zâàéèêëôôîïûü\-]+|'  # Ex: Baal-Zebub
     '[a-zâàéèêëôôîïûü]+-Baal|'  # Ex: Bamoth-Baal
     'Baalé-Yéhouda|'
     'Bar-Yéhoshoua|'
     'Bas-Pays|'
     'Baalath-Beer|'
     r'Bath-[a-zâàéèêëôôîïûü\-]+|'
     'Beaux-Ports|'
     r'Beer-[a-zâàéèêëôôîïûü\-]+|'
     r'Ben-[a-zâàéèêëôôîïûü\-]+|'
     'Bené-Berak|'
     'Bené-Hashem|'
     'Berodac-Baladan|'
     r'Beth-[a-zâàéèêëôôîïûü\-]+|'
     r'Béth-[a-zâàéèêëôôîïûü\-]+|'
     'Bikath-Aven|'
     'Caleb-Éphratha|'
     'Col-Hozé|'
     'Cor-Ashan|'
     'Dibon-Gad|'
     'Di-Zahab|'
     'Eben-Ezer|'
     r'El-[a-zâàéèêëôôîïûü\-]+|'
     r'Él-[a-zâàéèêëôôîïûü\-]+|'
     'Elkana-Tsophaï|'
     'Émek-Ketsits|'
     'Etsyôn-Guéber|'
     'Évil-Merodac|'
     'Gath-Hépher|'
     'Gath-Rimmon|'
     'Guibea-Elohïm|'
     'Guibeath-Phinées|'
     'Guittha-Hépher|'
     'Hammoth-Dor|'
     'Har-Hérès|'
     'Har-hour|'
     'Harosheth-Goïm|'
     r'Hatroth-[a-zâàéèêëôôîïûü\-]+|'
     r'Hatsar-[a-zâàéèêëôôîïûü\-]+|'
     'Hatsatson-Thamar|'
     'Hatsi-Hammanachthi|'
     'Hatsi-Hammenouhoth|'
     'Hatsor-Hadattah|'
     'Hatzer-Hatthicon|'
     'Helqath-Hatsourim|'
     'Hor-Guidgad|'
     'Houram-Abi|'
     'I-Kabod|'
     'Immanou-El|'
     'Ir-Hammélach|'
     'Ir-Shémesh|'
     'Ish-Bosheth|'
     'Ittha-Katsin|'
     'Iyey-Abariym|'
     'Kephar-Ammonaï|'
     'Kibroth-Hattaava|'
     r'Kir-[a-zâàéèêëôôîïûü\-]+|'
     'Kisloth-Thabor|'
     'Koushân-Rishéataïm|'
     'Kédesh-Nephthali|'
     r'Keroub-[a-zâàéèêëôôîïûü\-]+|'
     'Lachaï-roï|'
     'Lo-Rouhama|'
     'Lo-Ammi|'
     'Machané-Dan|'
     'Magor-Missabib|'
     'Maher-Shalal-Chash-Baz|'
     'Malki-Shoua|'
     'Maaré-Guibea|'
     r'Migdal-[a-zâàéèêëôôîïûü\-]+|'  # Migdal-Gad
     'Misrephoth-Maïm|'
     'Moréshet-Gath|'
     'Mouth-Labben|'
     'Mé-Yarkon|'
     'Métheg-Amma|'
     'Myriam-Magdeleine|'
     'Nachalé-Gaash|'
     'Nergal-Sharetser|'
     'Nethan-Mélec|'
     'No-Amon|'
     'Obed-Édom|'
     'Ouzzèn-Shééra|'
     'Pachath-Moab|'
     'Paddan-Aram|'
     'Pas-Dammim|'
     'Pi-Béseth|'
     'Pi-Hahiroth|'
     'Pokéreth-Hatsebaïm|'
     'Poti-Phéra|'
     'Pérets-Ouzza|'
     'Qadesh-Barnéa|'
     r'Qeriyoth-[a-zâàéèêëôôîïûü\-]+|'
     r'Qiryath-[a-zâàéèêëôôîïûü\-]+|'
     'Qodesh-Barnéa|'
     'Qérèn-Happouk|'
     r'Qeroub-[a-zâàéèêëôôîïûü\-]+|'
     'Rab-Saris|'
     'Ramath-Léchi|'
     'Ramath-Mitspé|'
     'Ramathaïm-Tsophim|'
     'Rimmon-Pérets|'
     'Romamthi-Ézer|'
     'Réguem-Mélec|'
     'Séla-Hammachlekoth|'
     'Shethar-Boznaï|'
     'Shichor-Libnahth|'
     'Shimron-Meron|'
     'Shear-Yashoub|'
     'Shaveh-Qiryathayim|'
     'Shoushannim-Edouth|'
     'Samgar-Nebou|'
     'Soukkoth-Benoth|'
     'Thaanath-Shiloh|'
     'Thachthim-Hodshi|'
     'Thel-Abib|'
     'Thel-Harsha|'
     'Thel-Mélach|'
     'Thilgath-Pilnéser|'
     'Thimnath-Hérès|'
     'Thimnath-Sérach|'
     'Tiglath-Piléser|'
     'Tilgath-Pilnéser|'
     'Tob-Adoniyah|'
     'Toubal-Caïn|'
     'Trois-Tavernes|'
     'Tsaphnath-Paenéach|'
     'Tséreth-Hashachar|'
     r'YHWH-[a-zâàéèêëôôîïûü\-]+|'
     "Èl[’']azar|"
     "Re[’']élayah|"
     "Shim[’']ôn|"
     "Shema[’']yah|"
     "Daniye[']l|"
     "Qe[']iylah|"
     "Gabriy[']el|"
     "Ma[']akah|"
     "Guid[']ôn|"
     r"[a-zâàéèêëôôîïûü\-]*`[a-zâàéèêëôôîïûü\-']*"  # Ex: Yizre`e'l
     r')\b(?!-)'), flags=re.IGNORECASE)

# hyphens (tiret)
RE_HYPHENS = re.compile(r'[\-–—]')

# apostrophe
RE_QUOTES = re.compile(r"['‘’]")

# adjectif indéfini
RE_UNDEFINED_ADJECTIVES = re.compile(
    (r'\b(?<!-)('
     'autres?|'
     'quelqu|'
     'plusieurs|'
     'chaque|'
     'aucune?|'
     'quelques?'
     r')\b(?!-)'), flags=re.IGNORECASE)

# numéral : ordinal et cardinal
RE_NUMERAL = re.compile(
    (r'\b(?<!-)('
     'premiers?|'
     'premières?(ment)?|'
     'deux(ième)?(ment)?|'
     'trois(ièmes?)?(ment)?|'
     'quatre|'
     'quarante|'
     'quatrième|'
     'quatorzième|'
     'quinze|'
     'quinzième|'
     'cinq(uante)?|'
     'cinquième|'
     'cinquante-[a-zè]|'  # ex: cinquante-neuvième
     'cinquantième|'
     'cinquantaines|'
     'six(ième)?|'
     'soixante|'
     'sept(ième)?|'
     'huit(ième)?|'
     'neuf|'
     'neuvième|'
     'dix-[a-zè]|'
     'dix(ième)?s?|'
     'dizaines?|'
     'onze|'
     'unième|'
     'onzième|'
     'douze|'
     'douzième|'
     'treizième|'
     'trentième|'
     'vingt|'
     'vingt-[a-zè]|'  # ex: vingt-cinq ; vingt-cinquième
     'vingt(ième)?|'
     'trente|'
     'trente-[a-zè]|'
     'cents?|'
     'centaines?|'
     'mille|'
     'milliers?|'
     'millième'
     r')\b(?!-)'), flags=re.IGNORECASE)

# adjectives
RE_ADJECTIVES = re.compile(
    (r'\b(?<!-)('
     'tels?|'
     'telles?|'
     'quelconques?'
     r')\b(?!-)'), flags=re.IGNORECASE)

# articles
RE_ARTICLES = re.compile(
    (r'\b(?<!-)('
     'unes?|'
     'uns?|'
     'la?|'
     'les?|'
     'des?|'
     'du?|'
     'aux?'
     r')\b(?!-)'), flags=re.IGNORECASE)

# adverbs
RE_ADVERBS = re.compile(
    (r'\b(?<!-)('
     'dessus|'
     'dessous|'
     'là|'
     'mêmes?|'
     'delà|'
     'tous|'
     'tout|'
     'toutes?|'
     'ainsi|'
     'dedans|'
     'ça|'
     'çà|'
     'où|'
     'très|'
     'trop|'
     'mieux|'
     'peu|'
     'encore|'
     'tant(ôt)?|'
     'tard|'
     'tôt|'
     'toujours|'
     'toutefois|'
     'autrefois|'
     'dehors|'
     'aussitôt|'
     'auparavant|'
     'avant|'
     'derrière|'
     'après|'
     'arrière|'
     'auprès|'
     'alors|'
     'autant|'
     'pourtant|'
     'pourquoi|'
     'néanmoins|'
     'moins|'
     'plus|'
     'par|'
     'puis|'
     'puisque?|'
     'presque?|'
     'parce|'
     'sitôt|'
     'ici|'
     'hier|'
     'hors|'
     'abondamment|'
     'absolument|'
     'accidentellement|'
     'affectueusement|'
     'frauduleusement|'
     'ailleurs|'
     'aimablement|'
     'aisément|'
     'alors|'
     'amicalement|'
     'amplement|'
     'amèrement|'
     'anciennement|'
     'anxieusement|'
     'après|'
     'ardemment|'
     'assez|'
     'assidument|'
     'assurément|'
     'attentivement|'
     'aucunement|'
     'auparavant|'
     'auprès|'
     'aussitôt|'
     'autour|'
     'autrefois|'
     'autrement|'
     'avantageusement|'
     'beaucoup|'
     'bientôt|'
     'bien|'
     'bravement|'
     'brièvement|'
     'calmement|'
     'cependant|'
     'certainement|'
     'certes|'
     'chèrement|'
     'clairement|'
     'combien|'
     'comment|'
     'complétement|'
     'conformément|'
     'constamment|'
     'continuellement|'
     'contrairement|'
     'convenablement|'
     'corporellement|'
     'correctement|'
     'courageusement|'
     'couramment|'
     'd\'abord|'
     'd\'accord|'
     'davantage|'
     'debout|'
     'depuis|'
     'dernièrement|'
     'deçà|'
     'difficilement|'
     'diligemment|'
     'directement|'
     'divinement|'
     'dorénavant|'
     'doucement|'
     'droitement|'
     'durement|'
     'déjà|'
     'délicatement|'
     'désormais|'
     'effectivement|'
     'ensemble|'
     'ensuite|'
     'entièrement|'
     'environ|'
     'exactement|'
     'excessivement|'
     'expressément|'
     'extraordinairement|'
     'extrêmement|'
     'facilement|'
     'faussement|'
     'favorablement|'
     'fermement|'
     'fidèlement|'
     'finalement|'
     'fixement|'
     'fièrement|'
     'follement|'
     'fortement|'
     'franchement|'
     'fraîchement|'
     'fréquemment|'
     'furtivement|'
     'gaiement|'
     'glorieusement|'
     'gracieusement|'
     'grandement|'
     'gratuitement|'
     'gravement|'
     'guère|'
     'hardiment|'
     'hautement|'
     'hier|'
     'honnêtement|'
     'honorablement|'
     'ici|'
     'immédiatement|'
     'impétueusement|'
     'inaccessiblement|'
     'indignement|'
     'indéfiniment|'
     'infiniment|'
     'injustement|'
     'instamment|'
     'instantanément|'
     'intérieurement|'
     'inutilement|'
     'involontairement|'
     'jadis|'
     'jamais|'
     'joyeusement|'
     'justement|'
     'lendemain|'
     'lentement|'
     'librement|'
     'libéralement|'
     'loin|'
     'longtemps|'
     'longuement|'
     'lors|'
     'lourdement|'
     'légèrement|'
     'magnifiquement|'
     'merveilleusement|'
     'misérablement|'
     'mortellement|'
     'mutuellement|'
     'méchamment|'
     'naturellement|'
     'noblement|'
     'non|'
     'nouvellement|'
     'nullement|'
     'oralement|'
     'ordinairement|'
     'orgueilleusement|'
     'oui|'
     'ouvertement|'
     'paisiblement|'
     'parallèlement|'
     'pareillement|'
     'parfaitement|'
     'particulièrement|'
     'partout|'
     'patiemment|'
     'pauvrement|'
     'perfectionnement|'
     'pieusement|'
     'pleinement|'
     'plupart|'
     'plutôt|'
     'pourquoi|'
     'principalement|'
     'profondément|'
     'promptement|'
     'proprement|'
     'prudemment|'
     'près|'
     'précieusement|'
     'précipitamment|'
     'précisément|'
     'précédemment|'
     'prématurément|'
     'présentement|'
     'publiquement|'
     'puissamment|'
     'purement|'
     'rafraîchissement|'
     'rapidement|'
     'rarement|'
     'rassasiement|'
     'richement|'
     'rigoureusement|'
     'rudement|'
     'récemment|'
     'réciproquement|'
     'réellement|'
     'sagement|'
     'secrètement|'
     'seulement|'
     'simplement|'
     'sincèrement|'
     'sinon|'
     'sitôt|'
     'soigneusement|'
     'solidement|'
     'soudainement|'
     'souvent|'
     'spirituellement|'
     'spécialement|'
     'subitement|'
     'successivement|'
     'suffisamment|'
     'surtout|'
     'séparément|'
     'sérieusement|'
     'sévèrement|'
     'sûrement|'
     'tellement|'
     'tendrement|'
     'totalement|'
     'tranquillement|'
     'tumultueusement|'
     'tôt|'
     'unanimement|'
     'uniquement|'
     'vaillamment|'
     'vainement|'
     'vertueusement|'
     'violemment|'
     'volontairement|'
     'vraiment|'
     'véritablement|'
     'également|'
     'éternellement|'
     'étroitement|'
     'éminemment|'
     'demain'
     r')\b(?!-)'), flags=re.IGNORECASE)

# pronouns
RE_PRONOUNS = re.compile(
    (r'\b(?<!-)('
     'qu?|'
     'que?|'
     'qui|'
     'quoi|'
     'quiconque|'
     'laquelle|'
     'lequel|'
     'lesquels|'
     'lesquelles|'
     'chacune?|'
     'dont|'
     'ce?|'
     'ci|'
     'cela|'
     'ceci|'
     'ces|'
     'ceux|'
     'celui|'
     'celles?|'
     'cet|'
     'cettes?|'
     'y|'
     'rien|'
     'on|'
     'je?|'
     'tu?|'
     'ils?|'
     'nous|'
     'vous|'
     'elles?|'
     'se?|'
     'lui|'
     'soi|'
     'ta|'
     'toi|'
     'ton|'
     'tes?|'
     'ma?|'
     'moi|'
     'mon|'
     'mes?|'
     'nos|'
     'vos|'
     'sa|'
     'ses?|'
     'son|'
     'eux|'
     'leurs?|'
     'miens?|'
     'siens?|'
     'tiens?|'
     'miennes?|'
     'siennes?|'
     'tiennes?|'
     'nôtres?|'
     'notre|'
     'votre|'
     'autrui|'
     'vôtres?'
     r')\b(?!-)'), flags=re.IGNORECASE)

# conjonctions
RE_CONJONCTIONS = re.compile(
    (r'\b(?<!-)('
     'mais|'
     'ou|'
     'et|'
     'donc|'
     'car|'
     'ni|'
     'or|'
     'afin|'
     'si|'
     'lors|'
     'lorsque?|'
     'quand|'
     'quant|'
     'tandis|'
     'quoique?'
     r')\b(?!-)'), flags=re.IGNORECASE)

# prepositions
RE_PREPOSITIONS = re.compile(
    (r'\b(?<!-)('
     'à|'
     'hors|'
     'hormis|'
     'chez|'
     'dès|'
     'pour|'
     'pas|'
     'avec|'
     'sans|'
     'enfin|'
     'sur|'
     'jusque?s?|'
     'en|'
     'dans|'
     'parmi|'
     'contre|'
     'selon|'
     'aussi|'
     'comme|'
     'aux?quel(le)?s?|'
     'duquel|'
     'quel(le)?s?|'
     'desquels|'
     'desquel(le)?s?|'
     'voici|'
     'devant|'
     'vers|'
     'malgré|'
     'voilà'
     r')\b(?!-)'), flags=re.IGNORECASE)

# others
RE_OTHERS = re.compile(
    (r'\b(?<!-)('
     'étant|'
     'ne?|'
     'eh|'
     'hé|'
     'hélas|'
     'oh|'
     'ah|'
     'ha|'
     'ô'
     r')\b(?!-)'), flags=re.IGNORECASE)

# spaces
RE_SPACES = re.compile(r' +')


def verify_paths(*args):
    """Create folder if not exist."""
    try:
        for arg in args:
            if not os.path.exists(arg):
                print("Path error: « {} »".format(arg))
                raise FileNotFoundError
    except FileNotFoundError:
        os.mkdir(arg)
        print("Folder created: « {} »".format(os.path.basename(arg)))


def dispatch(book):
    """Return book name from its ordinal number."""
    return DICT_BOOKS.get(book, None)


def download(filename):
    """Download a file of words an return a dict."""
    ret = {}
    with open(filename, 'r', encoding='utf-8') as file:
        for line in file:
            values = line.rstrip().split(',')
            reference = values[0]
            ret.update([(value, reference) for value in values])
    return ret


def cleanup(text):
    """Remove french quote and comments."""
    verse = text
    if verse:
        def repl(mask):
            """Replacing with a string of characters with the same length."""
            return ' ' * len(mask.group())

        # remove or save some composed word which
        # contains "-" before removing hyphens
        # sign
        # nouns
        verse = RE_HYPHENS_NOUNS.sub(repl, verse)
        # persons
        persons = RE_HYPHENS_PERSONS.finditer(verse)
        # remove hyphens ? vis-à-vis => vis peut etre confondu avec le verbe
        # vivre
        verse = RE_HYPHENS.sub(' ', verse)
        # remove apostrophe
        verse = RE_QUOTES.sub(' ', verse)
        # adjectif indefini
        verse = RE_UNDEFINED_ADJECTIVES.sub(repl, verse)
        # numeral : ordinal et cardinal
        verse = RE_NUMERAL.sub(repl, verse)
        # adjectives
        verse = RE_ADJECTIVES.sub(repl, verse)
        # to remove irrelevant words (article, pronoun, etc)
        # articles
        verse = RE_ARTICLES.sub(repl, verse)
        # adverbs
        verse = RE_ADVERBS.sub(repl, verse)
        # pronouns
        verse = RE_PRONOUNS.sub(repl, verse)
        # conjonctions
        verse = RE_CONJONCTIONS.sub(repl, verse)
        # prepositions
        verse = RE_PREPOSITIONS.sub(repl, verse)
        # others
        verse = RE_OTHERS.sub(repl, verse)
        # restore proper nouns containing hyphens
        if persons:
            verse_list = list(verse)
            for per in persons:
                span = per.span()
                verse_list[span[0]:span[1]] = per.group()
            verse = ''.join(verse_list)
        # remove blank space
        verse = RE_SPACES.sub(' ', verse)
        verse = verse.strip()
    return verse


def sort(refs):
    """Sort list reference of verses according to bible orders."""
    def sort_by(content):
        content = content.rsplit(' ', 1)
        book = content[0]
        chap, num = map(int, content[-1].split(':'))
        return (BOOKS.index(book), chap, num)

    ret = sorted(
        list(refs),
        key=sort_by
    )
    return ret


def write(filename, word_list):
    """Write content in a file and return it."""
    with open(filename, 'w', encoding='utf-8') as file:
        for line in sorted(word_list):
            file.write(line)


def merge(in_path):
    """Write bible verses on a single file."""
    ret = []
    for file in sorted(listdir(in_path)):
        if file.endswith(EXT) and file[:2].isdigit():
            file = os.path.normpath(os.path.join(in_path, file))
            with open(file, 'r', encoding='utf-8') as lines:
                book = dispatch(os.path.basename(file)[:2])
                for line in lines:
                    verse = RE_VERSE.match(line)
                    if not verse:
                        continue
                    num, content = verse.groups()
                    # removing first comments,
                    # then french ponctuations characters
                    # followed by the remaining spaces
                    content = RE_COMMENTS.sub(' ', content)
                    content = RE_PONCTUATIONS.sub(' ', content)
                    content = RE_SPACES.sub(' ', content)
                    ret.append("{book} {num}\t{verse}\n".format(
                        **{'book': book, 'num': num, 'verse': content}))
    return ret


def clean(verses):
    """Clean up verses."""
    dict_words = defaultdict(set)
    for verse in verses:
        reference = verse.split('\t')[0]
        cleaned_verse = cleanup(verse.split('\t')[1])
        for ele in cleaned_verse.split():
            if not ele.isdigit():
                dict_words[ele.lower()].add(reference)
    return dict_words


def dispatch_if(dict_of_words, files_dict, path):
    """Dispatch words in dictionaries."""
    # dictionaries
    dec = defaultdict(set)  # decisional
    tmp = defaultdict(set)  # temporary
    files = OrderedDict({   # Ex. : adjectives, verbs, nouns, proper_nouns
        key: download(os.path.join(path, val))
        for key, val in files_dict.items()})
    # iterates word's check on all genre
    for word, ref in dict_of_words.items():
        loop_dict = {}
        # add to temporary dictionary
        for genre in files_dict.keys():
            match = ''
            try:
                match = files[genre][word]
            except KeyError:
                loop_dict[genre] = match
            else:
                tmp[match, genre].update(ref)
                loop_dict[genre] = match
        else:
            # evaluate dict length and content before adding
            if len(set(loop_dict.values())) == 1:
                dec[word].update(ref)
    return tmp, dec


def update(filename, common_dict, decisional_dict):
    """Update dictionaries."""
    del_key = set()
    with open(filename, 'r', encoding='utf-8') as file:
        for line in sorted(file):
            items = line.split(',')
            for key in sorted(decisional_dict.keys()):
                key = str(key.split(',')).strip('[]\'')
                if items[0] == key:
                    # add each decisional item to common_dict
                    common_dict[items[2], items[1]].update(
                        [ref.strip() for ref in items[3:]])
                    # save keys to handle untracked
                    del_key.add(items[0])
    # save untracked
    for each in del_key:
        try:
            decisional_dict.pop(each)
        except KeyError:
            pass
    return common_dict, decisional_dict


def main():
    """Associate a word with its references.

    Tous les mots pouvant être soit nom et verbe
    sont tenus dans la liste de décision:

    - alarme,v,alarmer,[references]
    - alarme,n,alarme,[references]

    => Tous les mots présents dans la liste de décision
    ne doivent en aucun cas être présent dans une autre liste.
    """
    try:
        start = time()
        verify_paths(INPATH, OUTPATH)
        # merge all book verses
        list_of_verses = merge(os.path.normpath(INPATH))
        # clean verses and dispatch them on dictionaries
        dict_of_words = clean(list_of_verses)
        common_dict, decision_dict = dispatch_if(dict_of_words,
                                                 FILES_DICT,
                                                 os.path.normpath(OUTPATH))
        # update common dict with the decisional list's words
        cleaned_dict, added_dict = update(
            os.path.normpath(os.path.join(OUTPATH, DECISIONAL_FILENAME)),
            common_dict,
            decision_dict)
        cleaned = ["{word},\t{gender},\t{ref}\n".format(
            **{'word': word[0],
               'gender': word[1],
               'ref': sort(ref)}) for word, ref in cleaned_dict.items()]
        write(os.path.normpath(
            os.path.join(OUTPATH, CLEANUP_FILENAME)), cleaned)
        # save added
        if added_dict:
            added = ["{word},\t{ref}\n".format(
                **{'word': word,
                   'ref': sort(ref)}) for word, ref in sorted(
                       added_dict.items())]
            write(os.path.normpath(
                os.path.join(OUTPATH, ADDED_FILENAME)), added)
            print('>>> Added word(s) count: {}\n'.format(len(added_dict)))
    except FileNotFoundError as err:
        raise err
    finally:
        end = time()
        tot = end-start
        print("==========")
        print("Total time: {0:.2f} sec".format(round(tot, 2)))


if __name__ == '__main__':
    main()
