# -*- coding: utf-8 -*-

from mlconjug import Conjugator


FILE_LIST = r"used_verbs.md"


def main():
    """
    Liste les verbes utilisés dans le module mlconjug.

    INPUT  : listes des verbes
    OUTPUT : listes des verbes conjugées

    /!\ Certains verbes n'ont pas toutes les formes conjuguées.
    Ceux-ci ne sont pas gérés dans mlconjug:
      - apparoir,
      - chaloir,
      - déchoir,
      - ester,
      - falloir,
      - forclore,
      - frire,
      - férir,
      - occire,
      - quérir, ravoir, sourdre
    """
    conjugator = Conjugator(language='fr')
    try:
        verbs = open(FILE_LIST, 'r', encoding='utf-8')
        f_out = open("conjugated_verbs2.md", "a", encoding="utf-8")
        # parcours les verbes pour les conjuguer
        for verb in verbs:
            try:
                conj = conjugator.conjugate(verb.strip())
                all_conjugated_forms = conj.iterate()
            except AttributeError:
                print(">>> Ce verbe ne se conjugue pas ou en partie : \t{}"
                      .format(verb))
            else:
                infinitive = conj.name
                list_verbs = []
                set_verbs = set()
                # supprime les doublons
                for item in all_conjugated_forms:
                    if item[-1] != infinitive:
                        set_verbs.add(item[-1])
                # ordonne les formes conjuguées des verbes
                # met la forme infinitive en position [0]
                for item in set_verbs:
                    list_verbs.append(item)
                list_verbs.append(infinitive)
                list_verbs.reverse()
                res = str(list_verbs).strip('[]').replace('\'', '')
                f_out.write(res)
                f_out.write("\n")
    except FileNotFoundError as exc:
        print(exc)
    finally:
        verbs.close()
        f_out.close()

if __name__ == '__main__':
    main()
