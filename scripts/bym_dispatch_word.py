#!/usr/bin/env python3

"""BJC-DISPATCH-WORD.

This program allows picking words sequentially from a pre-formatted list
and dispatching them to the appropriate repository file.

To run it smoothly set the correct paths.
"""

import itertools as it
import os
import re
from time import time
from collections import deque, Counter, OrderedDict
import configuration as cfg
import utils

DIRNAME = os.path.dirname(__file__)
INPATH = os.path.normpath(os.path.join(DIRNAME, '../output/fr/md/'))

# input file
FILENAME = 'verify_added_word.md'
W_G_N = "{},{},{}"


def distribute(input_list, word_list, files_dict):
    """distribute the words in the correct list."""
    _dict = OrderedDict({
        ele: deque() for ele in sorted(files_dict.keys())})
    for row in input_list:
        row = row.rstrip()
        ele = row.split(',')
        word = ele[0]
        genre = ele[1]
        for each in word_list:
            if each == word:
                _dict[genre].append(row)
    return _dict


def convert_to_dict(filename):
    """Convert decision file to dictionary."""
    ret = {}
    with open(filename, 'r', encoding='utf-8') as rows:
        for row in rows:
            word, genre, name, refs = utils.to_list(row, True)
            refs = [ele.strip() for ele in refs]
            ret[W_G_N.format(word, genre, name)] = refs
    return ret


def remove_entry_from(_dict, genre, value):
    """Remove a dictionary entry."""
    return _dict[genre].remove(value)


def check_dict(singleton_dict, decision_dict):
    """Check dictionary data."""
    values = it.chain.from_iterable(singleton_dict.values())
    for value in sorted(values):
        word, genre, name, refs = utils.to_list(value, True)
        # format singleton's key
        key = W_G_N.format(word, genre, name)
        try:
            # if key not included then pass
            decision_dict[key]
        except KeyError:
            pass
        else:
            # otherwise add the new reference(s)
            # and remove (the entry) from singleton's dict
            for ref in refs:
                decision_dict[key].append(ref)
            remove_entry_from(singleton_dict, genre, value)
    return singleton_dict, decision_dict


def merge(_dict, lst):
    """Merge data to decision dictionary."""
    values = it.chain.from_iterable(lst.values())
    for value in sorted(values):
        word, genre, name, refs = utils.to_list(value, True)
        try:
            # populate the decision list with updated references
            _dict[W_G_N.format(word, genre, name)].append(refs)
        except KeyError:
            # adding a new entry to the decision list
            _dict[W_G_N.format(word, genre, name)] = refs
    return _dict


def to_set(_dict):
    """Remove duplicate references."""
    ret = set()
    for key, val in sorted(_dict.items()):
        val = set(utils.clear_sign(val).split(', '))
        val = utils.sort(cfg.BOOKS, val)
        val = utils.clear_sign(val)
        ret.add("{key},{val}".format(
            **{'key': key,
               'val': val.replace(', ', ',')}))
    return ret


def splitting(lists, files_dict):
    """Split lists by gender."""
    _dict = OrderedDict({
        ele: deque() for ele in sorted(files_dict.keys())})
    values = it.chain.from_iterable(lists.values())
    for value in sorted(values):
        word, genre, name = utils.to_list(value)
        # feed each list with the right item
        _dict[genre].append(W_G_N.format(word, genre, name))
    return _dict


def updated_dict(word_dict, word_list):
    """Update the dictionary with new references."""
    for ele in word_list:
        items = ele.split(',')
        word = items[0]
        name = items[2]
        try:
            words = word_dict[name]
            regex = re.compile(r'\b{}\b'.format(word))
            if not regex.findall(words):
                word_dict.update({name: "{},{}".format(words, word)})
        except KeyError:
            word_dict[name] = word
    return word_dict


def ordered(string, line):
    """Define a string as the first item in the list."""
    line = line.rstrip()
    items = deque(line.split(','))
    regex = re.compile(r'\b{}\b'.format(string))
    find = deque(regex.findall(line))
    diff = set(items) - set(find)
    new_line = "{first}{diff}".format(
        **{'first': string,
           'diff': ",{}".format(','.join(diff)) if diff else ''})
    return new_line


def filling(word_dict, word_list):
    """Filling in the dictionary."""
    string = set()
    _dict = updated_dict(word_dict, word_list)
    for key, val in _dict.items():
        string.add(ordered(key, val))
    return string


def save(string, filename):
    """Write to a file."""
    ret = deque()
    with open(filename, 'w', encoding='utf-8') as fout:
        for line in sorted(string):
            fout.write(line)
            fout.write('\n')
            ret.append(line)
    return ret


def update_singletons(lst, _dict):
    """update entry list."""
    ret = set()
    for row in lst:
        search, genre, reference = utils.to_list(row)
        if utils.is_word_saved(_dict[genre], reference, search):
            pass
        else:
            ret.add(row.rstrip())
    return ret


def update_decision(lst, _dict):
    """update entry list."""
    ret = set()
    for row in lst:
        word, genre, name, refs = utils.to_list(row, True)
        key = W_G_N.format(word, genre, name)
        for ref in refs:
            if utils.is_reference_saved(_dict, key, ref):
                pass
            else:
                ret.add(row.rstrip())
    return ret


def main():
    """Process dispatching."""
    try:
        start = time()
        updated = ''
        utils.create_dirs(INPATH)

        # convert input file to list
        filename = os.path.normpath(os.path.join(INPATH, FILENAME))
        input_list = utils.file_to_list(filename)

        # compute word's count
        word_list = utils.load_first_items(input_list)
        word_count = Counter(word_list)

        # determine duplicated word if present
        singleton, duplicate = utils.separate_by(word_count)

        # identify and list items from input file
        duplicates_dict = distribute(input_list, duplicate, cfg.FILES)
        singletons_dict = distribute(input_list, singleton, cfg.FILES)

        # handling duplicate in decision file
        decision_file = os.path.normpath(
            os.path.join(INPATH, cfg.DECISION_LIST_FILE))
        decision_dict = convert_to_dict(decision_file)

        # check if any of the singleton elements have a
        # previous entry in the decision list and then merge
        checked_singletons, checked_decision = check_dict(singletons_dict,
                                                          decision_dict)
        merged_decision_dict = merge(checked_decision, duplicates_dict)
        decision_set = to_set(merged_decision_dict)

        # handling singletons
        strings = {}
        lists = splitting(checked_singletons, cfg.FILES)
        dicts = OrderedDict({
            key: utils.download(val) for key, val in cfg.FILES.items()})
        for genre in cfg.FILES.keys():
            strings[genre] = filling(dicts[genre], lists[genre])

    except FileNotFoundError as err:
        raise err

    else:
        # write in decision file
        save(decision_set, decision_file)

        # write sequentially in other files
        for genre in cfg.FILES.keys():
            save(strings[genre], cfg.FILES[genre])

        # update input list
        dicts_updated = OrderedDict({
            key: utils.download(val) for key, val in cfg.FILES.items()})
        remaining_data = update_singletons(input_list, dicts_updated)
        decision_dict = convert_to_dict(decision_file)
        updated = update_decision(remaining_data, decision_dict)

    finally:
        if updated:
            print("\nReste à faire:\n")
            print(save(updated, os.path.normpath(
                os.path.join(INPATH, filename))))
        print("\n =======")
        print(" = Durée: {0:.2f} min".format(round((time()-start)/60, 2)))
        print(" =======")


if __name__ == '__main__':
    main()
