#!/usr/bin/env python3

# message pour l'utilisateur
MSG1 = "\n« {} »  a pour infinitif : « {} »"
MSG2 = "\n« {} »  n'est pas un verbe en Français"

# liste des verbes (7012)
VERBS_FILE = "verbs.md"

# conjugated verb to search
TEXT = 'consummés'


def get_inf(text):
    """Retourne l'infinitif d'un verbe a partir de sa forme conjuguee.
    * Entree : un verbe conjugue
    * Sortie : l'infinitif du verbe ou None
    """
    conjugated_verbs = None
    try:
        # recupere les formes conjuguees dans une liste
        conjugated_verbs = open(VERBS_FILE, 'r', encoding='utf-8')
        text = text.lower()
        # parcours les formes conjuguées de la liste
        for line in conjugated_verbs:
            # parcours les items d'une ligne si le test est ok
            if text in line:
                items = line.split(',')
                # recupere l'infinitif si le test ok
                for item in items:
                    if item.strip() == text:
                        infinitive = items[-1].strip()
                        return infinitive
    except FileNotFoundError as exc:
        print(exc)
    finally:
        if conjugated_verbs:
            conjugated_verbs.close()
    return None


if __name__ == '__main__':
    SUCCESS = MSG1.format(TEXT.capitalize(), get_inf(TEXT))
    ERROR = MSG2.format(TEXT.capitalize())
    print(SUCCESS if get_inf(TEXT) else ERROR)
