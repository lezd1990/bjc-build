# -*- coding: utf-8 -*-

import mlconjug

def main():
    """Liste les verbes utilisés dans le module mlconjug."""
    try:
        conjugator = mlconjug.Conjugator(language='fr')
        verbs = conjugator.conjug_manager.verbs
        with open("verbs.md", 'w', encoding='utf-8') as f:
            for verb in verbs:
                f.write(verb)
                f.write("\n")
        print("Fin...")
    except FileNotFoundError as exc:
        print(exc)


if __name__ == '__main__':
    main()
