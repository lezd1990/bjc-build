#!/usr/bin/env python3

"""BJC-VERIFY-ADDED-WORD.

This program prints sequentially added words (output of "bym_concordance"),
to perform verification and modification of their genre.
Based on a pre-formatted file that includes respectively following model:
    - added word,
    - genre,
    - designated form,
    - verse's references.
    Ex.: appelé,v,appeler,1 S 3:5, 1 Co 7:22
It will help determine the repository file of a word.

These information helps to locate new word's repository file.

To run it smoothly, make sure to :
 1. set the correct paths: INPATH and OUTPATH
 2. install additionnal library: colorama
"""

import logging
import os
import re
import sys
from time import time, sleep
from collections import defaultdict, OrderedDict

try:
    import colorama
    colorama.init()
except ImportError:
    # required for windows platform
    if sys.platform.startswith('win'):
        raise

import configuration as cfg
import utils
from utils import red, green, yellow, blue, magenta, cyan


LOGGER = logging.getLogger(__name__)

DIRNAME = os.path.dirname(__file__)
INPATH = os.path.normpath(os.path.join(DIRNAME, '../source/'))
OUTPATH = os.path.normpath(os.path.join(DIRNAME, '../output/fr/md/'))

# variable initialisation
IN_FILENAME = 'added_word.md'            # input
OUT_FILENAME = 'verify_added_word.md'    # output

EXT = ".md"
CACHE = defaultdict(set)
MAX_DIGIT = 69    # max digit per line
CENTER_DIGIT = 3  # centered digit
RIGHT_BAR_DIGIT = 52  # right bar digit
NB_ATTEMPT = 3

# regex pattern
RE_CHOICE = re.compile(r'^([1-4]{1})$')             # to validate choice
RE_PROPOSAL = re.compile(r"^[a-zâàéèêëôôîïûüç\-']*")  # to validate proposal
YES_OR_NOT = ('o', 'oui', 'n', 'non')               # to confirm proposal
YES = ('o', 'oui')                              # to reject any other answer

# dictionary of genre
DICT_GENRE = OrderedDict([
    ("adj", magenta("ADJECTIF")),
    ("n", yellow("NOM COMMUN")),
    ("np", green("NOM PROPRE")),
    ("v", red("VERBE")),
    ("", ""),
    ("p", cyan("Passer")),
    ("eq", cyan("Enregistrer & Quitter")),
    ("q", cyan("Quitter"))
])

CHOICE_LIST = [str(i+1) for i, j in enumerate(DICT_GENRE.values())]
SKIP = CHOICE_LIST[-3]
SAVE_AND_QUIT = CHOICE_LIST[-2]
CLEA_AND_QUIT = CHOICE_LIST[-1]


def init_logger():
    """create and configure logger."""
    LOGGER.setLevel(cfg.LOGGER_LEVEL)
    formatter = logging.Formatter(
        '[%(asctime)s:%(module)s:%(lineno)s:%(levelname)s] %(message)s')
    log_file = os.path.join(DIRNAME, "{filenam}{log_ext}".format(
        **{'filenam': os.path.basename(sys.argv[0].split('.')[0]),
           'log_ext': '.log'}))
    file_handler = logging.FileHandler(log_file)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    LOGGER.addHandler(file_handler)


def pre_format_list(dict_of_verses, filename):
    """Pre-format input list to a model."""
    with open(filename, 'r', encoding='utf-8') as lines:
        for line in lines:
            line = line.rstrip().rsplit('\t', 2)
            word = line[0].strip(',')
            refs = line[1].split(", ")
            length = utils.count_word(word, refs, dict_of_verses)
            index = 0
            for ref in refs:
                ref = utils.clear_sign(ref)
                verse = dict_of_verses[ref.strip()]
                LOGGER.debug("Verse: %s %s", ref, verse)
                for pos in utils.finder(word, verse):
                    index += 1
                    position = (pos.start(), pos.end())
                    LOGGER.debug("Added « %s »: %s/%s", word, index, length)
                    LOGGER.debug("Range: %s", position)
                    yield word, ref, verse, (index, length), position


def format_word(word, numbering):
    """Format word and numbering in a frame."""
    model = "{barG}{word}{barC}{num}{/}{len}{barD}\n{=}"
    span = MAX_DIGIT + (CENTER_DIGIT * 2)
    index = numbering[0]
    length = numbering[1]
    formatted = model.format(
        **{'barG': cyan(' ======( '),
           'word': blue(word),
           'barC': cyan(' )======>  '),
           'num': blue(index),
           '/': cyan(' / '),
           'len': blue(length),
           'barD': cyan('  <{}'.format(
               '='*(RIGHT_BAR_DIGIT - (
                   len(word) +
                   len(str(index)) +
                   len(str(length)))))),
           '=': cyan(' ={}=\n ={}= '.format(' '*span, ' '*span))})
    LOGGER.debug("Word and numbering formatted in a frame")
    return formatted


def printing(string, position, coordinates):
    """Print colored text at a given position."""
    LOGGER.debug("Printing verse with color at a given position")
    i = 0
    status = False
    if position[0] in range(coordinates[0], coordinates[1]):
        start = position[0] - coordinates[0]
        end = (position[1] - position[0]) + start
        status = True
    print(cyan(' =  '), end=' ')
    for pos, char in enumerate(string):
        if i > MAX_DIGIT:
            print(cyan('  = \n =  '), end='')
            i = 0
        if status and pos in range(start, end):
            print(blue(char), end='')
        else:
            print(char, end='')
        i += 1
    print(cyan('{}='.format(' '*((MAX_DIGIT - i) + CENTER_DIGIT))))


def outline(string, position):
    """Compute text to print."""
    LOGGER.debug("Generating coordinates")
    for coordinat in utils.coordinate(string, MAX_DIGIT):
        string_sliced = string[coordinat[0]:coordinat[1]]
        printing(string_sliced, position, coordinat)


def format_reference(ref):
    """Return colored reference."""
    span = MAX_DIGIT + (CENTER_DIGIT * 2)
    reference = " {=}\n{barG} {ref}{barD}\n\n".format(
        **{'=': cyan('={}=\n ={}='.format(' '*span, ' '*span)),
           'barG': cyan(' ======>'),
           'ref': blue(ref),
           'barD': cyan(' <{}'.format('='*(67-(len(ref)))))})
    LOGGER.debug("Reference formatted in a frame")
    return reference


def display(word, ref, verse, numbering, position):
    """Display text in a shape."""
    LOGGER.debug("Displaying text in a shape")
    utils.clear()
    print("{T_MAIN}\n\n\n {T_VERS}\n{B_VERS}\n\n{S_WORD}".format(
        **{'T_MAIN': cyan(cfg.TITLE_MAIN),
           'T_VERS': cfg.TITLE_VERSE,
           'B_VERS': cyan(cfg.BARRE_VERSE),
           'S_WORD': format_word(word, numbering)}))
    outline(verse, position)
    print("{S_REF}\n\n {T_CHOICE}\n{B_CHOICE}\n".format(
        **{'S_REF': format_reference(ref),
           'T_CHOICE': cfg.TITLE_CHOICE,
           'B_CHOICE': cyan(cfg.UNDERLINE)}))
    for index, req in enumerate(DICT_GENRE.items()):
        print("  {index}  {res}".format(
            **{'index': ((str(index+1))
                         if req.__getitem__(0).strip() != ''
                         else cyan("\n " + cfg.BARRE_HAUTE)),
               'req': req.__getitem__(0).strip(),
               'res': req.__getitem__(1)}))
    print('\n')


def save_and_exit(filename, word_dict):
    """Save before exiting."""
    try:
        if word_dict:
            LOGGER.debug("Save record before exiting")
            write(filename, word_dict)
        else:
            LOGGER.debug("No record to save")
    finally:
        exit_without_saving()


def exit_without_saving():
    """Clear before exiting."""
    LOGGER.debug("Clear and exit")
    utils.clear()
    sys.exit()


def dict_by(genre):
    """Return dict by genre."""
    dicts = OrderedDict({
        key: utils.download(val) for key, val in cfg.FILES.items()})
    return dicts[genre]


def show_proposal():
    """Show proposal."""
    utils.clear()
    print("{T_MAIN}\n\n\n {T_PROP}\n{B_PROP}\n{DESIGN}\n\n\n".format(
        **{'T_MAIN': cyan(cfg.TITLE_MAIN),
           'T_PROP': cfg.TITLE_PROPOS,
           'B_PROP': cyan(cfg.UNDERLINE),
           'DESIGN': cfg.DESIGNATED_MSG.format(
               magenta("Adjectif"),
               yellow("Nom"),
               red("Verbe"))}))


def message_by(genre):
    """Return message by genre."""
    return cfg.MESSAGES[genre]


def get_proposal(word, message):
    """Inconditionnal return of a proposal."""
    i = NB_ATTEMPT
    while True:
        answer = input(message.format(word)).strip().lower()
        if RE_PROPOSAL.match(answer):
            conf = input(cfg.CONFIRM_MSG.format(answer)).strip().lower()
            if conf in YES_OR_NOT and conf in YES:
                return answer
        if i <= 1:
            print(red(cfg.EXIT_MSG))
            sys.exit()
        i -= 1


def get_designated_or_proposed(word, genre):
    """Get the designated word or set a proposal."""
    try:
        # return word on KeyError if genre is 'np'
        message = message_by(genre)
    except KeyError:
        return word
    # if we have cached the value, then return it
    if CACHE[word, genre]:
        return CACHE[word, genre]
    # find the designated word
    designated = utils.get_first_item(word, dict_by(genre))
    if designated:
        # cache the designated and return it
        CACHE[word, genre] = designated
        return designated
    show_proposal()
    proposal = get_proposal(word, message)
    if word not in cfg.HOMONYM_VERBS:
        # cache the proposal and return it
        CACHE[word, genre] = proposal
    return proposal


def process_choice(choice, word):
    """Pick or set a name."""
    LOGGER.debug("Processing choice to pick or set a name")
    genre = utils.map_choice(choice, DICT_GENRE)
    LOGGER.debug("Selected genre: %s", genre)
    name = get_designated_or_proposed(word, genre)
    LOGGER.debug("Name: %s", name)
    return genre, name


def attempt(i):
    """Attempt count."""
    if i == 1:
        print(cyan(cfg.VALUE_ERROR_MSG))
        LOGGER.warning("Wrong choice, last attempt")
        sleep(2)
    if i < 1:
        print(red(cfg.EXIT_MSG))
        LOGGER.warning("Process interrupted")
        sys.exit()


def switch_choices(choice, word, filename, verified, i):
    """Handle choices."""
    LOGGER.debug("Handling choices")
    picked_choice = choice if RE_CHOICE.match(choice) else 0
    return {
        SAVE_AND_QUIT: lambda: save_and_exit(filename, verified),
        CLEA_AND_QUIT: lambda: exit_without_saving(),
        picked_choice: lambda: process_choice(choice, word)
    }.get(choice, lambda: attempt(i))()


def verify(word_param, filename):
    """Return verified words."""
    verified = defaultdict(list)
    for word, ref, verse, numbering, position in word_param:
        i = NB_ATTEMPT - 2  # limit choice attempt to 2
        while True:
            display(word, ref, verse, numbering, position)
            choice = input(cfg.CHOICE_MSG)
            if choice == SKIP:
                LOGGER.info("Skip « %s »: %s", word, ref)
                break
            LOGGER.info("Processing choice for « %s »", word)
            ret = switch_choices(choice, word, filename, verified, i)
            if ret:
                # ret[0]: genre ; ret[1] # name
                LOGGER.debug("Adding verified word to dict: %s", ret)
                verified[word, ret[0], ret[1]].append(ref)
                break
            i -= 1
    LOGGER.info("Finish verifying words")
    return verified


def write(filename, word_dict):
    """Write a pre-formatted list."""
    with open(filename, 'w', encoding='utf-8') as fout:
        for key, val in word_dict.items():
            refs = utils.clear_sign(val)
            refs = [ref.strip() for ref in refs.split(',')]
            refs = utils.clear_sign(refs)
            refs = str(refs).replace(', ', ',')
            fout.write('{w},{g},{n},{r}\n'.format(
                **{'w': key[0],       # w: word
                   'g': key[1],       # g: genre
                   'n': key[2],       # n: name
                   'r': str(refs)}))  # r: references
        return fout.name


def main():
    """Verify and dispatch words in an appropriate file."""
    try:
        start = time()
        init_logger()
        LOGGER.info("Creating directories if not exist")
        utils.create_dirs(INPATH, OUTPATH)
        # load verses in a dict
        LOGGER.info("Selecting the appropriate books")
        files = utils.filter_file(EXT, sorted(os.listdir(INPATH)))
        LOGGER.info("Extracting only verses from books")
        verses = utils.extract_verse(INPATH, files)
        LOGGER.info("Load verses in a dict")
        dict_of_verses = utils.load_verses(verses, cfg.DICT_BOOKS)
        LOGGER.info("Dictionary of verses is setted.")
        # format input list
        LOGGER.info("Preformatting added words")
        word_param = pre_format_list(
            dict_of_verses,
            os.path.normpath(os.path.join(OUTPATH, IN_FILENAME)))
        # set a genre to each word selected
        out_file = os.path.normpath(
            os.path.join(OUTPATH, OUT_FILENAME))
        LOGGER.info("Setting a genre to added words")
        verified = verify(word_param, out_file)
    except FileNotFoundError as err:
        raise err
    except KeyboardInterrupt:
        LOGGER.error("Program stopped by the user")
        print(red(cfg.INTERRUPT_MSG))
    else:
        # write in file
        LOGGER.info("Saving verified word(s) to file")
        write(out_file, verified)
        utils.clear()
    finally:
        end = time()
        tot = (end - start)/60
        print(cyan("\n ======="))
        print(cyan(" = Durée : {0:.2f} min").format(round(tot, 2)))
        print(cyan(" ======="))


if __name__ == '__main__':
    main()
