"""BJC-VERIFY-ADDED-WORD CONFIGURATION FILE."""

import logging
import os
from collections import OrderedDict


DIRNAME = os.path.dirname(__file__)
PATH = os.path.normpath(os.path.join(DIRNAME, '../output/fr/md/'))

#############################
# User information variable #
#############################

TITLE_MAIN = """
 =============================================================================
 =            BYM - CONCORDANCE : VERIFICATION DE GENRES DES MOTS            =
 ============================================================================="""

TITLE_VERSE = "<A> Identifier le genre du mot en surbrillance :"
BARRE_VERSE = "     =========================================="
TITLE_CHOICE = "<B> Choisir le chiffre correspondant au genre :"
UNDERLINE = "     ========================================="
TITLE_PROPOS = "<C> Proposer une désignation pour votre choix :"

DESIGNATED_MSG = """
     - {} : genre masculin et singulier

     - {} : genre masculin et singulier

     - {} : infinitif"""

CHOICE_MSG = " >>> Veuillez saisir votre choix, puis valider : "
ADJECTIVE_MSG = "\n >>> Veuillez saisir la désignation de l'adjectif « {} » : "
NOUN_MSG = "\n >>> Veuillez saisir la désignation du nom « {} » : "
VERB_MSG = "\n >>> Veuillez saisir la désignation du verbe « {} » : "
CONFIRM_MSG = "\n >>> Confirmez-vous la saisie suivante « {} » ? (O/N) : "

EXIT_MSG = """

 =============================================================================
 =                           Processus interrompu                            =
 ============================================================================="""

VALUE_ERROR_MSG = """
 =============================================================================
 =  Choix erroné, veuillez procéder au dernier essai !                       =
 ============================================================================="""

INTERRUPT_MSG = """


 =============================================================================
 =                    Programme arrêté par l'utilisateur                     =
 ============================================================================="""

BARRE_HAUTE = "============================================================================="

ADJECTIVES_FILE = 'adjectives.md'        # adjectifs
NOUNS_FILE = 'nouns.md'                  # noms
PROPER_NOUNS_FILE = 'proper_nouns.md'    # noms propres
VERBS_FILE = 'verbs.md'                  # verbes
DECISION_LIST_FILE = 'decision_list.md'  # decision list

HOMONYM_VERBS = (
    'tues',  # to catch verbs: tues => taire ou tuer
    'suis'   # to catch verbs: suis => être ou suivre
)

# dictionary of book shortcut
DICT_BOOKS = OrderedDict([
    ('01', 'Ge'), ('02', 'Ex'), ('03', 'Lé'), ('04', 'No'), ('05', 'De'),
    ('06', 'Jos'), ('07', 'Jg'), ('08', '1 S'), ('09', '2 S'), ('10', '1 R'),
    ('11', '2 R'), ('12', 'Es'), ('13', 'Jé'), ('14', 'Ez'), ('15', 'Os'),
    ('16', 'Joë'), ('17', 'Am'), ('18', 'Ab'), ('19', 'Jon'), ('20', 'Mi'),
    ('21', 'Na'), ('22', 'Ha'), ('23', 'So'), ('24', 'Ag'), ('25', 'Za'),
    ('26', 'Mal'), ('27', 'Ps'), ('28', 'Pr'), ('29', 'Job'), ('30', 'Ca'),
    ('31', 'Ru'), ('32', 'La'), ('33', 'Ec'), ('34', 'Est'), ('35', 'Da'),
    ('36', 'Esd'), ('37', 'Né'), ('38', '1 Ch'), ('39', '2 Ch'), ('40', 'Mt'),
    ('41', 'Mc'), ('42', 'Lu'), ('43', 'Jn'), ('44', 'Ac'), ('45', 'Ja'),
    ('46', 'Ga'), ('47', '1 Th'), ('48', '2 Th'), ('49', '1 Co'),
    ('50', '2 Co'), ('51', 'Ro'), ('52', 'Ep'), ('53', 'Ph'), ('54', 'Col'),
    ('55', 'Phm'), ('56', '1 Ti'), ('57', 'Tit'), ('58', '1 Pi'),
    ('59', '2 Pi'), ('60', '2 Ti'), ('61', 'Jud'), ('62', 'Hé'),
    ('63', '1 Jn'), ('64', '2 Jn'), ('65', '3 Jn'), ('66', 'Ap')
])

# list of books
BOOKS = [n[1] for n in DICT_BOOKS.items()]

# dictionnary of files
FILES = OrderedDict({
    ('adj', os.path.normpath(os.path.join(PATH, ADJECTIVES_FILE))),
    ('n', os.path.normpath(os.path.join(PATH, NOUNS_FILE))),
    ('np', os.path.normpath(os.path.join(PATH, PROPER_NOUNS_FILE))),
    ('v', os.path.normpath(os.path.join(PATH, VERBS_FILE)))})

# dictionnary of messages
MESSAGES = OrderedDict({
    ('adj', ADJECTIVE_MSG),
    ('n', NOUN_MSG),
    ('v', VERB_MSG)})

# set log level
LOGGER_LEVEL = logging.INFO
