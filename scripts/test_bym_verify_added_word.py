"""BJC-VERIFY-ADDED-WORD."""

from unittest.mock import patch, MagicMock, Mock, mock_open
import unittest
import os
import re
import sys
import types
from collections import OrderedDict
from io import StringIO

import bym_verify_added_word as bv

DIR = os.path.dirname(__file__)
FIXTURES = os.path.normpath(os.path.join(DIR, 'fixtures'))


class MockMkdir(object):
    """Fake mkdir."""

    received_args = None

    def __init__(self):
        self.received_args = []

    def __call__(self, *args):
        for arg in args:
            if arg is None:
                raise TypeError('oops')
            elif arg == '':
                raise FileNotFoundError('oops')
            else:
                self.received_args.append(arg)
        return True


class TestBymVerifyCommon(unittest.TestCase):
    """Test class: utils.create_dirs function."""

    def setUp(self):
        self.held, sys.stdout = sys.stdout, StringIO()
        self.maxDiff = None  # enable to get full diff output
        self.text = 'Elohîm donc fit le grand luminaire et le petit luminaire'

    def test_clear_sign(self):
        string = ['Ps 83:14', 'Mc 8:29', 'Hé 6:17']
        res = bv.utils.clear_sign(string)
        expected = 'Ps 83:14, Mc 8:29, Hé 6:17'
        self.assertEqual(expected, res)

    def test_finder_iterable(self):
        string = 'luminaire'
        res = bv.utils.finder(string, self.text)
        self.assertTrue(iter(res))

    def test_finder_strings(self):
        string = 'luminaire'
        expected = ['luminaire', 'luminaire']
        res = []
        for ele in bv.utils.finder(string, self.text):
            res.append(ele.group())
        self.assertEqual(expected, res)

    def test_finder_empty_searched_string(self):
        string = ""
        expected = [i * '' for i in range(20)]
        res = []
        for ele in bv.utils.finder(string, self.text):
            res.append(ele.group())
        self.assertEqual(expected, res)

    def test_finder_unknown_string(self):
        string = "blabla"
        expected = []
        res = []
        for ele in bv.utils.finder(string, self.text):
            res.append(ele.group())
        self.assertEqual(expected, res)

    def test_clear(self):
        expected = ''
        res = bv.utils.clear()
        output = sys.stdout.getvalue()
        self.assertEqual(expected, output)
        self.assertIsNone(res)


class TestBymVerifyCreateDirs(unittest.TestCase):
    """Test class: utils.create_dirs function."""

    def test_create_one_directory(self):
        dir_a = os.path.normpath(os.path.join(DIR, 'dir_a'))
        fake = MockMkdir()
        with patch('os.mkdir', lambda *x: fake(*x)):
            with patch('os.path.exists', return_value=False):
                bv.utils.create_dirs(dir_a)
        self.assertEqual(len(fake.received_args), 1)
        self.assertEqual(fake.received_args[0], dir_a)

    def test_create_directories(self):
        dir_a = os.path.normpath(os.path.join(DIR, 'dir_a'))
        dir_b = os.path.normpath(os.path.join(DIR, 'dir_b'))
        fake = MockMkdir()
        with patch('os.mkdir', lambda *x: fake(*x)):
            with patch('os.path.exists', return_value=False):
                bv.utils.create_dirs(dir_a, dir_b)
        self.assertEqual(len(fake.received_args), 2)
        self.assertEqual(fake.received_args[0], dir_a)
        self.assertEqual(fake.received_args[1], dir_b)

    def test_create_dir_with_no_arg(self):
        fake = MockMkdir()
        with patch('os.mkdir', lambda *x: fake(*x)):
            with patch('os.path.exists', return_value=False):
                with self.assertRaises(TypeError):
                    bv.utils.create_dirs(None)

    def test_create_dir_with_empty_arg(self):
        fake = MockMkdir()
        with patch('os.mkdir', lambda *x: fake(*x)):
            with patch('os.path.exists', return_value=False):
                with self.assertRaises(FileNotFoundError):
                    bv.utils.create_dirs('')


class TestBymVerifyFilterFile(unittest.TestCase):
    """Test class: utils.filter_file function."""

    def test_filter_files(self):
        fi_ext = '.md'
        res = bv.utils.filter_file(fi_ext, [
            'file.txt',
            '06-file_a.md',
            '23-file_d.md',
            'file_c.csv',
            'no-bad_file.md',
            'file_b.md'])
        self.assertEqual(types.GeneratorType, type(res))
        res = [ele for ele in res]
        self.assertEqual(2, len(res))
        self.assertTrue('06-file_a.md' in res)
        self.assertTrue('23-file_d.md' in res)
        self.assertFalse('file.txt' in res)
        self.assertFalse('file_c.csv' in res)
        self.assertFalse('file_b.md' in res)
        self.assertFalse('no-bad_file.md' in res)

    def test_filter_file_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.filter_file(None)

    def test_filter_file_with_empty_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.filter_file('')


class TestBymVerifyExtractVerse(unittest.TestCase):
    """Test class: utils.extract_verse function."""

    def test_extract_verse_files(self):
        expected = [
            ("57-Tite.md", ("1:3", "mais qu'il a manifestée en son...")),
            ("58-1Pierre.md", ("4:9", "Soyez hospitaliers les uns...")),
            ("59-2Pierre.md", ("1:12", "C'est pourquoi je ne..."))]
        res = bv.utils.extract_verse(FIXTURES, os.listdir(FIXTURES))
        self.assertEqual(types.GeneratorType, type(res))
        results = [(filename, verse.groups()) for filename, verse in res]
        self.assertEqual(len(results), 3)
        self.assertIn(expected[0], results)
        self.assertIn(expected[1], results)
        self.assertIn(expected[2], results)

    def test_extract_verse_one_file(self):
        book = "57-Tite.md"
        number = "1:3"
        content = "mais qu'il a manifestée en son..."
        expected = (book, (number, content))
        res = bv.utils.extract_verse(FIXTURES, [book])
        self.assertEqual(types.GeneratorType, type(res))
        results = [(filename, verse) for filename, verse in res]
        self.assertEqual(len(results), 1)
        self.assertEqual(expected, (results[0][0], results[0][1].groups()))

    def test_extract_verse_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.extract_verse(None)

    def test_extract_verse_with_empty_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.extract_verse('')


class TestBymVerifyLoadVerses(unittest.TestCase):
    """Test class: utils.load_verses function."""

    def test_short_name(self):
        dict_books = OrderedDict([('01', 'Ge'), ('02', 'Ex'), ('03', 'Lé')])
        integer = '02'
        res = bv.utils.short_name(integer, dict_books)
        self.assertEqual('Ex', res)

    def test_short_name_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.short_name(None)

    def test_short_name_with_empty_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.short_name('')

    def test_sub_comments(self):
        text = "je te prie, avaler<!--Dévorer.--> de ce roux<!--Voir.-->,"
        expected = "je te prie, avaler de ce roux,"
        res = bv.utils.sub_comments(text)
        self.assertEqual(expected, res)

    def test_sub_comments_and_strong(self):
        text = """<w lemma="strong:H07225">Au commencement</w><!--Bereshit bara elohîm èth hashamaïm vehèth ah aretz.--> <w lemma="strong:H0430">Elohîm</w> <w morph="strongMorph:TH8804" lemma="strong:H0853 strong:H01254">créa</w> <w lemma="strong:H08064">les cieux</w> <w lemma="strong:H0853">et</w> <w lemma="strong:H0776">la Terre</w>."""
        expected = "Au commencement Elohîm créa les cieux et la Terre."
        res = bv.utils.sub_comments(text)
        self.assertEqual(expected, res)

    def test_remove_comment_with_empty_arg(self):
        expected = ''
        res = bv.utils.sub_comments('')
        self.assertEqual(expected, res)

    def test_remove_comment_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.sub_comments(None)

    def test_load_verses(self):
        re_verse = re.compile(r'^([0-9]+:[0-9]+)\t(.+)')
        verses = [
            ("57-Tite.md", re_verse.match("1:3\tmais qu'il a manifestée en")),
            ("58-1Pierre.md", re_verse.match("4:9\tSoyez hospitaliers les")),
            ("59-2Pierre.md", re_verse.match("1:12\tC'est pourquoi je ne"))]
        dict_books = OrderedDict([('57', 'Tit'), ('58', '1 Pi'), ('59', '2 Pi')])
        res = bv.utils.load_verses(verses, dict_books)
        expected = {
            "Tit 1:3": "mais qu'il a manifestée en",
            "1 Pi 4:9": "Soyez hospitaliers les",
            "2 Pi 1:12": "C'est pourquoi je ne"}
        self.assertEqual(dict, type(res))
        self.assertEqual(expected, res)

    def test_load_verses_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.load_verses(None)

    def test_load_verses_with_empty_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.load_verses('')


class TestBymVerifyPreFormatList(unittest.TestCase):
    """Test class: pre_format_list function."""

    def test_pre_format_list(self):
        dict_of_verses = {
            "Ps 107:29": "Il a ramené le calme, et les vagues se sont tues.",
            "Mt 23:37": "Yeroushalaim ! Yeroushalaim ! Qui tues les prophètes",
            "Lu 13:34": "Yeroushalaim ! Yeroushalaim ! Qui tues les prophètes"}
        res = bv.pre_format_list(dict_of_verses,
                                 os.path.normpath(os.path.join(FIXTURES, 'added_word.md')))

        # first iteration
        first = next(res)
        result = first[0]
        expected = 'tues'
        self.assertEqual(expected, result)

        result = first[1]
        expected = 'Ps 107:29'
        self.assertEqual(expected, result)

        result = first[2]
        expected = "Il a ramené le calme, et les vagues se sont tues."
        self.assertEqual(expected, result)

        result = first[3]
        expected = (1, 3)
        self.assertEqual(expected, result)

        result = first[4]
        expected = (44, 48)
        self.assertEqual(expected, result)

        # second iteration
        second = next(res)
        result = second[0]
        expected = 'tues'
        self.assertEqual(expected, result)

        result = second[1]
        expected = 'Mt 23:37'
        self.assertEqual(expected, result)

        result = second[2]
        expected = "Yeroushalaim ! Yeroushalaim ! Qui tues les prophètes"
        self.assertEqual(expected, result)

        result = second[3]
        expected = (2, 3)
        self.assertEqual(expected, result)

        result = second[4]
        expected = (34, 38)
        self.assertEqual(expected, result)

    def test_pre_format_list_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.pre_format_list(None)

    def test_pre_format_list_with_empty_arg(self):
        with self.assertRaises(TypeError):
            bv.pre_format_list('')


class TestBymVerifyDisplay(unittest.TestCase):
    """Test class: display function."""

    def setUp(self):
        self.held, sys.stdout = sys.stdout, StringIO()
        self.maxDiff = None  # enable to get full diff output
        self.red = '\x1b[31m'
        self.green = '\x1b[32m'
        self.yellow = '\x1b[33m'
        self.blue = '\x1b[34m'
        self.magenta = '\x1b[35m'
        self.cyan = '\x1b[36m'
        self.end = '\x1b[0m'

    def blue_color(self, word):
        ret = []
        for i in word:
            ret.append(self.blue + i + self.end)
        return ''.join(ret)

    def test_format_word(self):
        word = 'word'
        numbering = (1, 3)
        res = bv.format_word(word, numbering)
        model = '{barG}{word}{barC}{num}{/}{len}{barD}\n{=}'
        expected = model.format(
            **{'barG': self.cyan + ' ======( ' + self.end,
               'word': self.blue + word + self.end,
               'barC': self.cyan + ' )======>  ' + self.end,
               'num': self.blue + str(numbering[0]) + self.end,
               '/': self.cyan + ' / ' + self.end,
               'len': self.blue + str(numbering[1]) + self.end,
               'barD': self.cyan + '  <' + '='*46 + self.end,
               '=': self.cyan + ' =' + ' '*75 + '=\n =' + ' '*75 + '= ' + self.end
              })
        self.assertEqual(expected, res)

    def test_coordinate(self):
        string = 'Voici que votre maison vous est laissée déserte.'
        max_digit = 20
        res = bv.utils.coordinate(string, max_digit)
        expected = [(0, 15), (16, 31), (32, 48)]
        self.assertEqual(expected, res)

    def test_printing_one_line(self):
        string = 'Vanité des vanités, dit Qohelet, tout est vanité.'
        position = (24, 31)  # Qohelet
        word = 'Qohelet'
        coordinates = [(0, 48)]
        model = "{pre}{vl1}{suf}\n"
        expected = model.format(
            **{'pre': self.cyan + " =  " + self.end,
               'vl1': ' Vanité des vanités, dit ' + self.blue_color(word) + ', tout est vanité.',
               'suf': self.cyan + ' '*23 + '=' + self.end
              })
        for coordinate in coordinates:
            bv.printing(string, position, coordinate)
            output = sys.stdout.getvalue()
            self.assertEqual(expected, output)

    def test_printing_multi_line(self):
        string = "C'est pour cela que je vous ai envoyé Timotheos, qui est mon fils bien-aimé, et qui est fidèle dans le Seigneur, afin qu'il vous rappelle quelles sont mes voies en Mashiah et comment j'enseigne partout dans toutes les assemblées."
        position = (185, 193)  # enseigne
        word = 'enseigne'
        coordinates = [(0, 65), (66, 128), (129, 193), (194, 229)]
        model = "{pre1}{vl1}{suf1}\n{pre2}{vl2}{suf2}\n{pre3}{vl31}{vl32}{suf3}\n{pre4}{vl4}{suf4}\n"
        expected = model.format(
            **{'pre1': self.cyan + " =  " + self.end + ' ',
               'vl1': string[0:65],
               'suf1': self.cyan + ' '*7 + '=' + self.end,
               'pre2': self.cyan + " =  " + self.end + ' ',
               'vl2': string[66:128],
               'suf2': self.cyan + ' '*10 + '=' + self.end,
               'pre3': self.cyan + " =  " + self.end + ' ',
               'vl31': "rappelle quelles sont mes voies en Mashiah et comment j'",
               'vl32': self.blue_color(word),
               'suf3': self.cyan + ' '*8 + '=' + self.end,
               'pre4': self.cyan + " =  " + self.end + ' ',
               'vl4': string[194:229],
               'suf4': self.cyan + ' '*37 + '=' + self.end
              })
        for coordinate in coordinates:
            bv.printing(string[coordinate[0]:coordinate[1]], position, coordinate)
            output = sys.stdout.getvalue()
        self.assertEqual(expected, output)

    def test_outline_one_line(self):
        string = "Vanité des vanités, dit Qohelet, tout est vanité."
        position = (24, 31)  # Qohelet
        word = 'Qohelet'
        model = "{pre}{vl1}{suf}\n"
        expected = model.format(
            **{'pre': self.cyan + " =  " + self.end,
               'vl1': ' Vanité des vanités, dit ' + self.blue_color(word) + ', tout est vanité.',
               'suf': self.cyan + ' '*23 + '=' + self.end
              })
        bv.outline(string, position)
        output = sys.stdout.getvalue()
        self.assertEqual(expected, output)

    def test_outline_multi_line(self):
        string = "C'est pour cela que je vous ai envoyé Timotheos, qui est mon fils bien-aimé, et qui est fidèle dans le Seigneur, afin qu'il vous rappelle quelles sont mes voies en Mashiah et comment j'enseigne partout dans toutes les assemblées."
        position = (185, 193)  # enseigne
        word = 'enseigne'
        model = "{pre1}{vl1}{suf1}\n{pre2}{vl2}{suf2}\n{pre3}{vl31}{vl32}{suf3}\n{pre4}{vl4}{suf4}\n"
        expected = model.format(
            **{'pre1': self.cyan + " =  " + self.end + ' ',
               'vl1': string[0:65],
               'suf1': self.cyan + ' '*7 + '=' + self.end,
               'pre2': self.cyan + " =  " + self.end + ' ',
               'vl2': string[66:128],
               'suf2': self.cyan + ' '*10 + '=' + self.end,
               'pre3': self.cyan + " =  " + self.end + ' ',
               'vl31': "rappelle quelles sont mes voies en Mashiah et comment j'",
               'vl32': self.blue_color(word),
               'suf3': self.cyan + ' '*8 + '=' + self.end,
               'pre4': self.cyan + " =  " + self.end + ' ',
               'vl4': string[194:229],
               'suf4': self.cyan + ' '*37 + '=' + self.end
              })
        bv.outline(string, position)
        output = sys.stdout.getvalue()
        self.assertEqual(expected, output)

    def test_format_reference(self):
        ref = 'Mt 23:37'
        res = bv.format_reference(ref)
        model = ' {=}\n{barG} {ref}{barD}\n\n'
        expected = model.format(
            **{'=': self.cyan + '=' + ' '*75 + '=\n =' + ' '*75 + '=' + self.end,
               'barG': self.cyan + ' ======>' + self.end,
               'ref': self.blue + ref + self.end,
               'barD': self.cyan + ' <' + '='*59 + self.end
              })
        self.assertEqual(expected, res)

    def test_display_verse_one_line(self):
        word = 'enseigne'
        ref = 'Ps 119:12'
        verse = "YHWH ! tu es béni, enseigne-moi tes statuts !"
        numbering = (16, 45)
        position = (19, 27)
        msg_main = 'BYM - CONCORDANCE : VERIFICATION DE GENRES DES MOTS'
        model_main = '\n {l1}\n ={l2}=\n {l3}'.format(
            **{'l1': '='*77,
               'l2': ' '*12 + msg_main + ' '*12,
               'l3': '='*77
               })
        t_main = self.cyan + model_main + self.end
        msg_vers = '<A> Identifier le genre du mot en surbrillance :'
        t_vers = '{t_prop}\n{b_prop}'.format(
            **{'t_prop': msg_vers,
               'b_prop': self.cyan + ' '*5 + "="*42 + self.end,
               })
        model_s_word = '{barG}{word}{barC}{num}{/}{len}{barD}\n{=}'
        s_word = model_s_word.format(
            **{'barG': self.cyan + ' ======( ' + self.end,
               'word': self.blue + word + self.end,
               'barC': self.cyan + ' )======>  ' + self.end,
               'num': self.blue + str(numbering[0]) + self.end,
               '/': self.cyan + ' / ' + self.end,
               'len': self.blue + str(numbering[1]) + self.end,
               'barD': self.cyan + '  <' + '='*40 + self.end,
               '=': self.cyan + ' =' + ' '*75 + '=\n =' + ' '*75 + '= ' + self.end
              })
        block_1 = '{t_main}\n\n\n {t_vers}\n\n{s_word}'.format(
            **{'t_main': t_main,
               't_vers': t_vers,
               's_word': s_word,
               })
        model_block_2 = "{pre}{vl1}{suf}\n"
        block_2 = model_block_2.format(
            **{'pre': self.cyan + " =  " + self.end,
               'vl1': " YHWH ! tu es béni, " + self.blue_color(word) + "-moi tes statuts !",
               'suf': self.cyan + ' '*27 + '=' + self.end
              })
        model_s_ref = ' {=}\n{barG} {ref}{barD}\n\n'
        s_ref = model_s_ref.format(
            **{'=': self.cyan + '=' + ' '*75 + '=\n =' + ' '*75 + '=' + self.end,
               'barG': self.cyan + ' ======>' + self.end,
               'ref': self.blue + ref + self.end,
               'barD': self.cyan + ' <' + '='*58 + self.end
              })
        msg_t_choice = '<B> Choisir le chiffre correspondant au genre :'
        t_choice = '{t_choice}\n{b_choice}'.format(
            **{'t_choice': msg_t_choice,
               'b_choice': self.cyan + ' '*5 + "="*41 + self.end,
               })
        block_3 = '{s_ref}\n\n {t_choice}\n'.format(
            **{'s_ref': s_ref,
               't_choice': t_choice
               })
        block_4 = '{ch_1}\n{ch_2}\n{ch_3}\n{ch_4}\n  {barre}  \n{ch_6}\n{ch_7}\n{ch_8}\n\n\n'.format(
            **{'ch_1': "  1  " + self.magenta + "ADJECTIF" + self.end,
               'ch_2': "  2  " + self.yellow + "NOM COMMUN" + self.end,
               'ch_3': "  3  " + self.green + "NOM PROPRE" + self.end,
               'ch_4': "  4  " + self.red + "VERBE" + self.end,
               'barre': self.cyan + '\n ' + '='*77 + self.end,
               'ch_6': "  6  " + self.cyan + "Passer" + self.end,
               'ch_7': "  7  " + self.cyan + "Enregistrer & Quitter" + self.end,
               'ch_8': "  8  " + self.cyan + "Quitter" + self.end
               })
        expected = '{block_1}\n{block_2}{block_3}\n{block_4}'.format(
            **{'block_1': block_1,
               'block_2': block_2,
               'block_3': block_3,
               'block_4': block_4,
               })
        bv.display(word, ref, verse, numbering, position)
        output = sys.stdout.getvalue()
        self.assertEqual(expected, output)

    def test_display_verse_multi_line(self):
        word = 'enseigne'
        ref = '1 Co 4:17'
        verse = "C'est pour cela que je vous ai envoyé Timotheos, qui est mon fils bien-aimé, et qui est fidèle dans le Seigneur, afin qu'il vous rappelle quelles sont mes voies en Mashiah et comment j'enseigne partout dans toutes les assemblées."
        numbering = (34, 45)
        position = (185, 193)  # enseigne
        msg_main = 'BYM - CONCORDANCE : VERIFICATION DE GENRES DES MOTS'
        model_main = '\n {l1}\n ={l2}=\n {l3}'.format(
            **{'l1': '='*77,
               'l2': ' '*12 + msg_main + ' '*12,
               'l3': '='*77
               })
        t_main = self.cyan + model_main + self.end
        msg_vers = '<A> Identifier le genre du mot en surbrillance :'
        t_vers = '{t_prop}\n{b_prop}'.format(
            **{'t_prop': msg_vers,
               'b_prop': self.cyan + ' '*5 + "="*42 + self.end,
               })
        model_s_word = '{barG}{word}{barC}{num}{/}{len}{barD}\n{=}'
        s_word = model_s_word.format(
            **{'barG': self.cyan + ' ======( ' + self.end,
               'word': self.blue + word + self.end,
               'barC': self.cyan + ' )======>  ' + self.end,
               'num': self.blue + str(numbering[0]) + self.end,
               '/': self.cyan + ' / ' + self.end,
               'len': self.blue + str(numbering[1]) + self.end,
               'barD': self.cyan + '  <' + '='*40 + self.end,
               '=': self.cyan + ' =' + ' '*75 + '=\n =' + ' '*75 + '= ' + self.end
              })
        block_1 = '{t_main}\n\n\n {t_vers}\n\n{s_word}'.format(
            **{'t_main': t_main,
               't_vers': t_vers,
               's_word': s_word,
               })
        model_block_2 = "{pre1}{vl1}{suf1}\n{pre2}{vl2}{suf2}\n{pre3}{vl31}{vl32}{suf3}\n{pre4}{vl4}{suf4}\n"
        block_2 = model_block_2.format(
            **{'pre1': self.cyan + " =  " + self.end + ' ',
               'vl1': verse[0:65],
               'suf1': self.cyan + ' '*7 + '=' + self.end,
               'pre2': self.cyan + " =  " + self.end + ' ',
               'vl2': verse[66:128],
               'suf2': self.cyan + ' '*10 + '=' + self.end,
               'pre3': self.cyan + " =  " + self.end + ' ',
               'vl31': "rappelle quelles sont mes voies en Mashiah et comment j'",
               'vl32': self.blue_color(word),
               'suf3': self.cyan + ' '*8 + '=' + self.end,
               'pre4': self.cyan + " =  " + self.end + ' ',
               'vl4': verse[194:229],
               'suf4': self.cyan + ' '*37 + '=' + self.end
              })
        model_s_ref = ' {=}\n{barG} {ref}{barD}\n\n'
        s_ref = model_s_ref.format(
            **{'=': self.cyan + '=' + ' '*75 + '=\n =' + ' '*75 + '=' + self.end,
               'barG': self.cyan + ' ======>' + self.end,
               'ref': self.blue + ref + self.end,
               'barD': self.cyan + ' <' + '='*58 + self.end
              })
        msg_t_choice = '<B> Choisir le chiffre correspondant au genre :'
        t_choice = '{t_choice}\n{b_choice}'.format(
            **{'t_choice': msg_t_choice,
               'b_choice': self.cyan + ' '*5 + "="*41 + self.end,
               })
        block_3 = '{s_ref}\n\n {t_choice}\n'.format(
            **{'s_ref': s_ref,
               't_choice': t_choice
               })
        block_4 = '{ch_1}\n{ch_2}\n{ch_3}\n{ch_4}\n  {barre}  \n{ch_6}\n{ch_7}\n{ch_8}\n\n\n'.format(
            **{'ch_1': "  1  " + self.magenta + "ADJECTIF" + self.end,
               'ch_2': "  2  " + self.yellow + "NOM COMMUN" + self.end,
               'ch_3': "  3  " + self.green + "NOM PROPRE" + self.end,
               'ch_4': "  4  " + self.red + "VERBE" + self.end,
               'barre': self.cyan + '\n ' + '='*77 + self.end,
               'ch_6': "  6  " + self.cyan + "Passer" + self.end,
               'ch_7': "  7  " + self.cyan + "Enregistrer & Quitter" + self.end,
               'ch_8': "  8  " + self.cyan + "Quitter" + self.end
               })
        expected = '{block_1}\n{block_2}{block_3}\n{block_4}'.format(
            **{'block_1': block_1,
               'block_2': block_2,
               'block_3': block_3,
               'block_4': block_4,
               })
        bv.display(word, ref, verse, numbering, position)
        output = sys.stdout.getvalue()
        self.assertEqual(expected, output)


class TestBymVerifySaveAndExit(unittest.TestCase):
    """Test class: save_and_exit function."""

    filename = os.path.normpath(os.path.join(DIR, 'verify_added_word.md'))
    word_dict = OrderedDict([
            (('mariée', 'n', 'marié'), "Es 62:5"),
            (('mariée', 'v', 'marier'), "1 Co 7:28"),
            (('visite', 'v', 'visiter'), "Jé 15:15, Jé 32:5, Za 10:3, Ps 80:15"),
            (('visite', 'n', 'visite'), "Lu 19:44, 1 Pi 2:12")])

    def test_write(self):
        fake_open = mock_open()
        fake_open.return_value.__iter__ = lambda self: iter(self.readline, '')
        with patch('builtins.open', fake_open) as fout:
            bv.write(self.filename, self.word_dict)
        expected = [
            "mariée,n,marié,Es 62:5\n",
            "mariée,v,marier,1 Co 7:28\n",
            "visite,v,visiter,Jé 15:15,Jé 32:5,Za 10:3,Ps 80:15\n",
            "visite,n,visite,Lu 19:44,1 Pi 2:12\n"]
        results = [res[0] for res, dummy in fout().write.call_args_list]
        self.assertEqual(expected, results)

    def test_save_and_exit_word_dict_not_empty(self):
        fake_open = mock_open()
        fake_open.return_value.__iter__ = lambda self: iter(self.readline, '')
        with self.assertRaises(SystemExit) as ctx:
            with patch('builtins.open', fake_open) as fout:
                bv.save_and_exit(self.filename, self.word_dict)
        self.assertEqual(ctx.exception.code, None)
        expected = [
            "mariée,n,marié,Es 62:5\n",
            "mariée,v,marier,1 Co 7:28\n",
            "visite,v,visiter,Jé 15:15,Jé 32:5,Za 10:3,Ps 80:15\n",
            "visite,n,visite,Lu 19:44,1 Pi 2:12\n"]
        results = [res[0] for res, dummy in fout().write.call_args_list]
        self.assertEqual(expected, results)

    def test_save_and_exit_word_dict_empty(self):
        fake_open = mock_open()
        fake_open.return_value.__iter__ = lambda self: iter(self.readline, '')
        with self.assertRaises(SystemExit) as ctx:
            with patch('builtins.open', fake_open) as fout:
                bv.save_and_exit(self.filename, {})
        self.assertEqual(ctx.exception.code, None)
        results = [res[0] for res, dummy in fout().write.call_args_list]
        self.assertEqual(len(results), 0)


class TestBymVerifyExitWithoutSaving(unittest.TestCase):
    """Test class: exit_without_saving function."""

    def test_exit_without_saving(self):
        with self.assertRaises(SystemExit) as ctx:
            with patch('builtins.open',
                       Mock(side_effect=AttributeError('Oops'))):
                bv.exit_without_saving()
        self.assertIsNone(ctx.exception.code)


class TestBymVerifyAttempt(unittest.TestCase):
    """Test class: attempt function."""

    def setUp(self):
        self.held, sys.stdout = sys.stdout, StringIO()
        self.maxDiff = None  # enable to get full diff output
        self.end = '\x1b[0m\n'

    def test_attempt_i_1(self):
        i = 1
        cyan = '\x1b[36m'
        msg = 'Choix erroné, veuillez procéder au dernier essai !'
        model = "\n {l1}\n =  {l2}=\n {l3}".format(
            **{'l1': '='*77,
               'l2': msg + ' '*23,
               'l3': '='*77
               })
        expected = cyan + model + self.end
        bv.attempt(i)
        output = sys.stdout.getvalue()
        self.assertEqual(expected, output)

    def test_attempt_i_0(self):
        i = 0
        red = '\x1b[31m'
        msg = 'Processus interrompu'
        model = "\n\n {l1}\n ={l2}=\n {l3}".format(
            **{'l1': '='*77,
               'l2': ' '*27 + msg + ' '*28,
               'l3': '='*77
               })
        expected = red + model + self.end
        with self.assertRaises(SystemExit) as ctx:
            bv.attempt(i)
        self.assertIsNone(ctx.exception.code)
        output = sys.stdout.getvalue()
        self.assertEqual(expected, output)

    def test_attempt_i_2(self):
        i = 2
        bv.attempt(i)
        output = sys.stdout.getvalue()
        expected = ''
        self.assertEqual(expected, output)

    def test_attempt_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.attempt(None)

    def test_attempt_with_empty_arg(self):
        with self.assertRaises(TypeError):
            bv.attempt('')


class TestBymVerifyMapChoice(unittest.TestCase):
    """Test class: map_choice function."""

    dict_choices = OrderedDict([
        ("adj", ("ADJECTIF")),
        ("n", ("NOM COMMUN")),
        ("np", ("NOM PROPRE")),
        ("v", ("VERBE")),
        ("", ""),
        ("p", ("Passer")),
        ("eq", ("Enregistrer & Quitter")),
        ("q", ("Quitter"))])

    def test_map_choice_adj(self):
        choice = '1'
        res = bv.utils.map_choice(choice, self.dict_choices)
        expected = 'adj'
        self.assertEqual(expected, res)

    def test_map_choice_n(self):
        choice = '2'
        res = bv.utils.map_choice(choice, self.dict_choices)
        expected = 'n'
        self.assertEqual(expected, res)

    def test_map_choice_np(self):
        choice = '3'
        res = bv.utils.map_choice(choice, self.dict_choices)
        expected = 'np'
        self.assertEqual(expected, res)

    def test_map_choice_v(self):
        choice = '4'
        res = bv.utils.map_choice(choice, self.dict_choices)
        expected = 'v'
        self.assertEqual(expected, res)

    def test_map_choice_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.map_choice(None, self.dict_choices)

    def test_map_choice_with_empty_arg(self):
        with self.assertRaises(ValueError):
            bv.utils.map_choice('', self.dict_choices)


class TestBymVerifyDownload(unittest.TestCase):
    """Test class: download function."""

    def test_download(self):
        res = bv.utils.download(os.path.normpath(os.path.join(FIXTURES,
                                                              'adjectives.md')))
        expected = {
            "abject": "abject,abjects",
            "abominable": "abominable,abominables",
            "abondant": "abondant,abondantes,abondants,abondante",
            "acheteur": "acheteur"}
        self.assertEqual(dict, type(res))
        self.assertEqual(expected, res)

    def test_download_with_no_arg(self):
        with self.assertRaises(TypeError):
            bv.utils.download(None)

    def test_download_with_empty_arg(self):
        with self.assertRaises(FileNotFoundError):
            bv.utils.download('')


class TestBymVerifyDictBy(unittest.TestCase):
    """Test class: dict_by function."""

    def test_dict_by_adj(self):
        genre = 'adj'
        res = bv.dict_by(genre)
        word = 'insignifiant'
        self.assertTrue(res[word])

    def test_dict_by_n(self):
        genre = 'n'
        res = bv.dict_by(genre)
        word = 'agneau'
        self.assertTrue(res[word])

    def test_dict_by_np(self):
        genre = 'np'
        res = bv.dict_by(genre)
        word = 'shaddaï'
        self.assertTrue(res[word])

    def test_dict_by_v(self):
        genre = 'v'
        res = bv.dict_by(genre)
        word = 'œuvrer'
        self.assertTrue(res[word])

    def test_dict_by_genre_empty(self):
        genre = ''
        with self.assertRaises(KeyError):
            bv.dict_by(genre)


class TestBymVerifyShowProposal(unittest.TestCase):
    """Test class: show_proposal function."""

    def setUp(self):
        self.held, sys.stdout = sys.stdout, StringIO()
        self.maxDiff = None  # enable to get full diff output

    def test_show_proposal(self):
        bv.show_proposal()
        output = sys.stdout.getvalue()
        red = '\x1b[31m'
        cyan = '\x1b[36m'
        yellow = '\x1b[33m'
        magenta = '\x1b[35m'
        end = '\x1b[0m'
        msg_main = 'BYM - CONCORDANCE : VERIFICATION DE GENRES DES MOTS'
        model_main = '\n {l1}\n ={l2}=\n {l3}'.format(
            **{'l1': '='*77,
               'l2': ' '*12 + msg_main + ' '*12,
               'l3': '='*77
               })
        t_main = cyan + model_main + end
        msg_prop = '<C> Proposer une désignation pour votre choix :'
        t_prop = '{t_prop}\n{b_prop}'.format(
            **{'t_prop': msg_prop,
               'b_prop': cyan + ' '*5 + "="*41 + end,
               })
        design = '\n{adj} : {msg_adj}\n\n{nom} : {msg_nom}\n\n{verb} : {msg_verb}\n\n\n\n'.format(
            **{'adj': '     - ' + magenta + "Adjectif" + end,
               'msg_adj': 'genre masculin et singulier',
               'nom': '     - ' + yellow + "Nom" + end,
               'msg_nom': 'genre masculin et singulier',
               'verb': '     - ' + red + "Verbe" + end,
               'msg_verb': 'infinitif'
               })
        expected = '{t_main}\n\n\n {t_prop}\n{design}'.format(
            **{'t_main': t_main,
               't_prop': t_prop,
               'design': design
               })
        self.assertEqual(expected, output)


class TestBymVerifyMessageBy(unittest.TestCase):
    """Test class: message_by function."""

    def test_message_by_adjective(self):
        genre = 'adj'
        res = bv.message_by(genre)
        expected = "\n >>> Veuillez saisir la désignation de l'adjectif « {} » : "
        self.assertEqual(expected, res)

    def test_message_by_noun(self):
        genre = 'n'
        res = bv.message_by(genre)
        expected = "\n >>> Veuillez saisir la désignation du nom « {} » : "
        self.assertEqual(expected, res)

    def test_message_by_verb(self):
        genre = 'v'
        res = bv.message_by(genre)
        expected = "\n >>> Veuillez saisir la désignation du verbe « {} » : "
        self.assertEqual(expected, res)

    def test_message_by_with_no_arg(self):
        with self.assertRaises(KeyError):
            bv.message_by(None)

    def test_message_by_with_empty_arg(self):
        with self.assertRaises(KeyError):
            bv.message_by('')


class TestBymVerifyGetProposal(unittest.TestCase):
    """Test class: get_proposal function."""

    def setUp(self):
        self.held, sys.stdout = sys.stdout, StringIO()
        self.maxDiff = None  # enable to get full diff output
        self.confirm = ("\n >>> Confirmez-vous la saisie suivante « {} » ? "
                        "(O/N) : ")
        self.message_adj = ("\n >>> Veuillez saisir la désignation de "
                            "l'adjectif « {} » : ")
        self.msg = MagicMock(return_value=None)
        self.conf = MagicMock(return_value=None)

    def test_get_proposal_first_attempt_strip_lower(self):
        user_input = ['  AnCieN ', 'oui']
        word = 'anciens'
        with patch('builtins.input', side_effect=user_input):
            res = bv.get_proposal(word, self.message_adj)
            expected = 'ancien'
            self.assertEqual(expected, res)

    def test_get_proposal_second_attempt(self):
        user_input = ['  AnCieN ', 'non', '  AnCieN ', 'oui']
        word = 'anciens'
        with patch('builtins.input', side_effect=user_input):
            res = bv.get_proposal(word, self.message_adj)
            expected = 'ancien'
            self.assertEqual(expected, res)

    def test_get_proposal_third_attempt(self):
        user_input = ['  AnCieN ', 'non', '  AnCieN ', 'non', '  AnCieN ',
                      'oui']
        word = 'anciens'
        with patch('builtins.input', side_effect=user_input):
            res = bv.get_proposal(word, self.message_adj)
            expected = 'ancien'
            self.assertEqual(expected, res)

    def test_get_proposal_third_attempt_exit_msg(self):
        user_input = ['ancien', 'non', 'ancien', 'non', 'ancien', 'non']
        word = 'Anciens'
        red = '\x1b[31m'
        end = '\x1b[0m'
        msg = 'Processus interrompu'
        model = "\n\n {l1}\n ={l2}=\n {l3}".format(
            **{'l1': '='*77,
               'l2': ' '*27 + msg + ' '*28,
               'l3': '='*77
               })
        expected = red + model + end
        with patch('builtins.input', side_effect=user_input):
            with self.assertRaises(SystemExit):
                bv.get_proposal(word, self.message_adj)
            output = sys.stdout.getvalue().strip()
            self.assertEqual(expected, output)

    def test_get_proposal_check_calls_and_return_adj(self):
        user_input = ['ancien', 'oui']
        word = 'Anciens'
        expected = 'ancien'
        self.msg(self.message_adj.format(word))
        self.conf(self.confirm.format(expected))
        with patch('builtins.input', side_effect=user_input) as out:
            res = bv.get_proposal(word, self.message_adj)
            self.assertEqual(self.msg.call_args, out.call_args_list[0])
            self.assertEqual(self.conf.call_args, out.call_args_list[1])
            self.assertEqual(res, expected)

    def test_get_proposal_check_calls_and_return_n(self):
        user_input = ['ancien', 'oui']
        word = 'ancienne'
        message = "\n >>> Veuillez saisir la désignation du nom « {} » : "
        expected = 'ancien'
        self.msg(message.format(word))
        self.conf(self.confirm.format(expected))
        with patch('builtins.input', side_effect=user_input) as out:
            res = bv.get_proposal(word, message)
            self.assertEqual(self.msg.call_args, out.call_args_list[0])
            self.assertEqual(self.conf.call_args, out.call_args_list[1])
            self.assertEqual(res, expected)

    def test_get_proposal_check_calls_and_return_v(self):
        user_input = ['calmer', 'oui']
        word = 'calma'
        message = "\n >>> Veuillez saisir la désignation du verbe « {} » : "
        expected = 'calmer'
        self.msg(message.format(word))
        self.conf(self.confirm.format(expected))
        with patch('builtins.input', side_effect=user_input) as out:
            res = bv.get_proposal(word, message)
            self.assertEqual(self.msg.call_args, out.call_args_list[0])
            self.assertEqual(self.conf.call_args, out.call_args_list[1])
            self.assertEqual(res, expected)

    def test_get_proposal_regex_proposal_ok(self):
        regex_proposal = r"^[a-zâàéèêëôôîïûüç\-']*"
        given_answer = 'ancien'
        given_answer2 = 'oui'
        word = 'ancienne'
        with patch('builtins.input', return_value=given_answer) as out:
            with patch('builtins.input', return_value=given_answer2):
                bv.get_proposal(word, self.message_adj)
                self.assertRegex(out.return_value, regex_proposal)

    def test_get_proposal_regex_proposal_nok(self):
        regex_proposal = r"^[a-zâàéèêëôôîïûüç\-']*"
        given_answer = '@azfz12ancien'
        given_answer2 = 'oui'
        word = 'ancienne'
        with patch('builtins.input', return_value=given_answer) as out:
            with patch('builtins.input', return_value=given_answer2):
                bv.get_proposal(word, self.message_adj)
                self.assertNotRegex(regex_proposal, out.return_value)

    def test_get_proposal_regex_confirm_ok(self):
        regex_confirm = r'(o, oui, n, non)'
        given_answer = 'azfz12ancien'
        given_answer2 = 'non'
        word = 'ancienne'
        with patch('builtins.input', return_value=given_answer):
            with patch('builtins.input', return_value=given_answer2) as out2:
                with self.assertRaises(SystemExit):
                    bv.get_proposal(word, self.message_adj)
                self.assertRegex(regex_confirm, out2.return_value)

    def test_get_proposal_regex_confirm_nok(self):
        regex_confirm = r'(o, oui, n, non)'
        given_answer = 'azfz12ancien'
        given_answer2 = 'nonn'
        word = 'ancienne'
        with patch('builtins.input', return_value=given_answer):
            with patch('builtins.input', return_value=given_answer2) as out2:
                with self.assertRaises(SystemExit):
                    bv.get_proposal(word, self.message_adj)
                self.assertNotRegex(regex_confirm, out2.return_value)


class TestBymVerifyGetFirstItem(unittest.TestCase):
    """Test class: get_first_item function."""

    def test_get_first_item(self):
        string = "absorbent"
        word_dict = OrderedDict([
            ('aboyer', 'aboyer'),
            ('abriter', 'abriter,abritaient'),
            ('abréger', 'abréger,abrégés,abrégé'),
            ('absorber', 'absorber,absorbé,absorbent,absorba')])
        res = bv.utils.get_first_item(string, word_dict)
        expected = "absorber"
        self.assertEqual(str, type(res))
        self.assertEqual(expected, res)


class TestBymVerifygetDesignatedOrProposed(unittest.TestCase):
    """Test class: get_designated_or_proposed function."""

    def test_get_designated_or_proposed(self):
        pass


class TestBymVerifyProcessChoices(unittest.TestCase):
    """Test class: process_choice function."""

    def test_process_choice(self):
        pass


class TestBymVerifySwitchChoices(unittest.TestCase):
    """Test class: switch_choices function."""

    def test_switch_choices(self):
        pass


class TestBymVerifyVerify(unittest.TestCase):
    """Test class: verify function."""

    def test_verify(self):
        pass


if __name__ == '__main__':
    unittest.main()
