"""BJC-VERIFY-ADDED-WORD LIBRARY."""

import os
import re
from collections import deque

# colorized
def red(text):
    """Colored red.

    >>> func = red
    >>> '\x1b[31mmy sentence\x1b[0m' == func('my sentence')
    True
    >>> '\x1b[31m1234\x1b[0m' == func(1234)
    True
    >>> '\x1b[31mNone\x1b[0m' == func(None)
    True
    >>> '\x1b[31m\x1b[0m' == func('')
    True
    >>> func()
    Traceback (most recent call last):
        ...
    TypeError: red() missing 1 required positional argument: 'text'
    """
    return '\x1b[31m{}\x1b[0m'.format(text)


def green(text):
    """Colored green.

    >>> func = green
    >>> '\x1b[32mmy sentence\x1b[0m' == func('my sentence')
    True
    >>> '\x1b[32m1234\x1b[0m' == func(1234)
    True
    >>> '\x1b[32mNone\x1b[0m' == func(None)
    True
    >>> '\x1b[32m\x1b[0m' == func('')
    True
    >>> func()
    Traceback (most recent call last):
        ...
    TypeError: green() missing 1 required positional argument: 'text'
    """
    return '\x1b[32m{}\x1b[0m'.format(text)


def yellow(text):
    """Colored yellow.

    >>> func = yellow
    >>> '\x1b[33mmy sentence\x1b[0m' == func('my sentence')
    True
    >>> '\x1b[33m1234\x1b[0m' == func(1234)
    True
    >>> '\x1b[33mNone\x1b[0m' == func(None)
    True
    >>> '\x1b[33m\x1b[0m' == func('')
    True
    >>> func()
    Traceback (most recent call last):
        ...
    TypeError: yellow() missing 1 required positional argument: 'text'
    """
    return '\x1b[33m{}\x1b[0m'.format(text)


def blue(text):
    """Colored blue.

    >>> func = blue
    >>> '\x1b[34mmy sentence\x1b[0m' == func('my sentence')
    True
    >>> '\x1b[34m1234\x1b[0m' == func(1234)
    True
    >>> '\x1b[34mNone\x1b[0m' == func(None)
    True
    >>> '\x1b[34m\x1b[0m' == func('')
    True
    >>> func()
    Traceback (most recent call last):
        ...
    TypeError: blue() missing 1 required positional argument: 'text'
    """
    return '\x1b[34m{}\x1b[0m'.format(text)


def magenta(text):
    """Colored magenta.

    >>> func = magenta
    >>> '\x1b[35mmy sentence\x1b[0m' == func('my sentence')
    True
    >>> '\x1b[35m1234\x1b[0m' == func(1234)
    True
    >>> '\x1b[35mNone\x1b[0m' == func(None)
    True
    >>> '\x1b[35m\x1b[0m' == func('')
    True
    >>> func()
    Traceback (most recent call last):
        ...
    TypeError: magenta() missing 1 required positional argument: 'text'
    """
    return '\x1b[35m{}\x1b[0m'.format(text)


def cyan(text):
    """Colored cyan.

    >>> func = cyan
    >>> '\x1b[36mmy sentence\x1b[0m' == func('my sentence')
    True
    >>> '\x1b[36m1234\x1b[0m' == func(1234)
    True
    >>> '\x1b[36mNone\x1b[0m' == func(None)
    True
    >>> '\x1b[36m\x1b[0m' == func('')
    True
    >>> func()
    Traceback (most recent call last):
        ...
    TypeError: cyan() missing 1 required positional argument: 'text'
    """
    return '\x1b[36m{}\x1b[0m'.format(text)


def create_dirs(*args):
    """Create a directory if not exist."""
    for folder in args:
        if not os.path.exists(folder):
            os.mkdir(folder)


def clear():
    """Clear screen."""
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')


def filter_file(file_extension, list_file):
    """Filter files."""
    for file in list_file:
        if file.endswith(file_extension) and file[:2].isdigit():
            yield file


def extract_verse(folder, list_file):
    """Extract verses."""
    re_verse = re.compile(r'^([0-9]+:[0-9]+)\t(.+)')
    for file in list_file:
        file = os.path.normpath(os.path.join(folder, file))
        with open(file, 'r', encoding='utf-8') as lines:
            for line in lines:
                verse = re_verse.match(line)
                if not verse:
                    continue
                yield os.path.basename(file), verse


def short_name(integer, dict_books):
    """Return book's short name."""
    return dict_books.get(integer, None)


def sub_comments(string):
    """Subtract comments and signs."""
    re_comments = re.compile(r'<[^<]+>')
    re_space_commas = re.compile(r' ,')
    re_space_points = re.compile(r' \.')
    re_spaces = re.compile(r' +')
    string = re_comments.sub(' ', string)
    string = re_spaces.sub(' ', string)
    string = re_space_commas.sub(',', string)
    string = re_space_points.sub('.', string)
    return string.strip()


def load_verses(verses, dict_books):
    """Load verses in a dict."""
    ret = {}
    for item, verse in verses:
        shortcut = short_name(item[:2], dict_books)
        number, content = verse.groups()
        verse_contents = sub_comments(content)
        verse_reference = "{book} {number}".format(
            **{'book': shortcut,
               'number': number})
        ret[verse_reference] = verse_contents.strip()
    return ret


def clear_sign(string):
    """Get rid of signs."""
    text = str(string)
    text = text.replace('[', '')
    text = text.replace('\'', '')
    text = text.replace(']', '')
    text = text.replace('{', '')
    text = text.replace('}', '')
    return text


def finder(searched_string, text):
    """Return regex iterator.

    @input: searched_string - string to search
    @input: text - string text to search into
    @return: function - regex iterator
    """
    regex = re.compile(r'\b{}\b'.format(searched_string), flags=re.I)
    return regex.finditer(text)


def count_word(string, lst, _dict):
    """Count word occurrences.

    @input: string - string to search
    @input: lst - list of references
    @input: _dict - dictionary of verses
    @return: integer - sum of the word occurrences
    """
    count = 0
    for item in lst:
        item = clear_sign(item)
        text = _dict[item]
        regex = re.compile(r'\b{}\b'.format(string), flags=re.I)
        for _ in regex.finditer(text):
            count += 1
    return count


def coordinate(string, integer):
    """Generate line's coordinates.

    @input: string - string text
    @input: integer - max digit per line
    @return: list of tuple - (start, end) coordinates of each line,
             start: first index
             end: last index
    """
    end = 0
    start = 0
    digits = []
    coordinates = []
    for index, digit in enumerate(list(string)):
        digits.append(digit)
        length = len(list(digits))
        while length > integer and digits[-1] != ' ':
            digits = ''.join(digits).split()
            line = ' '.join(digits[:-1])
            end = (index + 1) - (length - len(list(line)))
            coordinates.append((start, end))
            end += 1
            start = end
            remain_digits = digits[-1]
            digits.clear()
            # send remain digits to the next line
            for each in remain_digits:
                digits.append(each)
            break
    coordinates.append((end, len(list(string))))
    return coordinates


def map_choice(choice, dict_choices):
    """Map choice."""
    values = list(dict_choices.items())
    return {
        choice: lambda: values.__getitem__(int(choice)-1)[0].strip()
    }.get(choice, None)()


def download(filename):
    """Download a file of words and return a dict.

    @input: file - words' list
    @return: dict - key and value pair of listed words,
             key: first element of line
             value: the line
    """
    ret = {}
    with open(filename, 'r', encoding='utf-8') as lines:
        for line in lines:
            items = line.rstrip().split(',')
            ret[items[0]] = ','.join(items[0:])
    return ret


def get_first_item(string, word_dict):
    """Return first item from a dict value."""
    first_item = ''
    for line in word_dict.values():
        regex = re.compile(r'\b{}\b'.format(string))
        if regex.findall(line):
            first_item = line.split(',')[0].strip()
    return first_item


def load_first_items(lst):
    """Load first items of each line.

       @input: file - pre-formatted list
       @return: list - first items of lines
       e.g. input file:
            disposée,adj,disposé,1 Ch 28:9
            crues,v,croire,Hé 11:13
            tues,v,taire,Ps 107:29
            tues,v,tuer,Mt 23:37,Lu 13:34

            first_items = load_first_items(arg)
            first_items = ['disposée', 'crues', 'tues', 'tues']
    """
    first_items = deque()
    for item in lst:
        first_items.append(item.rstrip().split(',')[0])
    return first_items


def separate_by(counter):
    """Separate by element count.

       @input: Counter - counter element
       @return: lists - singleton : list of element that count == 1
                        duplicate : list of element that count >= 2
       e.g. counter = [('disposée', 1), ('crues', 1), ('tues', 2)]
            a, b = separate_by(counter)
            a = ['disposée', 'crues']
            b = ['tues']
    """
    singleton = deque()
    duplicate = deque()
    for word, count in counter.items():
        if count == 1:
            singleton.append(word)
        if count >= 2:
            duplicate.append(word)
    return singleton, duplicate


def file_to_list(filename):
    """Load file and return a list."""
    ret = deque()
    with open(filename, 'r', encoding='utf-8') as lines:
        for line in lines:
            ret.append(line)
    return ret


def is_word_saved(_dict, key, word):
    """Check if a word is in a dictionary."""
    try:
        val = _dict[key]
        regex = re.compile(r'\b{}\b'.format(word))
        if not regex.findall(val):
            return False
        return True
    except KeyError:
        return False


def is_reference_saved(_dict, key, reference):
    """Check if a reference is in a dictionary."""
    try:
        values = _dict[key]
        if not reference in values:
            return False
        return True
    except KeyError:
        return False


def sort(books, refs):
    """Sort list reference of verses according to bible orders."""
    def sort_by(content):
        content = content.rsplit(' ', 1)
        book = content[0]
        chap, num = map(int, content[-1].split(':'))
        return (books.index(book), chap, num)

    ret = sorted(
        list(refs),
        key=sort_by
    )
    return ret


def to_list(string, status=False):
    """Return string's elements."""
    ele = string.rstrip().split(',')
    ret = ele[0], ele[1], ele[2], ele[3:]
    if status:
        return ret
    return ret[:3]
